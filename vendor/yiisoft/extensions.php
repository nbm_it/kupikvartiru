<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'vova07/yii2-imperavi-widget' => 
  array (
    'name' => 'vova07/yii2-imperavi-widget',
    'version' => '1.2.10.0',
    'alias' => 
    array (
      '@vova07/imperavi' => $vendorDir . '/vova07/yii2-imperavi-widget/src',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput',
    ),
  ),
  'kartik-v/yii2-helpers' => 
  array (
    'name' => 'kartik-v/yii2-helpers',
    'version' => '1.3.5.0',
    'alias' => 
    array (
      '@kartik/helpers' => $vendorDir . '/kartik-v/yii2-helpers',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.7.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog',
    ),
  ),
  'kartik-v/yii2-detail-view' => 
  array (
    'name' => 'kartik-v/yii2-detail-view',
    'version' => '1.7.5.0',
    'alias' => 
    array (
      '@kartik/detail' => $vendorDir . '/kartik-v/yii2-detail-view',
    ),
  ),
  'arogachev/yii2-excel' => 
  array (
    'name' => 'arogachev/yii2-excel',
    'version' => '0.1.1.0',
    'alias' => 
    array (
      '@arogachev/excel' => $vendorDir . '/arogachev/yii2-excel/src',
    ),
  ),
  'kartik-v/yii2-slider' => 
  array (
    'name' => 'kartik-v/yii2-slider',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/slider' => $vendorDir . '/kartik-v/yii2-slider',
    ),
  ),
  '2amigos/yii2-gallery-widget' => 
  array (
    'name' => '2amigos/yii2-gallery-widget',
    'version' => '0.1.0.0',
    'alias' => 
    array (
      '@dosamigos/gallery' => $vendorDir . '/2amigos/yii2-gallery-widget',
    ),
  ),
);
