<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit0fb7274d6961f08116ce84af67e50509
{
    public static $files = array (
        '2cffec82183ee1cea088009cef9a6fc3' => __DIR__ . '/..' . '/ezyang/htmlpurifier/library/HTMLPurifier.composer.php',
        '2c102faa651ef8ea5874edb585946bce' => __DIR__ . '/..' . '/swiftmailer/swiftmailer/lib/swift_required.php',
    );

    public static $prefixLengthsPsr4 = array (
        'y' => 
        array (
            'yii\\swiftmailer\\' => 16,
            'yii\\gii\\' => 8,
            'yii\\faker\\' => 10,
            'yii\\debug\\' => 10,
            'yii\\composer\\' => 13,
            'yii\\codeception\\' => 16,
            'yii\\bootstrap\\' => 14,
            'yii\\' => 4,
        ),
        'v' => 
        array (
            'vova07\\imperavi\\' => 16,
        ),
        'k' => 
        array (
            'kartik\\slider\\' => 14,
            'kartik\\plugins\\fileinput\\' => 25,
            'kartik\\helpers\\' => 15,
            'kartik\\file\\' => 12,
            'kartik\\dialog\\' => 14,
            'kartik\\detail\\' => 14,
            'kartik\\base\\' => 12,
        ),
        'd' => 
        array (
            'dosamigos\\gallery\\' => 18,
        ),
        'c' => 
        array (
            'cebe\\markdown\\' => 14,
        ),
        'a' => 
        array (
            'arogachev\\excel\\' => 16,
        ),
        'F' => 
        array (
            'Faker\\' => 6,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'yii\\swiftmailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-swiftmailer',
        ),
        'yii\\gii\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-gii',
        ),
        'yii\\faker\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-faker',
        ),
        'yii\\debug\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-debug',
        ),
        'yii\\composer\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-composer',
        ),
        'yii\\codeception\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-codeception',
        ),
        'yii\\bootstrap\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-bootstrap',
        ),
        'yii\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2',
        ),
        'vova07\\imperavi\\' => 
        array (
            0 => __DIR__ . '/..' . '/vova07/yii2-imperavi-widget/src',
        ),
        'kartik\\slider\\' => 
        array (
            0 => __DIR__ . '/..' . '/kartik-v/yii2-slider',
        ),
        'kartik\\plugins\\fileinput\\' => 
        array (
            0 => __DIR__ . '/..' . '/kartik-v/bootstrap-fileinput',
        ),
        'kartik\\helpers\\' => 
        array (
            0 => __DIR__ . '/..' . '/kartik-v/yii2-helpers',
        ),
        'kartik\\file\\' => 
        array (
            0 => __DIR__ . '/..' . '/kartik-v/yii2-widget-fileinput',
        ),
        'kartik\\dialog\\' => 
        array (
            0 => __DIR__ . '/..' . '/kartik-v/yii2-dialog',
        ),
        'kartik\\detail\\' => 
        array (
            0 => __DIR__ . '/..' . '/kartik-v/yii2-detail-view',
        ),
        'kartik\\base\\' => 
        array (
            0 => __DIR__ . '/..' . '/kartik-v/yii2-krajee-base',
        ),
        'dosamigos\\gallery\\' => 
        array (
            0 => __DIR__ . '/..' . '/2amigos/yii2-gallery-widget',
        ),
        'cebe\\markdown\\' => 
        array (
            0 => __DIR__ . '/..' . '/cebe/markdown',
        ),
        'arogachev\\excel\\' => 
        array (
            0 => __DIR__ . '/..' . '/arogachev/yii2-excel/src',
        ),
        'Faker\\' => 
        array (
            0 => __DIR__ . '/..' . '/fzaninotto/faker/src/Faker',
        ),
    );

    public static $prefixesPsr0 = array (
        'P' => 
        array (
            'PHPExcel' => 
            array (
                0 => __DIR__ . '/..' . '/phpoffice/phpexcel/Classes',
            ),
        ),
        'H' => 
        array (
            'HTMLPurifier' => 
            array (
                0 => __DIR__ . '/..' . '/ezyang/htmlpurifier/library',
            ),
        ),
        'D' => 
        array (
            'Diff' => 
            array (
                0 => __DIR__ . '/..' . '/phpspec/php-diff/lib',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit0fb7274d6961f08116ce84af67e50509::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit0fb7274d6961f08116ce84af67e50509::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit0fb7274d6961f08116ce84af67e50509::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
