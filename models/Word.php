<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "word".
 *
 * @property integer $id
 * @property string $word
 * @property string $explanation
 * @property string $example
 * @property string $translation
 */
class Word extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'word';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['word', 'explanation', 'example', 'translation'], 'required'],
            [['explanation', 'example', 'translation'], 'string'],
            [['word'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'word' => 'Word',
            'explanation' => 'Explanation',
            'example' => 'Example',
            'translation' => 'Translation',
        ];
    }
}
