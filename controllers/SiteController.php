<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\db\Expression;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use yii\data\ActiveDataProvider;
use app\modules\admin\models\Carousel;
use app\modules\admin\models\Object;
use app\modules\admin\models\RaionObj;
use app\modules\admin\models\TypeObj;
use app\modules\admin\models\UnderTypeObj;
use app\modules\admin\models\Expert;
use app\modules\admin\models\Video;
use app\modules\admin\models\Developer;
use app\modules\admin\models\Sdacha;
use app\modules\admin\models\LeftBlock;
use app\modules\admin\models\BotUrl;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $model = Carousel::find()->all();
        $object = Object::find()->all();

// количество обьектов
        $p = Object::find()->count();
        $count_loft = Object::find()->where(['loft'=> 1])->count();
        // $p =floor ( $p/4)+($p % 4>0);
        $p=$p-4;

        $raion = RaionObj::find()->all();
        $type = TypeObj::find()->all();

        $expert = Expert::find()->limit(4)->all();
        $expert2 = Expert::find()->offset(4)->all();
        $exp_count = Expert::find()->offset(4)->count();

        $video = Video::find()->all();
        $boturl = BotUrl::find()->all();
        $sdacha = Sdacha::find()->orderBy([
            'name_cal' => SORT_DESC
        ])->all();

        $developer = Developer::find()->limit(4)->all();
        $developer2 = Developer::find()->offset(4)->all();
        $dev_count = Developer::find()->offset(4)->count();

        $untype = UnderTypeObj::find()->all();

        $leftblock = LeftBlock::find()->where(['type'=> 1])->orderBy(new Expression('rand()'))->limit(1)->all();
        $leftblock2 = LeftBlock::find()->where(['type'=> 2])->orderBy(new Expression('rand()'))->limit(2)->all();

        foreach ($type as $key => $val) {
            $calcType[$val['id']]=0;
            foreach ($object as $k => $v) {
                if(  in_array( $val['id'], explode(",",$v['type_is']) )  )
                    $calcType[$val['id']]++;
            }
        }
        foreach ($untype as $key => $val) {
            $calcUntype[$val['id']]=0;
            foreach ($object as $k => $v) {
                if(  in_array( $val['id'], explode(",",$v['un_type_is']) )  )
                    $calcUntype[$val['id']]++;
            }
        }
        $calcOtd[0]=Object::find()->where([ 'otdelka'=> 0])->count();
        $calcOtd[1]=Object::find()->where([ 'otdelka'=> 1])->count();
        foreach ($sdacha as $key => $val) {
            $calcSda[$val['name_cal']] = Object::find()->where([ 'time_off'=> $val['id'] ])->count();
        }
        foreach ($raion as $key => $val) {
            $calcRai[$val['id']] = Object::find()->where([ 'raion'=> $val['id'] ])->count();
        }
        $max=0;$min=100000000;
        $maxM=0;$minM=1000000;
        $max_m=0;$min_m=1000000;
        foreach ($object as $key => $val) {

            $price_m=explode(",",$val['price_m']);

            if($max_m<$price_m[1]){
                $max_m = $price_m[1];
            }
            if($min_m>$price_m[0]){
                $min_m = $price_m[0];
            }

            $price=explode(",",$val['price']);

            if($max<$price[1]){
                $max = $price[1];
            }
            if($min>$price[0]){
                $min = $price[0];
            }

            $metr=explode(",",$val['metr']);

            if($maxM<$metr[1]){
                $maxM = $metr[1];
            }
            if($minM>$metr[0]){
                $minM = $metr[0];
            }
        } 
// 
        // $max_m=1000000;$min_m=0;
        $config=[
	        'dc'=>($dev_count>4?2:1),
	        'ec'=>($exp_count>4?2:1),
	        'pages'=>$p,
            'calcType'=> $calcType,
	        'calcUnType'=> $calcUntype,
	        'calcOtd'=> $calcOtd,
	        'calcSda'=> $calcSda,
	        'calcRai'=> $calcRai,
            'min'=>$min,
            'max'=>$max,
            'metr_min'=>$minM,
            'metr_max'=>$maxM,
            'calcLoft'=>$count_loft,

            'price_min'=>$min_m,
            'price_max'=>$max_m,
        ];
 // echo "<pre>";print_r($video);exit;
        return $this->render('index', [
            'config'=>$config,
            'model' => $model,
            'object' => $object,
            'raion' => $raion,
            'type' => $type,
            'expert' => $expert,
            'expert2' => $expert2,
            'video' => $video,
            'developer' => $developer,
            'developer2' => $developer2,
            'sdacha' => $sdacha,
            'leftblock' => $leftblock,
            'leftblock2' => $leftblock2,
            'boturl' => $boturl,
            'untype' => $untype,
        ]);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    // public function actionContact()
    // {
    //     $model = new ContactForm();
    //     if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
    //         Yii::$app->session->setFlash('contactFormSubmitted');

    //         return $this->refresh();
    //     }
    //     return $this->render('contact', [
    //         'model' => $model,
    //     ]);
    // }

    // public function actionAbout()
    // {
    //     return $this->render('about');
    // }
    protected function findModel($id)
    {
        if (($model = Lesson::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
