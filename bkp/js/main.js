! function(e, t) { "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
        if (!e.document) throw new Error("jQuery requires a window with a document");
        return t(e) } : t(e) }("undefined" != typeof window ? window : this, function(e, t) {
    function n(e) {
        var t = "length" in e && e.length,
            n = J.type(e);
        return "function" === n || J.isWindow(e) ? !1 : 1 === e.nodeType && t ? !0 : "array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e }

    function r(e, t, n) {
        if (J.isFunction(t)) return J.grep(e, function(e, r) {
            return !!t.call(e, r, e) !== n });
        if (t.nodeType) return J.grep(e, function(e) {
            return e === t !== n });
        if ("string" == typeof t) {
            if (se.test(t)) return J.filter(t, e, n);
            t = J.filter(t, e) }
        return J.grep(e, function(e) {
            return V.call(t, e) >= 0 !== n }) }

    function i(e, t) {
        for (;
            (e = e[t]) && 1 !== e.nodeType;);
        return e }

    function o(e) {
        var t = he[e] = {};
        return J.each(e.match(de) || [], function(e, n) { t[n] = !0 }), t }

    function a() { Z.removeEventListener("DOMContentLoaded", a, !1), e.removeEventListener("load", a, !1), J.ready() }

    function s() { Object.defineProperty(this.cache = {}, 0, { get: function() {
                return {} } }), this.expando = J.expando + s.uid++ }

    function l(e, t, n) {
        var r;
        if (void 0 === n && 1 === e.nodeType)
            if (r = "data-" + t.replace(be, "-$1").toLowerCase(), n = e.getAttribute(r), "string" == typeof n) {
                try { n = "true" === n ? !0 : "false" === n ? !1 : "null" === n ? null : +n + "" === n ? +n : xe.test(n) ? J.parseJSON(n) : n } catch (i) {}
                ve.set(e, t, n) } else n = void 0;
        return n }

    function c() {
        return !0 }

    function u() {
        return !1 }

    function f() {
        try {
            return Z.activeElement } catch (e) {} }

    function p(e, t) {
        return J.nodeName(e, "table") && J.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e }

    function d(e) {
        return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e }

    function h(e) {
        var t = Re.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e }

    function g(e, t) {
        for (var n = 0, r = e.length; r > n; n++) ye.set(e[n], "globalEval", !t || ye.get(t[n], "globalEval")) }

    function m(e, t) {
        var n, r, i, o, a, s, l, c;
        if (1 === t.nodeType) {
            if (ye.hasData(e) && (o = ye.access(e), a = ye.set(t, o), c = o.events)) { delete a.handle, a.events = {};
                for (i in c)
                    for (n = 0, r = c[i].length; r > n; n++) J.event.add(t, i, c[i][n]) }
            ve.hasData(e) && (s = ve.access(e), l = J.extend({}, s), ve.set(t, l)) } }

    function y(e, t) {
        var n = e.getElementsByTagName ? e.getElementsByTagName(t || "*") : e.querySelectorAll ? e.querySelectorAll(t || "*") : [];
        return void 0 === t || t && J.nodeName(e, t) ? J.merge([e], n) : n }

    function v(e, t) {
        var n = t.nodeName.toLowerCase(); "input" === n && ke.test(e.type) ? t.checked = e.checked : ("input" === n || "textarea" === n) && (t.defaultValue = e.defaultValue) }

    function x(t, n) {
        var r, i = J(n.createElement(t)).appendTo(n.body),
            o = e.getDefaultComputedStyle && (r = e.getDefaultComputedStyle(i[0])) ? r.display : J.css(i[0], "display");
        return i.detach(), o }

    function b(e) {
        var t = Z,
            n = _e[e];
        return n || (n = x(e, t), "none" !== n && n || (Fe = (Fe || J("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement), t = Fe[0].contentDocument, t.write(), t.close(), n = x(e, t), Fe.detach()), _e[e] = n), n }

    function w(e, t, n) {
        var r, i, o, a, s = e.style;
        return n = n || Be(e), n && (a = n.getPropertyValue(t) || n[t]), n && ("" !== a || J.contains(e.ownerDocument, e) || (a = J.style(e, t)), $e.test(a) && Ie.test(t) && (r = s.width, i = s.minWidth, o = s.maxWidth, s.minWidth = s.maxWidth = s.width = a, a = n.width, s.width = r, s.minWidth = i, s.maxWidth = o)), void 0 !== a ? a + "" : a }

    function T(e, t) {
        return { get: function() {
                return e() ? void delete this.get : (this.get = t).apply(this, arguments) } } }

    function C(e, t) {
        if (t in e) return t;
        for (var n = t[0].toUpperCase() + t.slice(1), r = t, i = Ye.length; i--;)
            if (t = Ye[i] + n, t in e) return t;
        return r }

    function k(e, t, n) {
        var r = Xe.exec(t);
        return r ? Math.max(0, r[1] - (n || 0)) + (r[2] || "px") : t }

    function E(e, t, n, r, i) {
        for (var o = n === (r ? "border" : "content") ? 4 : "width" === t ? 1 : 0, a = 0; 4 > o; o += 2) "margin" === n && (a += J.css(e, n + Te[o], !0, i)), r ? ("content" === n && (a -= J.css(e, "padding" + Te[o], !0, i)), "margin" !== n && (a -= J.css(e, "border" + Te[o] + "Width", !0, i))) : (a += J.css(e, "padding" + Te[o], !0, i), "padding" !== n && (a += J.css(e, "border" + Te[o] + "Width", !0, i)));
        return a }

    function S(e, t, n) {
        var r = !0,
            i = "width" === t ? e.offsetWidth : e.offsetHeight,
            o = Be(e),
            a = "border-box" === J.css(e, "boxSizing", !1, o);
        if (0 >= i || null == i) {
            if (i = w(e, t, o), (0 > i || null == i) && (i = e.style[t]), $e.test(i)) return i;
            r = a && (G.boxSizingReliable() || i === e.style[t]), i = parseFloat(i) || 0 }
        return i + E(e, t, n || (a ? "border" : "content"), r, o) + "px" }

    function N(e, t) {
        for (var n, r, i, o = [], a = 0, s = e.length; s > a; a++) r = e[a], r.style && (o[a] = ye.get(r, "olddisplay"), n = r.style.display, t ? (o[a] || "none" !== n || (r.style.display = ""), "" === r.style.display && Ce(r) && (o[a] = ye.access(r, "olddisplay", b(r.nodeName)))) : (i = Ce(r), "none" === n && i || ye.set(r, "olddisplay", i ? n : J.css(r, "display"))));
        for (a = 0; s > a; a++) r = e[a], r.style && (t && "none" !== r.style.display && "" !== r.style.display || (r.style.display = t ? o[a] || "" : "none"));
        return e }

    function j(e, t, n, r, i) {
        return new j.prototype.init(e, t, n, r, i) }

    function D() {
        return setTimeout(function() { Ge = void 0 }), Ge = J.now() }

    function A(e, t) {
        var n, r = 0,
            i = { height: e };
        for (t = t ? 1 : 0; 4 > r; r += 2 - t) n = Te[r], i["margin" + n] = i["padding" + n] = e;
        return t && (i.opacity = i.width = e), i }

    function L(e, t, n) {
        for (var r, i = (nt[t] || []).concat(nt["*"]), o = 0, a = i.length; a > o; o++)
            if (r = i[o].call(n, t, e)) return r }

    function H(e, t, n) {
        var r, i, o, a, s, l, c, u, f = this,
            p = {},
            d = e.style,
            h = e.nodeType && Ce(e),
            g = ye.get(e, "fxshow");
        n.queue || (s = J._queueHooks(e, "fx"), null == s.unqueued && (s.unqueued = 0, l = s.empty.fire, s.empty.fire = function() { s.unqueued || l() }), s.unqueued++, f.always(function() { f.always(function() { s.unqueued--, J.queue(e, "fx").length || s.empty.fire() }) })), 1 === e.nodeType && ("height" in t || "width" in t) && (n.overflow = [d.overflow, d.overflowX, d.overflowY], c = J.css(e, "display"), u = "none" === c ? ye.get(e, "olddisplay") || b(e.nodeName) : c, "inline" === u && "none" === J.css(e, "float") && (d.display = "inline-block")), n.overflow && (d.overflow = "hidden", f.always(function() { d.overflow = n.overflow[0], d.overflowX = n.overflow[1], d.overflowY = n.overflow[2] }));
        for (r in t)
            if (i = t[r], Qe.exec(i)) {
                if (delete t[r], o = o || "toggle" === i, i === (h ? "hide" : "show")) {
                    if ("show" !== i || !g || void 0 === g[r]) continue;
                    h = !0 }
                p[r] = g && g[r] || J.style(e, r) } else c = void 0;
        if (J.isEmptyObject(p)) "inline" === ("none" === c ? b(e.nodeName) : c) && (d.display = c);
        else { g ? "hidden" in g && (h = g.hidden) : g = ye.access(e, "fxshow", {}), o && (g.hidden = !h), h ? J(e).show() : f.done(function() { J(e).hide() }), f.done(function() {
                var t;
                ye.remove(e, "fxshow");
                for (t in p) J.style(e, t, p[t]) });
            for (r in p) a = L(h ? g[r] : 0, r, f), r in g || (g[r] = a.start, h && (a.end = a.start, a.start = "width" === r || "height" === r ? 1 : 0)) } }

    function O(e, t) {
        var n, r, i, o, a;
        for (n in e)
            if (r = J.camelCase(n), i = t[r], o = e[n], J.isArray(o) && (i = o[1], o = e[n] = o[0]), n !== r && (e[r] = o, delete e[n]), a = J.cssHooks[r], a && "expand" in a) { o = a.expand(o), delete e[r];
                for (n in o) n in e || (e[n] = o[n], t[n] = i) } else t[r] = i }

    function q(e, t, n) {
        var r, i, o = 0,
            a = tt.length,
            s = J.Deferred().always(function() { delete l.elem }),
            l = function() {
                if (i) return !1;
                for (var t = Ge || D(), n = Math.max(0, c.startTime + c.duration - t), r = n / c.duration || 0, o = 1 - r, a = 0, l = c.tweens.length; l > a; a++) c.tweens[a].run(o);
                return s.notifyWith(e, [c, o, n]), 1 > o && l ? n : (s.resolveWith(e, [c]), !1) },
            c = s.promise({ elem: e, props: J.extend({}, t), opts: J.extend(!0, { specialEasing: {} }, n), originalProperties: t, originalOptions: n, startTime: Ge || D(), duration: n.duration, tweens: [], createTween: function(t, n) {
                    var r = J.Tween(e, c.opts, t, n, c.opts.specialEasing[t] || c.opts.easing);
                    return c.tweens.push(r), r }, stop: function(t) {
                    var n = 0,
                        r = t ? c.tweens.length : 0;
                    if (i) return this;
                    for (i = !0; r > n; n++) c.tweens[n].run(1);
                    return t ? s.resolveWith(e, [c, t]) : s.rejectWith(e, [c, t]), this } }),
            u = c.props;
        for (O(u, c.opts.specialEasing); a > o; o++)
            if (r = tt[o].call(c, e, u, c.opts)) return r;
        return J.map(u, L, c), J.isFunction(c.opts.start) && c.opts.start.call(e, c), J.fx.timer(J.extend(l, { elem: e, anim: c, queue: c.opts.queue })), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always) }

    function P(e) {
        return function(t, n) { "string" != typeof t && (n = t, t = "*");
            var r, i = 0,
                o = t.toLowerCase().match(de) || [];
            if (J.isFunction(n))
                for (; r = o[i++];) "+" === r[0] ? (r = r.slice(1) || "*", (e[r] = e[r] || []).unshift(n)) : (e[r] = e[r] || []).push(n) } }

    function R(e, t, n, r) {
        function i(s) {
            var l;
            return o[s] = !0, J.each(e[s] || [], function(e, s) {
                var c = s(t, n, r);
                return "string" != typeof c || a || o[c] ? a ? !(l = c) : void 0 : (t.dataTypes.unshift(c), i(c), !1) }), l }
        var o = {},
            a = e === xt;
        return i(t.dataTypes[0]) || !o["*"] && i("*") }

    function W(e, t) {
        var n, r, i = J.ajaxSettings.flatOptions || {};
        for (n in t) void 0 !== t[n] && ((i[n] ? e : r || (r = {}))[n] = t[n]);
        return r && J.extend(!0, e, r), e }

    function M(e, t, n) {
        for (var r, i, o, a, s = e.contents, l = e.dataTypes;
            "*" === l[0];) l.shift(), void 0 === r && (r = e.mimeType || t.getResponseHeader("Content-Type"));
        if (r)
            for (i in s)
                if (s[i] && s[i].test(r)) { l.unshift(i);
                    break }
        if (l[0] in n) o = l[0];
        else {
            for (i in n) {
                if (!l[0] || e.converters[i + " " + l[0]]) { o = i;
                    break }
                a || (a = i) }
            o = o || a }
        return o ? (o !== l[0] && l.unshift(o), n[o]) : void 0 }

    function F(e, t, n, r) {
        var i, o, a, s, l, c = {},
            u = e.dataTypes.slice();
        if (u[1])
            for (a in e.converters) c[a.toLowerCase()] = e.converters[a];
        for (o = u.shift(); o;)
            if (e.responseFields[o] && (n[e.responseFields[o]] = t), !l && r && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = o, o = u.shift())
                if ("*" === o) o = l;
                else if ("*" !== l && l !== o) {
            if (a = c[l + " " + o] || c["* " + o], !a)
                for (i in c)
                    if (s = i.split(" "), s[1] === o && (a = c[l + " " + s[0]] || c["* " + s[0]])) { a === !0 ? a = c[i] : c[i] !== !0 && (o = s[0], u.unshift(s[1]));
                        break }
            if (a !== !0)
                if (a && e["throws"]) t = a(t);
                else try { t = a(t) } catch (f) {
                    return { state: "parsererror", error: a ? f : "No conversion from " + l + " to " + o } } }
        return { state: "success", data: t } }

    function _(e, t, n, r) {
        var i;
        if (J.isArray(t)) J.each(t, function(t, i) { n || kt.test(e) ? r(e, i) : _(e + "[" + ("object" == typeof i ? t : "") + "]", i, n, r) });
        else if (n || "object" !== J.type(t)) r(e, t);
        else
            for (i in t) _(e + "[" + i + "]", t[i], n, r) }

    function I(e) {
        return J.isWindow(e) ? e : 9 === e.nodeType && e.defaultView }
    var $ = [],
        B = $.slice,
        z = $.concat,
        X = $.push,
        V = $.indexOf,
        U = {},
        K = U.toString,
        Y = U.hasOwnProperty,
        G = {},
        Z = e.document,
        Q = "2.1.4",
        J = function(e, t) {
            return new J.fn.init(e, t) },
        ee = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        te = /^-ms-/,
        ne = /-([\da-z])/gi,
        re = function(e, t) {
            return t.toUpperCase() };
    J.fn = J.prototype = { jquery: Q, constructor: J, selector: "", length: 0, toArray: function() {
            return B.call(this) }, get: function(e) {
            return null != e ? 0 > e ? this[e + this.length] : this[e] : B.call(this) }, pushStack: function(e) {
            var t = J.merge(this.constructor(), e);
            return t.prevObject = this, t.context = this.context, t }, each: function(e, t) {
            return J.each(this, e, t) }, map: function(e) {
            return this.pushStack(J.map(this, function(t, n) {
                return e.call(t, n, t) })) }, slice: function() {
            return this.pushStack(B.apply(this, arguments)) }, first: function() {
            return this.eq(0) }, last: function() {
            return this.eq(-1) }, eq: function(e) {
            var t = this.length,
                n = +e + (0 > e ? t : 0);
            return this.pushStack(n >= 0 && t > n ? [this[n]] : []) }, end: function() {
            return this.prevObject || this.constructor(null) }, push: X, sort: $.sort, splice: $.splice }, J.extend = J.fn.extend = function() {
        var e, t, n, r, i, o, a = arguments[0] || {},
            s = 1,
            l = arguments.length,
            c = !1;
        for ("boolean" == typeof a && (c = a, a = arguments[s] || {}, s++), "object" == typeof a || J.isFunction(a) || (a = {}), s === l && (a = this, s--); l > s; s++)
            if (null != (e = arguments[s]))
                for (t in e) n = a[t], r = e[t], a !== r && (c && r && (J.isPlainObject(r) || (i = J.isArray(r))) ? (i ? (i = !1, o = n && J.isArray(n) ? n : []) : o = n && J.isPlainObject(n) ? n : {}, a[t] = J.extend(c, o, r)) : void 0 !== r && (a[t] = r));
        return a }, J.extend({ expando: "jQuery" + (Q + Math.random()).replace(/\D/g, ""), isReady: !0, error: function(e) {
            throw new Error(e) }, noop: function() {}, isFunction: function(e) {
            return "function" === J.type(e) }, isArray: Array.isArray, isWindow: function(e) {
            return null != e && e === e.window }, isNumeric: function(e) {
            return !J.isArray(e) && e - parseFloat(e) + 1 >= 0 }, isPlainObject: function(e) {
            return "object" !== J.type(e) || e.nodeType || J.isWindow(e) ? !1 : e.constructor && !Y.call(e.constructor.prototype, "isPrototypeOf") ? !1 : !0 }, isEmptyObject: function(e) {
            var t;
            for (t in e) return !1;
            return !0 }, type: function(e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? U[K.call(e)] || "object" : typeof e }, globalEval: function(e) {
            var t, n = eval;
            e = J.trim(e), e && (1 === e.indexOf("use strict") ? (t = Z.createElement("script"), t.text = e, Z.head.appendChild(t).parentNode.removeChild(t)) : n(e)) }, camelCase: function(e) {
            return e.replace(te, "ms-").replace(ne, re) }, nodeName: function(e, t) {
            return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase() }, each: function(e, t, r) {
            var i, o = 0,
                a = e.length,
                s = n(e);
            if (r) {
                if (s)
                    for (; a > o && (i = t.apply(e[o], r), i !== !1); o++);
                else
                    for (o in e)
                        if (i = t.apply(e[o], r), i === !1) break } else if (s)
                for (; a > o && (i = t.call(e[o], o, e[o]), i !== !1); o++);
            else
                for (o in e)
                    if (i = t.call(e[o], o, e[o]), i === !1) break; return e }, trim: function(e) {
            return null == e ? "" : (e + "").replace(ee, "") }, makeArray: function(e, t) {
            var r = t || [];
            return null != e && (n(Object(e)) ? J.merge(r, "string" == typeof e ? [e] : e) : X.call(r, e)), r }, inArray: function(e, t, n) {
            return null == t ? -1 : V.call(t, e, n) }, merge: function(e, t) {
            for (var n = +t.length, r = 0, i = e.length; n > r; r++) e[i++] = t[r];
            return e.length = i, e }, grep: function(e, t, n) {
            for (var r, i = [], o = 0, a = e.length, s = !n; a > o; o++) r = !t(e[o], o), r !== s && i.push(e[o]);
            return i }, map: function(e, t, r) {
            var i, o = 0,
                a = e.length,
                s = n(e),
                l = [];
            if (s)
                for (; a > o; o++) i = t(e[o], o, r), null != i && l.push(i);
            else
                for (o in e) i = t(e[o], o, r), null != i && l.push(i);
            return z.apply([], l) }, guid: 1, proxy: function(e, t) {
            var n, r, i;
            return "string" == typeof t && (n = e[t], t = e, e = n), J.isFunction(e) ? (r = B.call(arguments, 2), i = function() {
                return e.apply(t || this, r.concat(B.call(arguments))) }, i.guid = e.guid = e.guid || J.guid++, i) : void 0 }, now: Date.now, support: G }), J.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(e, t) { U["[object " + t + "]"] = t.toLowerCase() });
    var ie = function(e) {
        function t(e, t, n, r) {
            var i, o, a, s, l, c, f, d, h, g;
            if ((t ? t.ownerDocument || t : _) !== H && L(t), t = t || H, n = n || [], s = t.nodeType, "string" != typeof e || !e || 1 !== s && 9 !== s && 11 !== s) return n;
            if (!r && q) {
                if (11 !== s && (i = ve.exec(e)))
                    if (a = i[1]) {
                        if (9 === s) {
                            if (o = t.getElementById(a), !o || !o.parentNode) return n;
                            if (o.id === a) return n.push(o), n } else if (t.ownerDocument && (o = t.ownerDocument.getElementById(a)) && M(t, o) && o.id === a) return n.push(o), n } else {
                        if (i[2]) return Q.apply(n, t.getElementsByTagName(e)), n;
                        if ((a = i[3]) && w.getElementsByClassName) return Q.apply(n, t.getElementsByClassName(a)), n }
                if (w.qsa && (!P || !P.test(e))) {
                    if (d = f = F, h = t, g = 1 !== s && e, 1 === s && "object" !== t.nodeName.toLowerCase()) {
                        for (c = E(e), (f = t.getAttribute("id")) ? d = f.replace(be, "\\$&") : t.setAttribute("id", d), d = "[id='" + d + "'] ", l = c.length; l--;) c[l] = d + p(c[l]);
                        h = xe.test(e) && u(t.parentNode) || t, g = c.join(",") }
                    if (g) try {
                        return Q.apply(n, h.querySelectorAll(g)), n } catch (m) {} finally { f || t.removeAttribute("id") } } }
            return N(e.replace(le, "$1"), t, n, r) }

        function n() {
            function e(n, r) {
                return t.push(n + " ") > T.cacheLength && delete e[t.shift()], e[n + " "] = r }
            var t = [];
            return e }

        function r(e) {
            return e[F] = !0, e }

        function i(e) {
            var t = H.createElement("div");
            try {
                return !!e(t) } catch (n) {
                return !1 } finally { t.parentNode && t.parentNode.removeChild(t), t = null } }

        function o(e, t) {
            for (var n = e.split("|"), r = e.length; r--;) T.attrHandle[n[r]] = t }

        function a(e, t) {
            var n = t && e,
                r = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || U) - (~e.sourceIndex || U);
            if (r) return r;
            if (n)
                for (; n = n.nextSibling;)
                    if (n === t) return -1;
            return e ? 1 : -1 }

        function s(e) {
            return function(t) {
                var n = t.nodeName.toLowerCase();
                return "input" === n && t.type === e } }

        function l(e) {
            return function(t) {
                var n = t.nodeName.toLowerCase();
                return ("input" === n || "button" === n) && t.type === e } }

        function c(e) {
            return r(function(t) {
                return t = +t, r(function(n, r) {
                    for (var i, o = e([], n.length, t), a = o.length; a--;) n[i = o[a]] && (n[i] = !(r[i] = n[i])) }) }) }

        function u(e) {
            return e && "undefined" != typeof e.getElementsByTagName && e }

        function f() {}

        function p(e) {
            for (var t = 0, n = e.length, r = ""; n > t; t++) r += e[t].value;
            return r }

        function d(e, t, n) {
            var r = t.dir,
                i = n && "parentNode" === r,
                o = $++;
            return t.first ? function(t, n, o) {
                for (; t = t[r];)
                    if (1 === t.nodeType || i) return e(t, n, o) } : function(t, n, a) {
                var s, l, c = [I, o];
                if (a) {
                    for (; t = t[r];)
                        if ((1 === t.nodeType || i) && e(t, n, a)) return !0 } else
                    for (; t = t[r];)
                        if (1 === t.nodeType || i) {
                            if (l = t[F] || (t[F] = {}), (s = l[r]) && s[0] === I && s[1] === o) return c[2] = s[2];
                            if (l[r] = c, c[2] = e(t, n, a)) return !0 } } }

        function h(e) {
            return e.length > 1 ? function(t, n, r) {
                for (var i = e.length; i--;)
                    if (!e[i](t, n, r)) return !1;
                return !0 } : e[0] }

        function g(e, n, r) {
            for (var i = 0, o = n.length; o > i; i++) t(e, n[i], r);
            return r }

        function m(e, t, n, r, i) {
            for (var o, a = [], s = 0, l = e.length, c = null != t; l > s; s++)(o = e[s]) && (!n || n(o, r, i)) && (a.push(o), c && t.push(s));
            return a }

        function y(e, t, n, i, o, a) {
            return i && !i[F] && (i = y(i)), o && !o[F] && (o = y(o, a)), r(function(r, a, s, l) {
                var c, u, f, p = [],
                    d = [],
                    h = a.length,
                    y = r || g(t || "*", s.nodeType ? [s] : s, []),
                    v = !e || !r && t ? y : m(y, p, e, s, l),
                    x = n ? o || (r ? e : h || i) ? [] : a : v;
                if (n && n(v, x, s, l), i)
                    for (c = m(x, d), i(c, [], s, l), u = c.length; u--;)(f = c[u]) && (x[d[u]] = !(v[d[u]] = f));
                if (r) {
                    if (o || e) {
                        if (o) {
                            for (c = [], u = x.length; u--;)(f = x[u]) && c.push(v[u] = f);
                            o(null, x = [], c, l) }
                        for (u = x.length; u--;)(f = x[u]) && (c = o ? ee(r, f) : p[u]) > -1 && (r[c] = !(a[c] = f)) } } else x = m(x === a ? x.splice(h, x.length) : x), o ? o(null, a, x, l) : Q.apply(a, x) }) }

        function v(e) {
            for (var t, n, r, i = e.length, o = T.relative[e[0].type], a = o || T.relative[" "], s = o ? 1 : 0, l = d(function(e) {
                    return e === t }, a, !0), c = d(function(e) {
                    return ee(t, e) > -1 }, a, !0), u = [function(e, n, r) {
                    var i = !o && (r || n !== j) || ((t = n).nodeType ? l(e, n, r) : c(e, n, r));
                    return t = null, i }]; i > s; s++)
                if (n = T.relative[e[s].type]) u = [d(h(u), n)];
                else {
                    if (n = T.filter[e[s].type].apply(null, e[s].matches), n[F]) {
                        for (r = ++s; i > r && !T.relative[e[r].type]; r++);
                        return y(s > 1 && h(u), s > 1 && p(e.slice(0, s - 1).concat({ value: " " === e[s - 2].type ? "*" : "" })).replace(le, "$1"), n, r > s && v(e.slice(s, r)), i > r && v(e = e.slice(r)), i > r && p(e)) }
                    u.push(n) }
            return h(u) }

        function x(e, n) {
            var i = n.length > 0,
                o = e.length > 0,
                a = function(r, a, s, l, c) {
                    var u, f, p, d = 0,
                        h = "0",
                        g = r && [],
                        y = [],
                        v = j,
                        x = r || o && T.find.TAG("*", c),
                        b = I += null == v ? 1 : Math.random() || .1,
                        w = x.length;
                    for (c && (j = a !== H && a); h !== w && null != (u = x[h]); h++) {
                        if (o && u) {
                            for (f = 0; p = e[f++];)
                                if (p(u, a, s)) { l.push(u);
                                    break }
                            c && (I = b) }
                        i && ((u = !p && u) && d--, r && g.push(u)) }
                    if (d += h, i && h !== d) {
                        for (f = 0; p = n[f++];) p(g, y, a, s);
                        if (r) {
                            if (d > 0)
                                for (; h--;) g[h] || y[h] || (y[h] = G.call(l));
                            y = m(y) }
                        Q.apply(l, y), c && !r && y.length > 0 && d + n.length > 1 && t.uniqueSort(l) }
                    return c && (I = b, j = v), g };
            return i ? r(a) : a }
        var b, w, T, C, k, E, S, N, j, D, A, L, H, O, q, P, R, W, M, F = "sizzle" + 1 * new Date,
            _ = e.document,
            I = 0,
            $ = 0,
            B = n(),
            z = n(),
            X = n(),
            V = function(e, t) {
                return e === t && (A = !0), 0 },
            U = 1 << 31,
            K = {}.hasOwnProperty,
            Y = [],
            G = Y.pop,
            Z = Y.push,
            Q = Y.push,
            J = Y.slice,
            ee = function(e, t) {
                for (var n = 0, r = e.length; r > n; n++)
                    if (e[n] === t) return n;
                return -1 },
            te = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            ne = "[\\x20\\t\\r\\n\\f]",
            re = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
            ie = re.replace("w", "w#"),
            oe = "\\[" + ne + "*(" + re + ")(?:" + ne + "*([*^$|!~]?=)" + ne + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + ie + "))|)" + ne + "*\\]",
            ae = ":(" + re + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + oe + ")*)|.*)\\)|)",
            se = new RegExp(ne + "+", "g"),
            le = new RegExp("^" + ne + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ne + "+$", "g"),
            ce = new RegExp("^" + ne + "*," + ne + "*"),
            ue = new RegExp("^" + ne + "*([>+~]|" + ne + ")" + ne + "*"),
            fe = new RegExp("=" + ne + "*([^\\]'\"]*?)" + ne + "*\\]", "g"),
            pe = new RegExp(ae),
            de = new RegExp("^" + ie + "$"),
            he = { ID: new RegExp("^#(" + re + ")"), CLASS: new RegExp("^\\.(" + re + ")"), TAG: new RegExp("^(" + re.replace("w", "w*") + ")"), ATTR: new RegExp("^" + oe), PSEUDO: new RegExp("^" + ae), CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ne + "*(even|odd|(([+-]|)(\\d*)n|)" + ne + "*(?:([+-]|)" + ne + "*(\\d+)|))" + ne + "*\\)|)", "i"), bool: new RegExp("^(?:" + te + ")$", "i"), needsContext: new RegExp("^" + ne + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ne + "*((?:-\\d)?\\d*)" + ne + "*\\)|)(?=[^-]|$)", "i") },
            ge = /^(?:input|select|textarea|button)$/i,
            me = /^h\d$/i,
            ye = /^[^{]+\{\s*\[native \w/,
            ve = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            xe = /[+~]/,
            be = /'|\\/g,
            we = new RegExp("\\\\([\\da-f]{1,6}" + ne + "?|(" + ne + ")|.)", "ig"),
            Te = function(e, t, n) {
                var r = "0x" + t - 65536;
                return r !== r || n ? t : 0 > r ? String.fromCharCode(r + 65536) : String.fromCharCode(r >> 10 | 55296, 1023 & r | 56320) },
            Ce = function() { L() };
        try { Q.apply(Y = J.call(_.childNodes), _.childNodes), Y[_.childNodes.length].nodeType } catch (ke) { Q = { apply: Y.length ? function(e, t) { Z.apply(e, J.call(t)) } : function(e, t) {
                    for (var n = e.length, r = 0; e[n++] = t[r++];);
                    e.length = n - 1 } } }
        w = t.support = {}, k = t.isXML = function(e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return t ? "HTML" !== t.nodeName : !1 }, L = t.setDocument = function(e) {
            var t, n, r = e ? e.ownerDocument || e : _;
            return r !== H && 9 === r.nodeType && r.documentElement ? (H = r, O = r.documentElement, n = r.defaultView, n && n !== n.top && (n.addEventListener ? n.addEventListener("unload", Ce, !1) : n.attachEvent && n.attachEvent("onunload", Ce)), q = !k(r), w.attributes = i(function(e) {
                return e.className = "i", !e.getAttribute("className") }), w.getElementsByTagName = i(function(e) {
                return e.appendChild(r.createComment("")), !e.getElementsByTagName("*").length }), w.getElementsByClassName = ye.test(r.getElementsByClassName), w.getById = i(function(e) {
                return O.appendChild(e).id = F, !r.getElementsByName || !r.getElementsByName(F).length }), w.getById ? (T.find.ID = function(e, t) {
                if ("undefined" != typeof t.getElementById && q) {
                    var n = t.getElementById(e);
                    return n && n.parentNode ? [n] : [] } }, T.filter.ID = function(e) {
                var t = e.replace(we, Te);
                return function(e) {
                    return e.getAttribute("id") === t } }) : (delete T.find.ID, T.filter.ID = function(e) {
                var t = e.replace(we, Te);
                return function(e) {
                    var n = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
                    return n && n.value === t } }), T.find.TAG = w.getElementsByTagName ? function(e, t) {
                return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : w.qsa ? t.querySelectorAll(e) : void 0 } : function(e, t) {
                var n, r = [],
                    i = 0,
                    o = t.getElementsByTagName(e);
                if ("*" === e) {
                    for (; n = o[i++];) 1 === n.nodeType && r.push(n);
                    return r }
                return o }, T.find.CLASS = w.getElementsByClassName && function(e, t) {
                return q ? t.getElementsByClassName(e) : void 0 }, R = [], P = [], (w.qsa = ye.test(r.querySelectorAll)) && (i(function(e) { O.appendChild(e).innerHTML = "<a id='" + F + "'></a><select id='" + F + "-\f]' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && P.push("[*^$]=" + ne + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || P.push("\\[" + ne + "*(?:value|" + te + ")"), e.querySelectorAll("[id~=" + F + "-]").length || P.push("~="), e.querySelectorAll(":checked").length || P.push(":checked"), e.querySelectorAll("a#" + F + "+*").length || P.push(".#.+[+~]") }), i(function(e) {
                var t = r.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && P.push("name" + ne + "*[*^$|!~]?="), e.querySelectorAll(":enabled").length || P.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), P.push(",.*:") })), (w.matchesSelector = ye.test(W = O.matches || O.webkitMatchesSelector || O.mozMatchesSelector || O.oMatchesSelector || O.msMatchesSelector)) && i(function(e) { w.disconnectedMatch = W.call(e, "div"), W.call(e, "[s!='']:x"), R.push("!=", ae) }), P = P.length && new RegExp(P.join("|")), R = R.length && new RegExp(R.join("|")), t = ye.test(O.compareDocumentPosition), M = t || ye.test(O.contains) ? function(e, t) {
                var n = 9 === e.nodeType ? e.documentElement : e,
                    r = t && t.parentNode;
                return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r))) } : function(e, t) {
                if (t)
                    for (; t = t.parentNode;)
                        if (t === e) return !0;
                return !1 }, V = t ? function(e, t) {
                if (e === t) return A = !0, 0;
                var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return n ? n : (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1, 1 & n || !w.sortDetached && t.compareDocumentPosition(e) === n ? e === r || e.ownerDocument === _ && M(_, e) ? -1 : t === r || t.ownerDocument === _ && M(_, t) ? 1 : D ? ee(D, e) - ee(D, t) : 0 : 4 & n ? -1 : 1) } : function(e, t) {
                if (e === t) return A = !0, 0;
                var n, i = 0,
                    o = e.parentNode,
                    s = t.parentNode,
                    l = [e],
                    c = [t];
                if (!o || !s) return e === r ? -1 : t === r ? 1 : o ? -1 : s ? 1 : D ? ee(D, e) - ee(D, t) : 0;
                if (o === s) return a(e, t);
                for (n = e; n = n.parentNode;) l.unshift(n);
                for (n = t; n = n.parentNode;) c.unshift(n);
                for (; l[i] === c[i];) i++;
                return i ? a(l[i], c[i]) : l[i] === _ ? -1 : c[i] === _ ? 1 : 0 }, r) : H }, t.matches = function(e, n) {
            return t(e, null, null, n) }, t.matchesSelector = function(e, n) {
            if ((e.ownerDocument || e) !== H && L(e), n = n.replace(fe, "='$1']"), !(!w.matchesSelector || !q || R && R.test(n) || P && P.test(n))) try {
                var r = W.call(e, n);
                if (r || w.disconnectedMatch || e.document && 11 !== e.document.nodeType) return r } catch (i) {}
            return t(n, H, null, [e]).length > 0 }, t.contains = function(e, t) {
            return (e.ownerDocument || e) !== H && L(e), M(e, t) }, t.attr = function(e, t) {
            (e.ownerDocument || e) !== H && L(e);
            var n = T.attrHandle[t.toLowerCase()],
                r = n && K.call(T.attrHandle, t.toLowerCase()) ? n(e, t, !q) : void 0;
            return void 0 !== r ? r : w.attributes || !q ? e.getAttribute(t) : (r = e.getAttributeNode(t)) && r.specified ? r.value : null }, t.error = function(e) {
            throw new Error("Syntax error, unrecognized expression: " + e) }, t.uniqueSort = function(e) {
            var t, n = [],
                r = 0,
                i = 0;
            if (A = !w.detectDuplicates, D = !w.sortStable && e.slice(0), e.sort(V), A) {
                for (; t = e[i++];) t === e[i] && (r = n.push(i));
                for (; r--;) e.splice(n[r], 1) }
            return D = null, e }, C = t.getText = function(e) {
            var t, n = "",
                r = 0,
                i = e.nodeType;
            if (i) {
                if (1 === i || 9 === i || 11 === i) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) n += C(e) } else if (3 === i || 4 === i) return e.nodeValue } else
                for (; t = e[r++];) n += C(t);
            return n }, T = t.selectors = { cacheLength: 50, createPseudo: r, match: he, attrHandle: {}, find: {}, relative: { ">": { dir: "parentNode", first: !0 }, " ": { dir: "parentNode" }, "+": { dir: "previousSibling", first: !0 }, "~": { dir: "previousSibling" } }, preFilter: { ATTR: function(e) {
                    return e[1] = e[1].replace(we, Te), e[3] = (e[3] || e[4] || e[5] || "").replace(we, Te), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4) }, CHILD: function(e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e }, PSEUDO: function(e) {
                    var t, n = !e[6] && e[2];
                    return he.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && pe.test(n) && (t = E(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3)) } }, filter: { TAG: function(e) {
                    var t = e.replace(we, Te).toLowerCase();
                    return "*" === e ? function() {
                        return !0 } : function(e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t } }, CLASS: function(e) {
                    var t = B[e + " "];
                    return t || (t = new RegExp("(^|" + ne + ")" + e + "(" + ne + "|$)")) && B(e, function(e) {
                        return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "") }) }, ATTR: function(e, n, r) {
                    return function(i) {
                        var o = t.attr(i, e);
                        return null == o ? "!=" === n : n ? (o += "", "=" === n ? o === r : "!=" === n ? o !== r : "^=" === n ? r && 0 === o.indexOf(r) : "*=" === n ? r && o.indexOf(r) > -1 : "$=" === n ? r && o.slice(-r.length) === r : "~=" === n ? (" " + o.replace(se, " ") + " ").indexOf(r) > -1 : "|=" === n ? o === r || o.slice(0, r.length + 1) === r + "-" : !1) : !0 } }, CHILD: function(e, t, n, r, i) {
                    var o = "nth" !== e.slice(0, 3),
                        a = "last" !== e.slice(-4),
                        s = "of-type" === t;
                    return 1 === r && 0 === i ? function(e) {
                        return !!e.parentNode } : function(t, n, l) {
                        var c, u, f, p, d, h, g = o !== a ? "nextSibling" : "previousSibling",
                            m = t.parentNode,
                            y = s && t.nodeName.toLowerCase(),
                            v = !l && !s;
                        if (m) {
                            if (o) {
                                for (; g;) {
                                    for (f = t; f = f[g];)
                                        if (s ? f.nodeName.toLowerCase() === y : 1 === f.nodeType) return !1;
                                    h = g = "only" === e && !h && "nextSibling" }
                                return !0 }
                            if (h = [a ? m.firstChild : m.lastChild], a && v) {
                                for (u = m[F] || (m[F] = {}), c = u[e] || [], d = c[0] === I && c[1], p = c[0] === I && c[2], f = d && m.childNodes[d]; f = ++d && f && f[g] || (p = d = 0) || h.pop();)
                                    if (1 === f.nodeType && ++p && f === t) { u[e] = [I, d, p];
                                        break } } else if (v && (c = (t[F] || (t[F] = {}))[e]) && c[0] === I) p = c[1];
                            else
                                for (;
                                    (f = ++d && f && f[g] || (p = d = 0) || h.pop()) && ((s ? f.nodeName.toLowerCase() !== y : 1 !== f.nodeType) || !++p || (v && ((f[F] || (f[F] = {}))[e] = [I, p]), f !== t)););
                            return p -= i, p === r || p % r === 0 && p / r >= 0 } } }, PSEUDO: function(e, n) {
                    var i, o = T.pseudos[e] || T.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                    return o[F] ? o(n) : o.length > 1 ? (i = [e, e, "", n], T.setFilters.hasOwnProperty(e.toLowerCase()) ? r(function(e, t) {
                        for (var r, i = o(e, n), a = i.length; a--;) r = ee(e, i[a]), e[r] = !(t[r] = i[a]) }) : function(e) {
                        return o(e, 0, i) }) : o } }, pseudos: { not: r(function(e) {
                    var t = [],
                        n = [],
                        i = S(e.replace(le, "$1"));
                    return i[F] ? r(function(e, t, n, r) {
                        for (var o, a = i(e, null, r, []), s = e.length; s--;)(o = a[s]) && (e[s] = !(t[s] = o)) }) : function(e, r, o) {
                        return t[0] = e, i(t, null, o, n), t[0] = null, !n.pop() } }), has: r(function(e) {
                    return function(n) {
                        return t(e, n).length > 0 } }), contains: r(function(e) {
                    return e = e.replace(we, Te),
                        function(t) {
                            return (t.textContent || t.innerText || C(t)).indexOf(e) > -1 } }), lang: r(function(e) {
                    return de.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(we, Te).toLowerCase(),
                        function(t) {
                            var n;
                            do
                                if (n = q ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return n = n.toLowerCase(), n === e || 0 === n.indexOf(e + "-");
                            while ((t = t.parentNode) && 1 === t.nodeType);
                            return !1 } }), target: function(t) {
                    var n = e.location && e.location.hash;
                    return n && n.slice(1) === t.id }, root: function(e) {
                    return e === O }, focus: function(e) {
                    return e === H.activeElement && (!H.hasFocus || H.hasFocus()) && !!(e.type || e.href || ~e.tabIndex) }, enabled: function(e) {
                    return e.disabled === !1 }, disabled: function(e) {
                    return e.disabled === !0 }, checked: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected }, selected: function(e) {
                    return e.parentNode && e.parentNode.selectedIndex, e.selected === !0 }, empty: function(e) {
                    for (e = e.firstChild; e; e = e.nextSibling)
                        if (e.nodeType < 6) return !1;
                    return !0 }, parent: function(e) {
                    return !T.pseudos.empty(e) }, header: function(e) {
                    return me.test(e.nodeName) }, input: function(e) {
                    return ge.test(e.nodeName) }, button: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t }, text: function(e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase()) }, first: c(function() {
                    return [0] }), last: c(function(e, t) {
                    return [t - 1] }), eq: c(function(e, t, n) {
                    return [0 > n ? n + t : n] }), even: c(function(e, t) {
                    for (var n = 0; t > n; n += 2) e.push(n);
                    return e }), odd: c(function(e, t) {
                    for (var n = 1; t > n; n += 2) e.push(n);
                    return e }), lt: c(function(e, t, n) {
                    for (var r = 0 > n ? n + t : n; --r >= 0;) e.push(r);
                    return e }), gt: c(function(e, t, n) {
                    for (var r = 0 > n ? n + t : n; ++r < t;) e.push(r);
                    return e }) } }, T.pseudos.nth = T.pseudos.eq;
        for (b in { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }) T.pseudos[b] = s(b);
        for (b in { submit: !0, reset: !0 }) T.pseudos[b] = l(b);
        return f.prototype = T.filters = T.pseudos, T.setFilters = new f, E = t.tokenize = function(e, n) {
            var r, i, o, a, s, l, c, u = z[e + " "];
            if (u) return n ? 0 : u.slice(0);
            for (s = e, l = [], c = T.preFilter; s;) {
                (!r || (i = ce.exec(s))) && (i && (s = s.slice(i[0].length) || s), l.push(o = [])), r = !1, (i = ue.exec(s)) && (r = i.shift(), o.push({ value: r, type: i[0].replace(le, " ") }), s = s.slice(r.length));
                for (a in T.filter) !(i = he[a].exec(s)) || c[a] && !(i = c[a](i)) || (r = i.shift(), o.push({ value: r, type: a, matches: i }), s = s.slice(r.length));
                if (!r) break }
            return n ? s.length : s ? t.error(e) : z(e, l).slice(0) }, S = t.compile = function(e, t) {
            var n, r = [],
                i = [],
                o = X[e + " "];
            if (!o) {
                for (t || (t = E(e)), n = t.length; n--;) o = v(t[n]), o[F] ? r.push(o) : i.push(o);
                o = X(e, x(i, r)), o.selector = e }
            return o }, N = t.select = function(e, t, n, r) {
            var i, o, a, s, l, c = "function" == typeof e && e,
                f = !r && E(e = c.selector || e);
            if (n = n || [], 1 === f.length) {
                if (o = f[0] = f[0].slice(0), o.length > 2 && "ID" === (a = o[0]).type && w.getById && 9 === t.nodeType && q && T.relative[o[1].type]) {
                    if (t = (T.find.ID(a.matches[0].replace(we, Te), t) || [])[0], !t) return n;
                    c && (t = t.parentNode), e = e.slice(o.shift().value.length) }
                for (i = he.needsContext.test(e) ? 0 : o.length; i-- && (a = o[i], !T.relative[s = a.type]);)
                    if ((l = T.find[s]) && (r = l(a.matches[0].replace(we, Te), xe.test(o[0].type) && u(t.parentNode) || t))) {
                        if (o.splice(i, 1), e = r.length && p(o), !e) return Q.apply(n, r), n;
                        break } }
            return (c || S(e, f))(r, t, !q, n, xe.test(e) && u(t.parentNode) || t), n }, w.sortStable = F.split("").sort(V).join("") === F, w.detectDuplicates = !!A, L(), w.sortDetached = i(function(e) {
            return 1 & e.compareDocumentPosition(H.createElement("div")) }), i(function(e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href") }) || o("type|href|height|width", function(e, t, n) {
            return n ? void 0 : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2) }), w.attributes && i(function(e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value") }) || o("value", function(e, t, n) {
            return n || "input" !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue }), i(function(e) {
            return null == e.getAttribute("disabled") }) || o(te, function(e, t, n) {
            var r;
            return n ? void 0 : e[t] === !0 ? t.toLowerCase() : (r = e.getAttributeNode(t)) && r.specified ? r.value : null }), t }(e);
    J.find = ie, J.expr = ie.selectors, J.expr[":"] = J.expr.pseudos, J.unique = ie.uniqueSort, J.text = ie.getText, J.isXMLDoc = ie.isXML, J.contains = ie.contains;
    var oe = J.expr.match.needsContext,
        ae = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        se = /^.[^:#\[\.,]*$/;
    J.filter = function(e, t, n) {
        var r = t[0];
        return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === r.nodeType ? J.find.matchesSelector(r, e) ? [r] : [] : J.find.matches(e, J.grep(t, function(e) {
            return 1 === e.nodeType })) }, J.fn.extend({
        find: function(e) {
            var t, n = this.length,
                r = [],
                i = this;
            if ("string" != typeof e) return this.pushStack(J(e).filter(function() {
                for (t = 0; n > t; t++)
                    if (J.contains(i[t], this)) return !0 }));
            for (t = 0; n > t; t++) J.find(e, i[t], r);
            return r = this.pushStack(n > 1 ? J.unique(r) : r), r.selector = this.selector ? this.selector + " " + e : e, r
        },
        filter: function(e) {
            return this.pushStack(r(this, e || [], !1)) },
        not: function(e) {
            return this.pushStack(r(this, e || [], !0)) },
        is: function(e) {
            return !!r(this, "string" == typeof e && oe.test(e) ? J(e) : e || [], !1).length }
    });
    var le, ce = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
        ue = J.fn.init = function(e, t) {
            var n, r;
            if (!e) return this;
            if ("string" == typeof e) {
                if (n = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [null, e, null] : ce.exec(e), !n || !n[1] && t) return !t || t.jquery ? (t || le).find(e) : this.constructor(t).find(e);
                if (n[1]) {
                    if (t = t instanceof J ? t[0] : t, J.merge(this, J.parseHTML(n[1], t && t.nodeType ? t.ownerDocument || t : Z, !0)), ae.test(n[1]) && J.isPlainObject(t))
                        for (n in t) J.isFunction(this[n]) ? this[n](t[n]) : this.attr(n, t[n]);
                    return this }
                return r = Z.getElementById(n[2]), r && r.parentNode && (this.length = 1, this[0] = r), this.context = Z, this.selector = e, this }
            return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : J.isFunction(e) ? "undefined" != typeof le.ready ? le.ready(e) : e(J) : (void 0 !== e.selector && (this.selector = e.selector, this.context = e.context), J.makeArray(e, this)) };
    ue.prototype = J.fn, le = J(Z);
    var fe = /^(?:parents|prev(?:Until|All))/,
        pe = { children: !0, contents: !0, next: !0, prev: !0 };
    J.extend({ dir: function(e, t, n) {
            for (var r = [], i = void 0 !== n;
                (e = e[t]) && 9 !== e.nodeType;)
                if (1 === e.nodeType) {
                    if (i && J(e).is(n)) break;
                    r.push(e) }
            return r }, sibling: function(e, t) {
            for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
            return n } }), J.fn.extend({ has: function(e) {
            var t = J(e, this),
                n = t.length;
            return this.filter(function() {
                for (var e = 0; n > e; e++)
                    if (J.contains(this, t[e])) return !0 }) }, closest: function(e, t) {
            for (var n, r = 0, i = this.length, o = [], a = oe.test(e) || "string" != typeof e ? J(e, t || this.context) : 0; i > r; r++)
                for (n = this[r]; n && n !== t; n = n.parentNode)
                    if (n.nodeType < 11 && (a ? a.index(n) > -1 : 1 === n.nodeType && J.find.matchesSelector(n, e))) { o.push(n);
                        break }
            return this.pushStack(o.length > 1 ? J.unique(o) : o) }, index: function(e) {
            return e ? "string" == typeof e ? V.call(J(e), this[0]) : V.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1 }, add: function(e, t) {
            return this.pushStack(J.unique(J.merge(this.get(), J(e, t)))) }, addBack: function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e)) } }), J.each({ parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null }, parents: function(e) {
            return J.dir(e, "parentNode") }, parentsUntil: function(e, t, n) {
            return J.dir(e, "parentNode", n) }, next: function(e) {
            return i(e, "nextSibling") }, prev: function(e) {
            return i(e, "previousSibling") }, nextAll: function(e) {
            return J.dir(e, "nextSibling") }, prevAll: function(e) {
            return J.dir(e, "previousSibling") }, nextUntil: function(e, t, n) {
            return J.dir(e, "nextSibling", n) }, prevUntil: function(e, t, n) {
            return J.dir(e, "previousSibling", n) }, siblings: function(e) {
            return J.sibling((e.parentNode || {}).firstChild, e) }, children: function(e) {
            return J.sibling(e.firstChild) }, contents: function(e) {
            return e.contentDocument || J.merge([], e.childNodes) } }, function(e, t) { J.fn[e] = function(n, r) {
            var i = J.map(this, t, n);
            return "Until" !== e.slice(-5) && (r = n), r && "string" == typeof r && (i = J.filter(r, i)), this.length > 1 && (pe[e] || J.unique(i), fe.test(e) && i.reverse()), this.pushStack(i) } });
    var de = /\S+/g,
        he = {};
    J.Callbacks = function(e) { e = "string" == typeof e ? he[e] || o(e) : J.extend({}, e);
        var t, n, r, i, a, s, l = [],
            c = !e.once && [],
            u = function(o) {
                for (t = e.memory && o, n = !0, s = i || 0, i = 0, a = l.length, r = !0; l && a > s; s++)
                    if (l[s].apply(o[0], o[1]) === !1 && e.stopOnFalse) { t = !1;
                        break }
                r = !1, l && (c ? c.length && u(c.shift()) : t ? l = [] : f.disable()) },
            f = { add: function() {
                    if (l) {
                        var n = l.length;! function o(t) { J.each(t, function(t, n) {
                                var r = J.type(n); "function" === r ? e.unique && f.has(n) || l.push(n) : n && n.length && "string" !== r && o(n) }) }(arguments), r ? a = l.length : t && (i = n, u(t)) }
                    return this }, remove: function() {
                    return l && J.each(arguments, function(e, t) {
                        for (var n;
                            (n = J.inArray(t, l, n)) > -1;) l.splice(n, 1), r && (a >= n && a--, s >= n && s--) }), this }, has: function(e) {
                    return e ? J.inArray(e, l) > -1 : !(!l || !l.length) }, empty: function() {
                    return l = [], a = 0, this }, disable: function() {
                    return l = c = t = void 0, this }, disabled: function() {
                    return !l }, lock: function() {
                    return c = void 0, t || f.disable(), this }, locked: function() {
                    return !c }, fireWith: function(e, t) {
                    return !l || n && !c || (t = t || [], t = [e, t.slice ? t.slice() : t], r ? c.push(t) : u(t)), this }, fire: function() {
                    return f.fireWith(this, arguments), this }, fired: function() {
                    return !!n } };
        return f }, J.extend({ Deferred: function(e) {
            var t = [
                    ["resolve", "done", J.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", J.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", J.Callbacks("memory")]
                ],
                n = "pending",
                r = { state: function() {
                        return n }, always: function() {
                        return i.done(arguments).fail(arguments), this }, then: function() {
                        var e = arguments;
                        return J.Deferred(function(n) { J.each(t, function(t, o) {
                                var a = J.isFunction(e[t]) && e[t];
                                i[o[1]](function() {
                                    var e = a && a.apply(this, arguments);
                                    e && J.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[o[0] + "With"](this === r ? n.promise() : this, a ? [e] : arguments) }) }), e = null }).promise() }, promise: function(e) {
                        return null != e ? J.extend(e, r) : r } },
                i = {};
            return r.pipe = r.then, J.each(t, function(e, o) {
                var a = o[2],
                    s = o[3];
                r[o[1]] = a.add, s && a.add(function() { n = s }, t[1 ^ e][2].disable, t[2][2].lock), i[o[0]] = function() {
                    return i[o[0] + "With"](this === i ? r : this, arguments), this }, i[o[0] + "With"] = a.fireWith }), r.promise(i), e && e.call(i, i), i }, when: function(e) {
            var t, n, r, i = 0,
                o = B.call(arguments),
                a = o.length,
                s = 1 !== a || e && J.isFunction(e.promise) ? a : 0,
                l = 1 === s ? e : J.Deferred(),
                c = function(e, n, r) {
                    return function(i) { n[e] = this, r[e] = arguments.length > 1 ? B.call(arguments) : i, r === t ? l.notifyWith(n, r) : --s || l.resolveWith(n, r) } };
            if (a > 1)
                for (t = new Array(a), n = new Array(a), r = new Array(a); a > i; i++) o[i] && J.isFunction(o[i].promise) ? o[i].promise().done(c(i, r, o)).fail(l.reject).progress(c(i, n, t)) : --s;
            return s || l.resolveWith(r, o), l.promise() } });
    var ge;
    J.fn.ready = function(e) {
        return J.ready.promise().done(e), this }, J.extend({ isReady: !1, readyWait: 1, holdReady: function(e) { e ? J.readyWait++ : J.ready(!0) }, ready: function(e) {
            (e === !0 ? --J.readyWait : J.isReady) || (J.isReady = !0, e !== !0 && --J.readyWait > 0 || (ge.resolveWith(Z, [J]), J.fn.triggerHandler && (J(Z).triggerHandler("ready"), J(Z).off("ready")))) } }), J.ready.promise = function(t) {
        return ge || (ge = J.Deferred(), "complete" === Z.readyState ? setTimeout(J.ready) : (Z.addEventListener("DOMContentLoaded", a, !1), e.addEventListener("load", a, !1))), ge.promise(t) }, J.ready.promise();
    var me = J.access = function(e, t, n, r, i, o, a) {
        var s = 0,
            l = e.length,
            c = null == n;
        if ("object" === J.type(n)) { i = !0;
            for (s in n) J.access(e, t, s, n[s], !0, o, a) } else if (void 0 !== r && (i = !0, J.isFunction(r) || (a = !0), c && (a ? (t.call(e, r), t = null) : (c = t, t = function(e, t, n) {
                return c.call(J(e), n) })), t))
            for (; l > s; s++) t(e[s], n, a ? r : r.call(e[s], s, t(e[s], n)));
        return i ? e : c ? t.call(e) : l ? t(e[0], n) : o };
    J.acceptData = function(e) {
        return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType }, s.uid = 1, s.accepts = J.acceptData, s.prototype = { key: function(e) {
            if (!s.accepts(e)) return 0;
            var t = {},
                n = e[this.expando];
            if (!n) { n = s.uid++;
                try { t[this.expando] = { value: n }, Object.defineProperties(e, t) } catch (r) { t[this.expando] = n, J.extend(e, t) } }
            return this.cache[n] || (this.cache[n] = {}), n }, set: function(e, t, n) {
            var r, i = this.key(e),
                o = this.cache[i];
            if ("string" == typeof t) o[t] = n;
            else if (J.isEmptyObject(o)) J.extend(this.cache[i], t);
            else
                for (r in t) o[r] = t[r];
            return o }, get: function(e, t) {
            var n = this.cache[this.key(e)];
            return void 0 === t ? n : n[t] }, access: function(e, t, n) {
            var r;
            return void 0 === t || t && "string" == typeof t && void 0 === n ? (r = this.get(e, t), void 0 !== r ? r : this.get(e, J.camelCase(t))) : (this.set(e, t, n), void 0 !== n ? n : t) }, remove: function(e, t) {
            var n, r, i, o = this.key(e),
                a = this.cache[o];
            if (void 0 === t) this.cache[o] = {};
            else { J.isArray(t) ? r = t.concat(t.map(J.camelCase)) : (i = J.camelCase(t), t in a ? r = [t, i] : (r = i, r = r in a ? [r] : r.match(de) || [])), n = r.length;
                for (; n--;) delete a[r[n]] } }, hasData: function(e) {
            return !J.isEmptyObject(this.cache[e[this.expando]] || {}) }, discard: function(e) { e[this.expando] && delete this.cache[e[this.expando]] } };
    var ye = new s,
        ve = new s,
        xe = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        be = /([A-Z])/g;
    J.extend({ hasData: function(e) {
            return ve.hasData(e) || ye.hasData(e) }, data: function(e, t, n) {
            return ve.access(e, t, n) }, removeData: function(e, t) { ve.remove(e, t) }, _data: function(e, t, n) {
            return ye.access(e, t, n) }, _removeData: function(e, t) { ye.remove(e, t) } }), J.fn.extend({ data: function(e, t) {
            var n, r, i, o = this[0],
                a = o && o.attributes;
            if (void 0 === e) {
                if (this.length && (i = ve.get(o), 1 === o.nodeType && !ye.get(o, "hasDataAttrs"))) {
                    for (n = a.length; n--;) a[n] && (r = a[n].name, 0 === r.indexOf("data-") && (r = J.camelCase(r.slice(5)), l(o, r, i[r])));
                    ye.set(o, "hasDataAttrs", !0) }
                return i }
            return "object" == typeof e ? this.each(function() { ve.set(this, e) }) : me(this, function(t) {
                var n, r = J.camelCase(e);
                if (o && void 0 === t) {
                    if (n = ve.get(o, e), void 0 !== n) return n;
                    if (n = ve.get(o, r), void 0 !== n) return n;
                    if (n = l(o, r, void 0), void 0 !== n) return n } else this.each(function() {
                    var n = ve.get(this, r);
                    ve.set(this, r, t), -1 !== e.indexOf("-") && void 0 !== n && ve.set(this, e, t) }) }, null, t, arguments.length > 1, null, !0) }, removeData: function(e) {
            return this.each(function() { ve.remove(this, e) }) } }), J.extend({ queue: function(e, t, n) {
            var r;
            return e ? (t = (t || "fx") + "queue", r = ye.get(e, t), n && (!r || J.isArray(n) ? r = ye.access(e, t, J.makeArray(n)) : r.push(n)), r || []) : void 0 }, dequeue: function(e, t) { t = t || "fx";
            var n = J.queue(e, t),
                r = n.length,
                i = n.shift(),
                o = J._queueHooks(e, t),
                a = function() { J.dequeue(e, t) }; "inprogress" === i && (i = n.shift(), r--), i && ("fx" === t && n.unshift("inprogress"), delete o.stop, i.call(e, a, o)), !r && o && o.empty.fire() }, _queueHooks: function(e, t) {
            var n = t + "queueHooks";
            return ye.get(e, n) || ye.access(e, n, { empty: J.Callbacks("once memory").add(function() { ye.remove(e, [t + "queue", n]) }) }) } }), J.fn.extend({ queue: function(e, t) {
            var n = 2;
            return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? J.queue(this[0], e) : void 0 === t ? this : this.each(function() {
                var n = J.queue(this, e, t);
                J._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && J.dequeue(this, e) }) }, dequeue: function(e) {
            return this.each(function() { J.dequeue(this, e) }) }, clearQueue: function(e) {
            return this.queue(e || "fx", []) }, promise: function(e, t) {
            var n, r = 1,
                i = J.Deferred(),
                o = this,
                a = this.length,
                s = function() {--r || i.resolveWith(o, [o]) };
            for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; a--;) n = ye.get(o[a], e + "queueHooks"), n && n.empty && (r++, n.empty.add(s));
            return s(), i.promise(t) } });
    var we = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        Te = ["Top", "Right", "Bottom", "Left"],
        Ce = function(e, t) {
            return e = t || e, "none" === J.css(e, "display") || !J.contains(e.ownerDocument, e) },
        ke = /^(?:checkbox|radio)$/i;
    ! function() {
        var e = Z.createDocumentFragment(),
            t = e.appendChild(Z.createElement("div")),
            n = Z.createElement("input");
        n.setAttribute("type", "radio"), n.setAttribute("checked", "checked"), n.setAttribute("name", "t"), t.appendChild(n), G.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, t.innerHTML = "<textarea>x</textarea>", G.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue }();
    var Ee = "undefined";
    G.focusinBubbles = "onfocusin" in e;
    var Se = /^key/,
        Ne = /^(?:mouse|pointer|contextmenu)|click/,
        je = /^(?:focusinfocus|focusoutblur)$/,
        De = /^([^.]*)(?:\.(.+)|)$/;
    J.event = { global: {}, add: function(e, t, n, r, i) {
            var o, a, s, l, c, u, f, p, d, h, g, m = ye.get(e);
            if (m)
                for (n.handler && (o = n, n = o.handler, i = o.selector), n.guid || (n.guid = J.guid++), (l = m.events) || (l = m.events = {}), (a = m.handle) || (a = m.handle = function(t) {
                        return typeof J !== Ee && J.event.triggered !== t.type ? J.event.dispatch.apply(e, arguments) : void 0 }), t = (t || "").match(de) || [""], c = t.length; c--;) s = De.exec(t[c]) || [], d = g = s[1], h = (s[2] || "").split(".").sort(), d && (f = J.event.special[d] || {}, d = (i ? f.delegateType : f.bindType) || d, f = J.event.special[d] || {}, u = J.extend({ type: d, origType: g, data: r, handler: n, guid: n.guid, selector: i, needsContext: i && J.expr.match.needsContext.test(i), namespace: h.join(".") }, o), (p = l[d]) || (p = l[d] = [], p.delegateCount = 0, f.setup && f.setup.call(e, r, h, a) !== !1 || e.addEventListener && e.addEventListener(d, a, !1)), f.add && (f.add.call(e, u), u.handler.guid || (u.handler.guid = n.guid)), i ? p.splice(p.delegateCount++, 0, u) : p.push(u), J.event.global[d] = !0) }, remove: function(e, t, n, r, i) {
            var o, a, s, l, c, u, f, p, d, h, g, m = ye.hasData(e) && ye.get(e);
            if (m && (l = m.events)) {
                for (t = (t || "").match(de) || [""], c = t.length; c--;)
                    if (s = De.exec(t[c]) || [], d = g = s[1], h = (s[2] || "").split(".").sort(), d) {
                        for (f = J.event.special[d] || {}, d = (r ? f.delegateType : f.bindType) || d, p = l[d] || [], s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = o = p.length; o--;) u = p[o], !i && g !== u.origType || n && n.guid !== u.guid || s && !s.test(u.namespace) || r && r !== u.selector && ("**" !== r || !u.selector) || (p.splice(o, 1), u.selector && p.delegateCount--, f.remove && f.remove.call(e, u));
                        a && !p.length && (f.teardown && f.teardown.call(e, h, m.handle) !== !1 || J.removeEvent(e, d, m.handle), delete l[d]) } else
                        for (d in l) J.event.remove(e, d + t[c], n, r, !0);
                J.isEmptyObject(l) && (delete m.handle, ye.remove(e, "events")) } }, trigger: function(t, n, r, i) {
            var o, a, s, l, c, u, f, p = [r || Z],
                d = Y.call(t, "type") ? t.type : t,
                h = Y.call(t, "namespace") ? t.namespace.split(".") : [];
            if (a = s = r = r || Z, 3 !== r.nodeType && 8 !== r.nodeType && !je.test(d + J.event.triggered) && (d.indexOf(".") >= 0 && (h = d.split("."), d = h.shift(), h.sort()), c = d.indexOf(":") < 0 && "on" + d, t = t[J.expando] ? t : new J.Event(d, "object" == typeof t && t), t.isTrigger = i ? 2 : 3, t.namespace = h.join("."), t.namespace_re = t.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = r), n = null == n ? [t] : J.makeArray(n, [t]), f = J.event.special[d] || {}, i || !f.trigger || f.trigger.apply(r, n) !== !1)) {
                if (!i && !f.noBubble && !J.isWindow(r)) {
                    for (l = f.delegateType || d, je.test(l + d) || (a = a.parentNode); a; a = a.parentNode) p.push(a), s = a;
                    s === (r.ownerDocument || Z) && p.push(s.defaultView || s.parentWindow || e) }
                for (o = 0;
                    (a = p[o++]) && !t.isPropagationStopped();) t.type = o > 1 ? l : f.bindType || d, u = (ye.get(a, "events") || {})[t.type] && ye.get(a, "handle"), u && u.apply(a, n), u = c && a[c], u && u.apply && J.acceptData(a) && (t.result = u.apply(a, n), t.result === !1 && t.preventDefault());
                return t.type = d, i || t.isDefaultPrevented() || f._default && f._default.apply(p.pop(), n) !== !1 || !J.acceptData(r) || c && J.isFunction(r[d]) && !J.isWindow(r) && (s = r[c], s && (r[c] = null), J.event.triggered = d, r[d](), J.event.triggered = void 0, s && (r[c] = s)), t.result } }, dispatch: function(e) { e = J.event.fix(e);
            var t, n, r, i, o, a = [],
                s = B.call(arguments),
                l = (ye.get(this, "events") || {})[e.type] || [],
                c = J.event.special[e.type] || {};
            if (s[0] = e, e.delegateTarget = this, !c.preDispatch || c.preDispatch.call(this, e) !== !1) {
                for (a = J.event.handlers.call(this, e, l), t = 0;
                    (i = a[t++]) && !e.isPropagationStopped();)
                    for (e.currentTarget = i.elem, n = 0;
                        (o = i.handlers[n++]) && !e.isImmediatePropagationStopped();)(!e.namespace_re || e.namespace_re.test(o.namespace)) && (e.handleObj = o, e.data = o.data, r = ((J.event.special[o.origType] || {}).handle || o.handler).apply(i.elem, s), void 0 !== r && (e.result = r) === !1 && (e.preventDefault(), e.stopPropagation()));
                return c.postDispatch && c.postDispatch.call(this, e), e.result } }, handlers: function(e, t) {
            var n, r, i, o, a = [],
                s = t.delegateCount,
                l = e.target;
            if (s && l.nodeType && (!e.button || "click" !== e.type))
                for (; l !== this; l = l.parentNode || this)
                    if (l.disabled !== !0 || "click" !== e.type) {
                        for (r = [], n = 0; s > n; n++) o = t[n], i = o.selector + " ", void 0 === r[i] && (r[i] = o.needsContext ? J(i, this).index(l) >= 0 : J.find(i, this, null, [l]).length), r[i] && r.push(o);
                        r.length && a.push({ elem: l, handlers: r }) }
            return s < t.length && a.push({ elem: this, handlers: t.slice(s) }), a }, props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "), fixHooks: {}, keyHooks: { props: "char charCode key keyCode".split(" "), filter: function(e, t) {
                return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e } }, mouseHooks: { props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "), filter: function(e, t) {
                var n, r, i, o = t.button;
                return null == e.pageX && null != t.clientX && (n = e.target.ownerDocument || Z, r = n.documentElement, i = n.body, e.pageX = t.clientX + (r && r.scrollLeft || i && i.scrollLeft || 0) - (r && r.clientLeft || i && i.clientLeft || 0), e.pageY = t.clientY + (r && r.scrollTop || i && i.scrollTop || 0) - (r && r.clientTop || i && i.clientTop || 0)), e.which || void 0 === o || (e.which = 1 & o ? 1 : 2 & o ? 3 : 4 & o ? 2 : 0), e } }, fix: function(e) {
            if (e[J.expando]) return e;
            var t, n, r, i = e.type,
                o = e,
                a = this.fixHooks[i];
            for (a || (this.fixHooks[i] = a = Ne.test(i) ? this.mouseHooks : Se.test(i) ? this.keyHooks : {}), r = a.props ? this.props.concat(a.props) : this.props, e = new J.Event(o), t = r.length; t--;) n = r[t], e[n] = o[n];
            return e.target || (e.target = Z), 3 === e.target.nodeType && (e.target = e.target.parentNode), a.filter ? a.filter(e, o) : e }, special: { load: { noBubble: !0 }, focus: { trigger: function() {
                    return this !== f() && this.focus ? (this.focus(), !1) : void 0 }, delegateType: "focusin" }, blur: { trigger: function() {
                    return this === f() && this.blur ? (this.blur(), !1) : void 0 }, delegateType: "focusout" }, click: { trigger: function() {
                    return "checkbox" === this.type && this.click && J.nodeName(this, "input") ? (this.click(), !1) : void 0 }, _default: function(e) {
                    return J.nodeName(e.target, "a") } }, beforeunload: { postDispatch: function(e) { void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result) } } }, simulate: function(e, t, n, r) {
            var i = J.extend(new J.Event, n, { type: e, isSimulated: !0, originalEvent: {} });
            r ? J.event.trigger(i, null, t) : J.event.dispatch.call(t, i), i.isDefaultPrevented() && n.preventDefault() } }, J.removeEvent = function(e, t, n) { e.removeEventListener && e.removeEventListener(t, n, !1) }, J.Event = function(e, t) {
        return this instanceof J.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && e.returnValue === !1 ? c : u) : this.type = e, t && J.extend(this, t), this.timeStamp = e && e.timeStamp || J.now(), void(this[J.expando] = !0)) : new J.Event(e, t) }, J.Event.prototype = { isDefaultPrevented: u, isPropagationStopped: u, isImmediatePropagationStopped: u, preventDefault: function() {
            var e = this.originalEvent;
            this.isDefaultPrevented = c, e && e.preventDefault && e.preventDefault() }, stopPropagation: function() {
            var e = this.originalEvent;
            this.isPropagationStopped = c, e && e.stopPropagation && e.stopPropagation() }, stopImmediatePropagation: function() {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = c, e && e.stopImmediatePropagation && e.stopImmediatePropagation(), this.stopPropagation() } }, J.each({ mouseenter: "mouseover", mouseleave: "mouseout", pointerenter: "pointerover", pointerleave: "pointerout" }, function(e, t) { J.event.special[e] = { delegateType: t, bindType: t, handle: function(e) {
                var n, r = this,
                    i = e.relatedTarget,
                    o = e.handleObj;
                return (!i || i !== r && !J.contains(r, i)) && (e.type = o.origType, n = o.handler.apply(this, arguments), e.type = t), n } } }), G.focusinBubbles || J.each({ focus: "focusin", blur: "focusout" }, function(e, t) {
        var n = function(e) { J.event.simulate(t, e.target, J.event.fix(e), !0) };
        J.event.special[t] = { setup: function() {
                var r = this.ownerDocument || this,
                    i = ye.access(r, t);
                i || r.addEventListener(e, n, !0), ye.access(r, t, (i || 0) + 1) }, teardown: function() {
                var r = this.ownerDocument || this,
                    i = ye.access(r, t) - 1;
                i ? ye.access(r, t, i) : (r.removeEventListener(e, n, !0), ye.remove(r, t)) } } }), J.fn.extend({ on: function(e, t, n, r, i) {
            var o, a;
            if ("object" == typeof e) { "string" != typeof t && (n = n || t, t = void 0);
                for (a in e) this.on(a, t, n, e[a], i);
                return this }
            if (null == n && null == r ? (r = t, n = t = void 0) : null == r && ("string" == typeof t ? (r = n, n = void 0) : (r = n, n = t, t = void 0)), r === !1) r = u;
            else if (!r) return this;
            return 1 === i && (o = r, r = function(e) {
                return J().off(e), o.apply(this, arguments) }, r.guid = o.guid || (o.guid = J.guid++)), this.each(function() { J.event.add(this, e, r, n, t) }) }, one: function(e, t, n, r) {
            return this.on(e, t, n, r, 1) }, off: function(e, t, n) {
            var r, i;
            if (e && e.preventDefault && e.handleObj) return r = e.handleObj, J(e.delegateTarget).off(r.namespace ? r.origType + "." + r.namespace : r.origType, r.selector, r.handler), this;
            if ("object" == typeof e) {
                for (i in e) this.off(i, t, e[i]);
                return this }
            return (t === !1 || "function" == typeof t) && (n = t, t = void 0), n === !1 && (n = u), this.each(function() { J.event.remove(this, e, n, t) }) }, trigger: function(e, t) {
            return this.each(function() { J.event.trigger(e, t, this) }) }, triggerHandler: function(e, t) {
            var n = this[0];
            return n ? J.event.trigger(e, t, n, !0) : void 0 } });
    var Ae = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        Le = /<([\w:]+)/,
        He = /<|&#?\w+;/,
        Oe = /<(?:script|style|link)/i,
        qe = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Pe = /^$|\/(?:java|ecma)script/i,
        Re = /^true\/(.*)/,
        We = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        Me = { option: [1, "<select multiple='multiple'>", "</select>"], thead: [1, "<table>", "</table>"], col: [2, "<table><colgroup>", "</colgroup></table>"], tr: [2, "<table><tbody>", "</tbody></table>"], td: [3, "<table><tbody><tr>", "</tr></tbody></table>"], _default: [0, "", ""] };
    Me.optgroup = Me.option, Me.tbody = Me.tfoot = Me.colgroup = Me.caption = Me.thead, Me.th = Me.td, J.extend({ clone: function(e, t, n) {
            var r, i, o, a, s = e.cloneNode(!0),
                l = J.contains(e.ownerDocument, e);
            if (!(G.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || J.isXMLDoc(e)))
                for (a = y(s), o = y(e), r = 0, i = o.length; i > r; r++) v(o[r], a[r]);
            if (t)
                if (n)
                    for (o = o || y(e), a = a || y(s), r = 0, i = o.length; i > r; r++) m(o[r], a[r]);
                else m(e, s);
            return a = y(s, "script"), a.length > 0 && g(a, !l && y(e, "script")), s }, buildFragment: function(e, t, n, r) {
            for (var i, o, a, s, l, c, u = t.createDocumentFragment(), f = [], p = 0, d = e.length; d > p; p++)
                if (i = e[p], i || 0 === i)
                    if ("object" === J.type(i)) J.merge(f, i.nodeType ? [i] : i);
                    else if (He.test(i)) {
                for (o = o || u.appendChild(t.createElement("div")), a = (Le.exec(i) || ["", ""])[1].toLowerCase(), s = Me[a] || Me._default, o.innerHTML = s[1] + i.replace(Ae, "<$1></$2>") + s[2], c = s[0]; c--;) o = o.lastChild;
                J.merge(f, o.childNodes), o = u.firstChild, o.textContent = "" } else f.push(t.createTextNode(i));
            for (u.textContent = "", p = 0; i = f[p++];)
                if ((!r || -1 === J.inArray(i, r)) && (l = J.contains(i.ownerDocument, i), o = y(u.appendChild(i), "script"), l && g(o), n))
                    for (c = 0; i = o[c++];) Pe.test(i.type || "") && n.push(i);
            return u }, cleanData: function(e) {
            for (var t, n, r, i, o = J.event.special, a = 0; void 0 !== (n = e[a]); a++) {
                if (J.acceptData(n) && (i = n[ye.expando], i && (t = ye.cache[i]))) {
                    if (t.events)
                        for (r in t.events) o[r] ? J.event.remove(n, r) : J.removeEvent(n, r, t.handle);
                    ye.cache[i] && delete ye.cache[i] }
                delete ve.cache[n[ve.expando]] } } }), J.fn.extend({ text: function(e) {
            return me(this, function(e) {
                return void 0 === e ? J.text(this) : this.empty().each(function() {
                    (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && (this.textContent = e) }) }, null, e, arguments.length) }, append: function() {
            return this.domManip(arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = p(this, e);
                    t.appendChild(e) } }) }, prepend: function() {
            return this.domManip(arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = p(this, e);
                    t.insertBefore(e, t.firstChild) } }) }, before: function() {
            return this.domManip(arguments, function(e) { this.parentNode && this.parentNode.insertBefore(e, this) }) }, after: function() {
            return this.domManip(arguments, function(e) { this.parentNode && this.parentNode.insertBefore(e, this.nextSibling) }) }, remove: function(e, t) {
            for (var n, r = e ? J.filter(e, this) : this, i = 0; null != (n = r[i]); i++) t || 1 !== n.nodeType || J.cleanData(y(n)), n.parentNode && (t && J.contains(n.ownerDocument, n) && g(y(n, "script")), n.parentNode.removeChild(n));
            return this }, empty: function() {
            for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (J.cleanData(y(e, !1)), e.textContent = "");
            return this }, clone: function(e, t) {
            return e = null == e ? !1 : e, t = null == t ? e : t, this.map(function() {
                return J.clone(this, e, t) }) }, html: function(e) {
            return me(this, function(e) {
                var t = this[0] || {},
                    n = 0,
                    r = this.length;
                if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                if ("string" == typeof e && !Oe.test(e) && !Me[(Le.exec(e) || ["", ""])[1].toLowerCase()]) { e = e.replace(Ae, "<$1></$2>");
                    try {
                        for (; r > n; n++) t = this[n] || {}, 1 === t.nodeType && (J.cleanData(y(t, !1)), t.innerHTML = e);
                        t = 0 } catch (i) {} }
                t && this.empty().append(e) }, null, e, arguments.length) }, replaceWith: function() {
            var e = arguments[0];
            return this.domManip(arguments, function(t) { e = this.parentNode, J.cleanData(y(this)), e && e.replaceChild(t, this) }), e && (e.length || e.nodeType) ? this : this.remove() }, detach: function(e) {
            return this.remove(e, !0) }, domManip: function(e, t) { e = z.apply([], e);
            var n, r, i, o, a, s, l = 0,
                c = this.length,
                u = this,
                f = c - 1,
                p = e[0],
                g = J.isFunction(p);
            if (g || c > 1 && "string" == typeof p && !G.checkClone && qe.test(p)) return this.each(function(n) {
                var r = u.eq(n);
                g && (e[0] = p.call(this, n, r.html())), r.domManip(e, t) });
            if (c && (n = J.buildFragment(e, this[0].ownerDocument, !1, this), r = n.firstChild, 1 === n.childNodes.length && (n = r), r)) {
                for (i = J.map(y(n, "script"), d), o = i.length; c > l; l++) a = n, l !== f && (a = J.clone(a, !0, !0), o && J.merge(i, y(a, "script"))), t.call(this[l], a, l);
                if (o)
                    for (s = i[i.length - 1].ownerDocument, J.map(i, h), l = 0; o > l; l++) a = i[l], Pe.test(a.type || "") && !ye.access(a, "globalEval") && J.contains(s, a) && (a.src ? J._evalUrl && J._evalUrl(a.src) : J.globalEval(a.textContent.replace(We, ""))) }
            return this } }), J.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, function(e, t) { J.fn[e] = function(e) {
            for (var n, r = [], i = J(e), o = i.length - 1, a = 0; o >= a; a++) n = a === o ? this : this.clone(!0), J(i[a])[t](n), X.apply(r, n.get());
            return this.pushStack(r) } });
    var Fe, _e = {},
        Ie = /^margin/,
        $e = new RegExp("^(" + we + ")(?!px)[a-z%]+$", "i"),
        Be = function(t) {
            return t.ownerDocument.defaultView.opener ? t.ownerDocument.defaultView.getComputedStyle(t, null) : e.getComputedStyle(t, null) };
    ! function() {
        function t() { a.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", a.innerHTML = "", i.appendChild(o);
            var t = e.getComputedStyle(a, null);
            n = "1%" !== t.top, r = "4px" === t.width, i.removeChild(o) }
        var n, r, i = Z.documentElement,
            o = Z.createElement("div"),
            a = Z.createElement("div");
        a.style && (a.style.backgroundClip = "content-box", a.cloneNode(!0).style.backgroundClip = "", G.clearCloneStyle = "content-box" === a.style.backgroundClip, o.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;position:absolute", o.appendChild(a), e.getComputedStyle && J.extend(G, { pixelPosition: function() {
                return t(), n }, boxSizingReliable: function() {
                return null == r && t(), r }, reliableMarginRight: function() {
                var t, n = a.appendChild(Z.createElement("div"));
                return n.style.cssText = a.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", n.style.marginRight = n.style.width = "0", a.style.width = "1px", i.appendChild(o), t = !parseFloat(e.getComputedStyle(n, null).marginRight), i.removeChild(o), a.removeChild(n), t } })) }(), J.swap = function(e, t, n, r) {
        var i, o, a = {};
        for (o in t) a[o] = e.style[o], e.style[o] = t[o];
        i = n.apply(e, r || []);
        for (o in t) e.style[o] = a[o];
        return i };
    var ze = /^(none|table(?!-c[ea]).+)/,
        Xe = new RegExp("^(" + we + ")(.*)$", "i"),
        Ve = new RegExp("^([+-])=(" + we + ")", "i"),
        Ue = { position: "absolute", visibility: "hidden", display: "block" },
        Ke = { letterSpacing: "0", fontWeight: "400" },
        Ye = ["Webkit", "O", "Moz", "ms"];
    J.extend({ cssHooks: { opacity: { get: function(e, t) {
                    if (t) {
                        var n = w(e, "opacity");
                        return "" === n ? "1" : n } } } }, cssNumber: { columnCount: !0, fillOpacity: !0, flexGrow: !0, flexShrink: !0, fontWeight: !0, lineHeight: !0, opacity: !0, order: !0, orphans: !0, widows: !0, zIndex: !0, zoom: !0 }, cssProps: { "float": "cssFloat" }, style: function(e, t, n, r) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var i, o, a, s = J.camelCase(t),
                    l = e.style;
                return t = J.cssProps[s] || (J.cssProps[s] = C(l, s)), a = J.cssHooks[t] || J.cssHooks[s], void 0 === n ? a && "get" in a && void 0 !== (i = a.get(e, !1, r)) ? i : l[t] : (o = typeof n, "string" === o && (i = Ve.exec(n)) && (n = (i[1] + 1) * i[2] + parseFloat(J.css(e, t)), o = "number"), void(null != n && n === n && ("number" !== o || J.cssNumber[s] || (n += "px"), G.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), a && "set" in a && void 0 === (n = a.set(e, n, r)) || (l[t] = n)))) } }, css: function(e, t, n, r) {
            var i, o, a, s = J.camelCase(t);
            return t = J.cssProps[s] || (J.cssProps[s] = C(e.style, s)), a = J.cssHooks[t] || J.cssHooks[s], a && "get" in a && (i = a.get(e, !0, n)), void 0 === i && (i = w(e, t, r)), "normal" === i && t in Ke && (i = Ke[t]), "" === n || n ? (o = parseFloat(i), n === !0 || J.isNumeric(o) ? o || 0 : i) : i } }), J.each(["height", "width"], function(e, t) { J.cssHooks[t] = { get: function(e, n, r) {
                return n ? ze.test(J.css(e, "display")) && 0 === e.offsetWidth ? J.swap(e, Ue, function() {
                    return S(e, t, r) }) : S(e, t, r) : void 0 }, set: function(e, n, r) {
                var i = r && Be(e);
                return k(e, n, r ? E(e, t, r, "border-box" === J.css(e, "boxSizing", !1, i), i) : 0) } } }), J.cssHooks.marginRight = T(G.reliableMarginRight, function(e, t) {
        return t ? J.swap(e, { display: "inline-block" }, w, [e, "marginRight"]) : void 0 }), J.each({ margin: "", padding: "", border: "Width" }, function(e, t) { J.cssHooks[e + t] = { expand: function(n) {
                for (var r = 0, i = {}, o = "string" == typeof n ? n.split(" ") : [n]; 4 > r; r++) i[e + Te[r] + t] = o[r] || o[r - 2] || o[0];
                return i } }, Ie.test(e) || (J.cssHooks[e + t].set = k) }), J.fn.extend({ css: function(e, t) {
            return me(this, function(e, t, n) {
                var r, i, o = {},
                    a = 0;
                if (J.isArray(t)) {
                    for (r = Be(e), i = t.length; i > a; a++) o[t[a]] = J.css(e, t[a], !1, r);
                    return o }
                return void 0 !== n ? J.style(e, t, n) : J.css(e, t) }, e, t, arguments.length > 1) }, show: function() {
            return N(this, !0) }, hide: function() {
            return N(this) }, toggle: function(e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() { Ce(this) ? J(this).show() : J(this).hide() }) } }), J.Tween = j, j.prototype = { constructor: j, init: function(e, t, n, r, i, o) { this.elem = e, this.prop = n, this.easing = i || "swing", this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = o || (J.cssNumber[n] ? "" : "px") }, cur: function() {
            var e = j.propHooks[this.prop];
            return e && e.get ? e.get(this) : j.propHooks._default.get(this) }, run: function(e) {
            var t, n = j.propHooks[this.prop];
            return this.options.duration ? this.pos = t = J.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : j.propHooks._default.set(this), this } }, j.prototype.init.prototype = j.prototype, j.propHooks = { _default: { get: function(e) {
                var t;
                return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = J.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop] }, set: function(e) { J.fx.step[e.prop] ? J.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[J.cssProps[e.prop]] || J.cssHooks[e.prop]) ? J.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now } } }, j.propHooks.scrollTop = j.propHooks.scrollLeft = { set: function(e) { e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now) } }, J.easing = { linear: function(e) {
            return e }, swing: function(e) {
            return .5 - Math.cos(e * Math.PI) / 2 } }, J.fx = j.prototype.init, J.fx.step = {};
    var Ge, Ze, Qe = /^(?:toggle|show|hide)$/,
        Je = new RegExp("^(?:([+-])=|)(" + we + ")([a-z%]*)$", "i"),
        et = /queueHooks$/,
        tt = [H],
        nt = { "*": [function(e, t) {
                var n = this.createTween(e, t),
                    r = n.cur(),
                    i = Je.exec(t),
                    o = i && i[3] || (J.cssNumber[e] ? "" : "px"),
                    a = (J.cssNumber[e] || "px" !== o && +r) && Je.exec(J.css(n.elem, e)),
                    s = 1,
                    l = 20;
                if (a && a[3] !== o) { o = o || a[3], i = i || [], a = +r || 1;
                    do s = s || ".5", a /= s, J.style(n.elem, e, a + o); while (s !== (s = n.cur() / r) && 1 !== s && --l) }
                return i && (a = n.start = +a || +r || 0, n.unit = o, n.end = i[1] ? a + (i[1] + 1) * i[2] : +i[2]), n }] };
    J.Animation = J.extend(q, { tweener: function(e, t) { J.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
                for (var n, r = 0, i = e.length; i > r; r++) n = e[r], nt[n] = nt[n] || [], nt[n].unshift(t) }, prefilter: function(e, t) { t ? tt.unshift(e) : tt.push(e) } }), J.speed = function(e, t, n) {
            var r = e && "object" == typeof e ? J.extend({}, e) : { complete: n || !n && t || J.isFunction(e) && e, duration: e, easing: n && t || t && !J.isFunction(t) && t };
            return r.duration = J.fx.off ? 0 : "number" == typeof r.duration ? r.duration : r.duration in J.fx.speeds ? J.fx.speeds[r.duration] : J.fx.speeds._default, (null == r.queue || r.queue === !0) && (r.queue = "fx"), r.old = r.complete, r.complete = function() { J.isFunction(r.old) && r.old.call(this), r.queue && J.dequeue(this, r.queue) }, r }, J.fn.extend({
            fadeTo: function(e, t, n, r) {
                return this.filter(Ce).css("opacity", 0).show().end().animate({ opacity: t }, e, n, r) },
            animate: function(e, t, n, r) {
                var i = J.isEmptyObject(e),
                    o = J.speed(t, n, r),
                    a = function() {
                        var t = q(this, J.extend({}, e), o);
                        (i || ye.get(this, "finish")) && t.stop(!0) };
                return a.finish = a, i || o.queue === !1 ? this.each(a) : this.queue(o.queue, a) },
            stop: function(e, t, n) {
                var r = function(e) {
                    var t = e.stop;
                    delete e.stop, t(n) };
                return "string" != typeof e && (n = t, t = e, e = void 0), t && e !== !1 && this.queue(e || "fx", []), this.each(function() {
                    var t = !0,
                        i = null != e && e + "queueHooks",
                        o = J.timers,
                        a = ye.get(this);
                    if (i) a[i] && a[i].stop && r(a[i]);
                    else
                        for (i in a) a[i] && a[i].stop && et.test(i) && r(a[i]);
                    for (i = o.length; i--;) o[i].elem !== this || null != e && o[i].queue !== e || (o[i].anim.stop(n), t = !1, o.splice(i, 1));
                    (t || !n) && J.dequeue(this, e) }) },
            finish: function(e) {
                return e !== !1 && (e = e || "fx"), this.each(function() {
                    var t, n = ye.get(this),
                        r = n[e + "queue"],
                        i = n[e + "queueHooks"],
                        o = J.timers,
                        a = r ? r.length : 0;
                    for (n.finish = !0, J.queue(this, e, []),
                        i && i.stop && i.stop.call(this, !0), t = o.length; t--;) o[t].elem === this && o[t].queue === e && (o[t].anim.stop(!0), o.splice(t, 1));
                    for (t = 0; a > t; t++) r[t] && r[t].finish && r[t].finish.call(this);
                    delete n.finish
                })
            }
        }), J.each(["toggle", "show", "hide"], function(e, t) {
            var n = J.fn[t];
            J.fn[t] = function(e, r, i) {
                return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(A(t, !0), e, r, i) } }), J.each({ slideDown: A("show"), slideUp: A("hide"), slideToggle: A("toggle"), fadeIn: { opacity: "show" }, fadeOut: { opacity: "hide" }, fadeToggle: { opacity: "toggle" } }, function(e, t) { J.fn[e] = function(e, n, r) {
                return this.animate(t, e, n, r) } }), J.timers = [], J.fx.tick = function() {
            var e, t = 0,
                n = J.timers;
            for (Ge = J.now(); t < n.length; t++) e = n[t], e() || n[t] !== e || n.splice(t--, 1);
            n.length || J.fx.stop(), Ge = void 0 }, J.fx.timer = function(e) { J.timers.push(e), e() ? J.fx.start() : J.timers.pop() }, J.fx.interval = 13, J.fx.start = function() { Ze || (Ze = setInterval(J.fx.tick, J.fx.interval)) }, J.fx.stop = function() { clearInterval(Ze), Ze = null }, J.fx.speeds = { slow: 600, fast: 200, _default: 400 }, J.fn.delay = function(e, t) {
            return e = J.fx ? J.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function(t, n) {
                var r = setTimeout(t, e);
                n.stop = function() { clearTimeout(r) } }) },
        function() {
            var e = Z.createElement("input"),
                t = Z.createElement("select"),
                n = t.appendChild(Z.createElement("option"));
            e.type = "checkbox", G.checkOn = "" !== e.value, G.optSelected = n.selected, t.disabled = !0, G.optDisabled = !n.disabled, e = Z.createElement("input"), e.value = "t", e.type = "radio", G.radioValue = "t" === e.value }();
    var rt, it, ot = J.expr.attrHandle;
    J.fn.extend({ attr: function(e, t) {
            return me(this, J.attr, e, t, arguments.length > 1) }, removeAttr: function(e) {
            return this.each(function() { J.removeAttr(this, e) }) } }), J.extend({ attr: function(e, t, n) {
            var r, i, o = e.nodeType;
            return e && 3 !== o && 8 !== o && 2 !== o ? typeof e.getAttribute === Ee ? J.prop(e, t, n) : (1 === o && J.isXMLDoc(e) || (t = t.toLowerCase(), r = J.attrHooks[t] || (J.expr.match.bool.test(t) ? it : rt)), void 0 === n ? r && "get" in r && null !== (i = r.get(e, t)) ? i : (i = J.find.attr(e, t), null == i ? void 0 : i) : null !== n ? r && "set" in r && void 0 !== (i = r.set(e, n, t)) ? i : (e.setAttribute(t, n + ""), n) : void J.removeAttr(e, t)) : void 0 }, removeAttr: function(e, t) {
            var n, r, i = 0,
                o = t && t.match(de);
            if (o && 1 === e.nodeType)
                for (; n = o[i++];) r = J.propFix[n] || n, J.expr.match.bool.test(n) && (e[r] = !1), e.removeAttribute(n) }, attrHooks: { type: { set: function(e, t) {
                    if (!G.radioValue && "radio" === t && J.nodeName(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t } } } } }), it = { set: function(e, t, n) {
            return t === !1 ? J.removeAttr(e, n) : e.setAttribute(n, n), n } }, J.each(J.expr.match.bool.source.match(/\w+/g), function(e, t) {
        var n = ot[t] || J.find.attr;
        ot[t] = function(e, t, r) {
            var i, o;
            return r || (o = ot[t], ot[t] = i, i = null != n(e, t, r) ? t.toLowerCase() : null, ot[t] = o), i } });
    var at = /^(?:input|select|textarea|button)$/i;
    J.fn.extend({ prop: function(e, t) {
            return me(this, J.prop, e, t, arguments.length > 1) }, removeProp: function(e) {
            return this.each(function() { delete this[J.propFix[e] || e] }) } }), J.extend({ propFix: { "for": "htmlFor", "class": "className" }, prop: function(e, t, n) {
            var r, i, o, a = e.nodeType;
            return e && 3 !== a && 8 !== a && 2 !== a ? (o = 1 !== a || !J.isXMLDoc(e), o && (t = J.propFix[t] || t, i = J.propHooks[t]), void 0 !== n ? i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : e[t] = n : i && "get" in i && null !== (r = i.get(e, t)) ? r : e[t]) : void 0 }, propHooks: { tabIndex: { get: function(e) {
                    return e.hasAttribute("tabindex") || at.test(e.nodeName) || e.href ? e.tabIndex : -1 } } } }), G.optSelected || (J.propHooks.selected = { get: function(e) {
            var t = e.parentNode;
            return t && t.parentNode && t.parentNode.selectedIndex, null } }), J.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() { J.propFix[this.toLowerCase()] = this });
    var st = /[\t\r\n\f]/g;
    J.fn.extend({ addClass: function(e) {
            var t, n, r, i, o, a, s = "string" == typeof e && e,
                l = 0,
                c = this.length;
            if (J.isFunction(e)) return this.each(function(t) { J(this).addClass(e.call(this, t, this.className)) });
            if (s)
                for (t = (e || "").match(de) || []; c > l; l++)
                    if (n = this[l], r = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(st, " ") : " ")) {
                        for (o = 0; i = t[o++];) r.indexOf(" " + i + " ") < 0 && (r += i + " ");
                        a = J.trim(r), n.className !== a && (n.className = a) }
            return this }, removeClass: function(e) {
            var t, n, r, i, o, a, s = 0 === arguments.length || "string" == typeof e && e,
                l = 0,
                c = this.length;
            if (J.isFunction(e)) return this.each(function(t) { J(this).removeClass(e.call(this, t, this.className)) });
            if (s)
                for (t = (e || "").match(de) || []; c > l; l++)
                    if (n = this[l], r = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(st, " ") : "")) {
                        for (o = 0; i = t[o++];)
                            for (; r.indexOf(" " + i + " ") >= 0;) r = r.replace(" " + i + " ", " ");
                        a = e ? J.trim(r) : "", n.className !== a && (n.className = a) }
            return this }, toggleClass: function(e, t) {
            var n = typeof e;
            return "boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : this.each(J.isFunction(e) ? function(n) { J(this).toggleClass(e.call(this, n, this.className, t), t) } : function() {
                if ("string" === n)
                    for (var t, r = 0, i = J(this), o = e.match(de) || []; t = o[r++];) i.hasClass(t) ? i.removeClass(t) : i.addClass(t);
                else(n === Ee || "boolean" === n) && (this.className && ye.set(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : ye.get(this, "__className__") || "") }) }, hasClass: function(e) {
            for (var t = " " + e + " ", n = 0, r = this.length; r > n; n++)
                if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(st, " ").indexOf(t) >= 0) return !0;
            return !1 } });
    var lt = /\r/g;
    J.fn.extend({ val: function(e) {
            var t, n, r, i = this[0];
            return arguments.length ? (r = J.isFunction(e), this.each(function(n) {
                var i;
                1 === this.nodeType && (i = r ? e.call(this, n, J(this).val()) : e, null == i ? i = "" : "number" == typeof i ? i += "" : J.isArray(i) && (i = J.map(i, function(e) {
                    return null == e ? "" : e + "" })), t = J.valHooks[this.type] || J.valHooks[this.nodeName.toLowerCase()], t && "set" in t && void 0 !== t.set(this, i, "value") || (this.value = i)) })) : i ? (t = J.valHooks[i.type] || J.valHooks[i.nodeName.toLowerCase()], t && "get" in t && void 0 !== (n = t.get(i, "value")) ? n : (n = i.value, "string" == typeof n ? n.replace(lt, "") : null == n ? "" : n)) : void 0 } }), J.extend({ valHooks: { option: { get: function(e) {
                    var t = J.find.attr(e, "value");
                    return null != t ? t : J.trim(J.text(e)) } }, select: { get: function(e) {
                    for (var t, n, r = e.options, i = e.selectedIndex, o = "select-one" === e.type || 0 > i, a = o ? null : [], s = o ? i + 1 : r.length, l = 0 > i ? s : o ? i : 0; s > l; l++)
                        if (n = r[l], !(!n.selected && l !== i || (G.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && J.nodeName(n.parentNode, "optgroup"))) {
                            if (t = J(n).val(), o) return t;
                            a.push(t) }
                    return a }, set: function(e, t) {
                    for (var n, r, i = e.options, o = J.makeArray(t), a = i.length; a--;) r = i[a], (r.selected = J.inArray(r.value, o) >= 0) && (n = !0);
                    return n || (e.selectedIndex = -1), o } } } }), J.each(["radio", "checkbox"], function() { J.valHooks[this] = { set: function(e, t) {
                return J.isArray(t) ? e.checked = J.inArray(J(e).val(), t) >= 0 : void 0 } }, G.checkOn || (J.valHooks[this].get = function(e) {
            return null === e.getAttribute("value") ? "on" : e.value }) }), J.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) { J.fn[t] = function(e, n) {
            return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t) } }), J.fn.extend({ hover: function(e, t) {
            return this.mouseenter(e).mouseleave(t || e) }, bind: function(e, t, n) {
            return this.on(e, null, t, n) }, unbind: function(e, t) {
            return this.off(e, null, t) }, delegate: function(e, t, n, r) {
            return this.on(t, e, n, r) }, undelegate: function(e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n) } });
    var ct = J.now(),
        ut = /\?/;
    J.parseJSON = function(e) {
        return JSON.parse(e + "") }, J.parseXML = function(e) {
        var t, n;
        if (!e || "string" != typeof e) return null;
        try { n = new DOMParser, t = n.parseFromString(e, "text/xml") } catch (r) { t = void 0 }
        return (!t || t.getElementsByTagName("parsererror").length) && J.error("Invalid XML: " + e), t };
    var ft = /#.*$/,
        pt = /([?&])_=[^&]*/,
        dt = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        ht = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        gt = /^(?:GET|HEAD)$/,
        mt = /^\/\//,
        yt = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
        vt = {},
        xt = {},
        bt = "*/".concat("*"),
        wt = e.location.href,
        Tt = yt.exec(wt.toLowerCase()) || [];
    J.extend({ active: 0, lastModified: {}, etag: {}, ajaxSettings: { url: wt, type: "GET", isLocal: ht.test(Tt[1]), global: !0, processData: !0, async: !0, contentType: "application/x-www-form-urlencoded; charset=UTF-8", accepts: { "*": bt, text: "text/plain", html: "text/html", xml: "application/xml, text/xml", json: "application/json, text/javascript" }, contents: { xml: /xml/, html: /html/, json: /json/ }, responseFields: { xml: "responseXML", text: "responseText", json: "responseJSON" }, converters: { "* text": String, "text html": !0, "text json": J.parseJSON, "text xml": J.parseXML }, flatOptions: { url: !0, context: !0 } }, ajaxSetup: function(e, t) {
            return t ? W(W(e, J.ajaxSettings), t) : W(J.ajaxSettings, e) }, ajaxPrefilter: P(vt), ajaxTransport: P(xt), ajax: function(e, t) {
            function n(e, t, n, a) {
                var l, u, y, v, b, T = t;
                2 !== x && (x = 2, s && clearTimeout(s), r = void 0, o = a || "", w.readyState = e > 0 ? 4 : 0, l = e >= 200 && 300 > e || 304 === e, n && (v = M(f, w, n)), v = F(f, v, w, l), l ? (f.ifModified && (b = w.getResponseHeader("Last-Modified"), b && (J.lastModified[i] = b), b = w.getResponseHeader("etag"), b && (J.etag[i] = b)), 204 === e || "HEAD" === f.type ? T = "nocontent" : 304 === e ? T = "notmodified" : (T = v.state, u = v.data, y = v.error, l = !y)) : (y = T, (e || !T) && (T = "error", 0 > e && (e = 0))), w.status = e, w.statusText = (t || T) + "", l ? h.resolveWith(p, [u, T, w]) : h.rejectWith(p, [w, T, y]), w.statusCode(m), m = void 0, c && d.trigger(l ? "ajaxSuccess" : "ajaxError", [w, f, l ? u : y]), g.fireWith(p, [w, T]), c && (d.trigger("ajaxComplete", [w, f]), --J.active || J.event.trigger("ajaxStop"))) } "object" == typeof e && (t = e, e = void 0), t = t || {};
            var r, i, o, a, s, l, c, u, f = J.ajaxSetup({}, t),
                p = f.context || f,
                d = f.context && (p.nodeType || p.jquery) ? J(p) : J.event,
                h = J.Deferred(),
                g = J.Callbacks("once memory"),
                m = f.statusCode || {},
                y = {},
                v = {},
                x = 0,
                b = "canceled",
                w = { readyState: 0, getResponseHeader: function(e) {
                        var t;
                        if (2 === x) {
                            if (!a)
                                for (a = {}; t = dt.exec(o);) a[t[1].toLowerCase()] = t[2];
                            t = a[e.toLowerCase()] }
                        return null == t ? null : t }, getAllResponseHeaders: function() {
                        return 2 === x ? o : null }, setRequestHeader: function(e, t) {
                        var n = e.toLowerCase();
                        return x || (e = v[n] = v[n] || e, y[e] = t), this }, overrideMimeType: function(e) {
                        return x || (f.mimeType = e), this }, statusCode: function(e) {
                        var t;
                        if (e)
                            if (2 > x)
                                for (t in e) m[t] = [m[t], e[t]];
                            else w.always(e[w.status]);
                        return this }, abort: function(e) {
                        var t = e || b;
                        return r && r.abort(t), n(0, t), this } };
            if (h.promise(w).complete = g.add, w.success = w.done, w.error = w.fail, f.url = ((e || f.url || wt) + "").replace(ft, "").replace(mt, Tt[1] + "//"), f.type = t.method || t.type || f.method || f.type, f.dataTypes = J.trim(f.dataType || "*").toLowerCase().match(de) || [""], null == f.crossDomain && (l = yt.exec(f.url.toLowerCase()), f.crossDomain = !(!l || l[1] === Tt[1] && l[2] === Tt[2] && (l[3] || ("http:" === l[1] ? "80" : "443")) === (Tt[3] || ("http:" === Tt[1] ? "80" : "443")))), f.data && f.processData && "string" != typeof f.data && (f.data = J.param(f.data, f.traditional)), R(vt, f, t, w), 2 === x) return w;
            c = J.event && f.global, c && 0 === J.active++ && J.event.trigger("ajaxStart"), f.type = f.type.toUpperCase(), f.hasContent = !gt.test(f.type), i = f.url, f.hasContent || (f.data && (i = f.url += (ut.test(i) ? "&" : "?") + f.data, delete f.data), f.cache === !1 && (f.url = pt.test(i) ? i.replace(pt, "$1_=" + ct++) : i + (ut.test(i) ? "&" : "?") + "_=" + ct++)), f.ifModified && (J.lastModified[i] && w.setRequestHeader("If-Modified-Since", J.lastModified[i]), J.etag[i] && w.setRequestHeader("If-None-Match", J.etag[i])), (f.data && f.hasContent && f.contentType !== !1 || t.contentType) && w.setRequestHeader("Content-Type", f.contentType), w.setRequestHeader("Accept", f.dataTypes[0] && f.accepts[f.dataTypes[0]] ? f.accepts[f.dataTypes[0]] + ("*" !== f.dataTypes[0] ? ", " + bt + "; q=0.01" : "") : f.accepts["*"]);
            for (u in f.headers) w.setRequestHeader(u, f.headers[u]);
            if (f.beforeSend && (f.beforeSend.call(p, w, f) === !1 || 2 === x)) return w.abort();
            b = "abort";
            for (u in { success: 1, error: 1, complete: 1 }) w[u](f[u]);
            if (r = R(xt, f, t, w)) { w.readyState = 1, c && d.trigger("ajaxSend", [w, f]), f.async && f.timeout > 0 && (s = setTimeout(function() { w.abort("timeout") }, f.timeout));
                try { x = 1, r.send(y, n) } catch (T) {
                    if (!(2 > x)) throw T;
                    n(-1, T) } } else n(-1, "No Transport");
            return w }, getJSON: function(e, t, n) {
            return J.get(e, t, n, "json") }, getScript: function(e, t) {
            return J.get(e, void 0, t, "script") } }), J.each(["get", "post"], function(e, t) { J[t] = function(e, n, r, i) {
            return J.isFunction(n) && (i = i || r, r = n, n = void 0), J.ajax({ url: e, type: t, dataType: i, data: n, success: r }) } }), J._evalUrl = function(e) {
        return J.ajax({ url: e, type: "GET", dataType: "script", async: !1, global: !1, "throws": !0 }) }, J.fn.extend({ wrapAll: function(e) {
            var t;
            return J.isFunction(e) ? this.each(function(t) { J(this).wrapAll(e.call(this, t)) }) : (this[0] && (t = J(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                for (var e = this; e.firstElementChild;) e = e.firstElementChild;
                return e }).append(this)), this) }, wrapInner: function(e) {
            return this.each(J.isFunction(e) ? function(t) { J(this).wrapInner(e.call(this, t)) } : function() {
                var t = J(this),
                    n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e) }) }, wrap: function(e) {
            var t = J.isFunction(e);
            return this.each(function(n) { J(this).wrapAll(t ? e.call(this, n) : e) }) }, unwrap: function() {
            return this.parent().each(function() { J.nodeName(this, "body") || J(this).replaceWith(this.childNodes) }).end() } }), J.expr.filters.hidden = function(e) {
        return e.offsetWidth <= 0 && e.offsetHeight <= 0 }, J.expr.filters.visible = function(e) {
        return !J.expr.filters.hidden(e) };
    var Ct = /%20/g,
        kt = /\[\]$/,
        Et = /\r?\n/g,
        St = /^(?:submit|button|image|reset|file)$/i,
        Nt = /^(?:input|select|textarea|keygen)/i;
    J.param = function(e, t) {
        var n, r = [],
            i = function(e, t) { t = J.isFunction(t) ? t() : null == t ? "" : t, r[r.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t) };
        if (void 0 === t && (t = J.ajaxSettings && J.ajaxSettings.traditional), J.isArray(e) || e.jquery && !J.isPlainObject(e)) J.each(e, function() { i(this.name, this.value) });
        else
            for (n in e) _(n, e[n], t, i);
        return r.join("&").replace(Ct, "+") }, J.fn.extend({ serialize: function() {
            return J.param(this.serializeArray()) }, serializeArray: function() {
            return this.map(function() {
                var e = J.prop(this, "elements");
                return e ? J.makeArray(e) : this }).filter(function() {
                var e = this.type;
                return this.name && !J(this).is(":disabled") && Nt.test(this.nodeName) && !St.test(e) && (this.checked || !ke.test(e)) }).map(function(e, t) {
                var n = J(this).val();
                return null == n ? null : J.isArray(n) ? J.map(n, function(e) {
                    return { name: t.name, value: e.replace(Et, "\r\n") } }) : { name: t.name, value: n.replace(Et, "\r\n") } }).get() } }), J.ajaxSettings.xhr = function() {
        try {
            return new XMLHttpRequest } catch (e) {} };
    var jt = 0,
        Dt = {},
        At = { 0: 200, 1223: 204 },
        Lt = J.ajaxSettings.xhr();
    e.attachEvent && e.attachEvent("onunload", function() {
        for (var e in Dt) Dt[e]() }), G.cors = !!Lt && "withCredentials" in Lt, G.ajax = Lt = !!Lt, J.ajaxTransport(function(e) {
        var t;
        return G.cors || Lt && !e.crossDomain ? { send: function(n, r) {
                var i, o = e.xhr(),
                    a = ++jt;
                if (o.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields)
                    for (i in e.xhrFields) o[i] = e.xhrFields[i];
                e.mimeType && o.overrideMimeType && o.overrideMimeType(e.mimeType), e.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                for (i in n) o.setRequestHeader(i, n[i]);
                t = function(e) {
                    return function() { t && (delete Dt[a], t = o.onload = o.onerror = null, "abort" === e ? o.abort() : "error" === e ? r(o.status, o.statusText) : r(At[o.status] || o.status, o.statusText, "string" == typeof o.responseText ? { text: o.responseText } : void 0, o.getAllResponseHeaders())) } }, o.onload = t(), o.onerror = t("error"), t = Dt[a] = t("abort");
                try { o.send(e.hasContent && e.data || null) } catch (s) {
                    if (t) throw s } }, abort: function() { t && t() } } : void 0 }), J.ajaxSetup({ accepts: { script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript" }, contents: { script: /(?:java|ecma)script/ }, converters: { "text script": function(e) {
                return J.globalEval(e), e } } }), J.ajaxPrefilter("script", function(e) { void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET") }), J.ajaxTransport("script", function(e) {
        if (e.crossDomain) {
            var t, n;
            return { send: function(r, i) { t = J("<script>").prop({ async: !0, charset: e.scriptCharset, src: e.url }).on("load error", n = function(e) { t.remove(), n = null, e && i("error" === e.type ? 404 : 200, e.type) }), Z.head.appendChild(t[0]) }, abort: function() { n && n() } } } });
    var Ht = [],
        Ot = /(=)\?(?=&|$)|\?\?/;
    J.ajaxSetup({ jsonp: "callback", jsonpCallback: function() {
            var e = Ht.pop() || J.expando + "_" + ct++;
            return this[e] = !0, e } }), J.ajaxPrefilter("json jsonp", function(t, n, r) {
        var i, o, a, s = t.jsonp !== !1 && (Ot.test(t.url) ? "url" : "string" == typeof t.data && !(t.contentType || "").indexOf("application/x-www-form-urlencoded") && Ot.test(t.data) && "data");
        return s || "jsonp" === t.dataTypes[0] ? (i = t.jsonpCallback = J.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, s ? t[s] = t[s].replace(Ot, "$1" + i) : t.jsonp !== !1 && (t.url += (ut.test(t.url) ? "&" : "?") + t.jsonp + "=" + i), t.converters["script json"] = function() {
            return a || J.error(i + " was not called"), a[0] }, t.dataTypes[0] = "json", o = e[i], e[i] = function() { a = arguments }, r.always(function() { e[i] = o, t[i] && (t.jsonpCallback = n.jsonpCallback, Ht.push(i)), a && J.isFunction(o) && o(a[0]), a = o = void 0 }), "script") : void 0 }), J.parseHTML = function(e, t, n) {
        if (!e || "string" != typeof e) return null; "boolean" == typeof t && (n = t, t = !1), t = t || Z;
        var r = ae.exec(e),
            i = !n && [];
        return r ? [t.createElement(r[1])] : (r = J.buildFragment([e], t, i), i && i.length && J(i).remove(), J.merge([], r.childNodes)) };
    var qt = J.fn.load;
    J.fn.load = function(e, t, n) {
        if ("string" != typeof e && qt) return qt.apply(this, arguments);
        var r, i, o, a = this,
            s = e.indexOf(" ");
        return s >= 0 && (r = J.trim(e.slice(s)), e = e.slice(0, s)), J.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (i = "POST"), a.length > 0 && J.ajax({ url: e, type: i, dataType: "html", data: t }).done(function(e) { o = arguments, a.html(r ? J("<div>").append(J.parseHTML(e)).find(r) : e) }).complete(n && function(e, t) { a.each(n, o || [e.responseText, t, e]) }), this }, J.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) { J.fn[t] = function(e) {
            return this.on(t, e) } }), J.expr.filters.animated = function(e) {
        return J.grep(J.timers, function(t) {
            return e === t.elem }).length };
    var Pt = e.document.documentElement;
    J.offset = { setOffset: function(e, t, n) {
            var r, i, o, a, s, l, c, u = J.css(e, "position"),
                f = J(e),
                p = {}; "static" === u && (e.style.position = "relative"), s = f.offset(), o = J.css(e, "top"), l = J.css(e, "left"), c = ("absolute" === u || "fixed" === u) && (o + l).indexOf("auto") > -1, c ? (r = f.position(), a = r.top, i = r.left) : (a = parseFloat(o) || 0, i = parseFloat(l) || 0), J.isFunction(t) && (t = t.call(e, n, s)), null != t.top && (p.top = t.top - s.top + a), null != t.left && (p.left = t.left - s.left + i), "using" in t ? t.using.call(e, p) : f.css(p) } }, J.fn.extend({ offset: function(e) {
            if (arguments.length) return void 0 === e ? this : this.each(function(t) { J.offset.setOffset(this, e, t) });
            var t, n, r = this[0],
                i = { top: 0, left: 0 },
                o = r && r.ownerDocument;
            return o ? (t = o.documentElement, J.contains(t, r) ? (typeof r.getBoundingClientRect !== Ee && (i = r.getBoundingClientRect()), n = I(o), { top: i.top + n.pageYOffset - t.clientTop, left: i.left + n.pageXOffset - t.clientLeft }) : i) : void 0 }, position: function() {
            if (this[0]) {
                var e, t, n = this[0],
                    r = { top: 0, left: 0 };
                return "fixed" === J.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), J.nodeName(e[0], "html") || (r = e.offset()), r.top += J.css(e[0], "borderTopWidth", !0), r.left += J.css(e[0], "borderLeftWidth", !0)), { top: t.top - r.top - J.css(n, "marginTop", !0), left: t.left - r.left - J.css(n, "marginLeft", !0) } } }, offsetParent: function() {
            return this.map(function() {
                for (var e = this.offsetParent || Pt; e && !J.nodeName(e, "html") && "static" === J.css(e, "position");) e = e.offsetParent;
                return e || Pt }) } }), J.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function(t, n) {
        var r = "pageYOffset" === n;
        J.fn[t] = function(i) {
            return me(this, function(t, i, o) {
                var a = I(t);
                return void 0 === o ? a ? a[n] : t[i] : void(a ? a.scrollTo(r ? e.pageXOffset : o, r ? o : e.pageYOffset) : t[i] = o) }, t, i, arguments.length, null) } }), J.each(["top", "left"], function(e, t) { J.cssHooks[t] = T(G.pixelPosition, function(e, n) {
            return n ? (n = w(e, t), $e.test(n) ? J(e).position()[t] + "px" : n) : void 0 }) }), J.each({ Height: "height", Width: "width" }, function(e, t) { J.each({ padding: "inner" + e, content: t, "": "outer" + e }, function(n, r) { J.fn[r] = function(r, i) {
                var o = arguments.length && (n || "boolean" != typeof r),
                    a = n || (r === !0 || i === !0 ? "margin" : "border");
                return me(this, function(t, n, r) {
                    var i;
                    return J.isWindow(t) ? t.document.documentElement["client" + e] : 9 === t.nodeType ? (i = t.documentElement, Math.max(t.body["scroll" + e], i["scroll" + e], t.body["offset" + e], i["offset" + e], i["client" + e])) : void 0 === r ? J.css(t, n, a) : J.style(t, n, r, a) }, t, o ? r : void 0, o, null) } }) }), J.fn.size = function() {
        return this.length }, J.fn.andSelf = J.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function() {
        return J });
    var Rt = e.jQuery,
        Wt = e.$;
    return J.noConflict = function(t) {
        return e.$ === J && (e.$ = Wt), t && e.jQuery === J && (e.jQuery = Rt), J }, typeof t === Ee && (e.jQuery = e.$ = J), J
}),
function(e, t, n, r) {
    var i = n("html"),
        o = n(e),
        a = n(t),
        s = n.fancybox = function() { s.open.apply(this, arguments) },
        l = navigator.userAgent.match(/msie/i),
        c = null,
        u = t.createTouch !== r,
        f = function(e) {
            return e && e.hasOwnProperty && e instanceof n },
        p = function(e) {
            return e && "string" === n.type(e) },
        d = function(e) {
            return p(e) && 0 < e.indexOf("%") },
        h = function(e, t) {
            var n = parseInt(e, 10) || 0;
            return t && d(e) && (n *= s.getViewport()[t] / 100), Math.ceil(n) },
        g = function(e, t) {
            return h(e, t) + "px" };
    n.extend(s, {
        version: "2.1.5",
        defaults: { padding: 15, margin: 20, width: 800, height: 600, minWidth: 100, minHeight: 100, maxWidth: 9999, maxHeight: 9999, pixelRatio: 1, autoSize: !0, autoHeight: !1, autoWidth: !1, autoResize: !0, autoCenter: !u, fitToView: !0, aspectRatio: !1, topRatio: .5, leftRatio: .5, scrolling: "auto", wrapCSS: "", arrows: !0, closeBtn: !0, closeClick: !1, nextClick: !1, mouseWheel: !0, autoPlay: !1, playSpeed: 3e3, preload: 3, modal: !1, loop: !0, ajax: { dataType: "html", headers: { "X-fancyBox": !0 } }, iframe: { scrolling: "auto", preload: !0 }, swf: { wmode: "transparent", allowfullscreen: "true", allowscriptaccess: "always" }, keys: { next: { 13: "left", 34: "up", 39: "left", 40: "up" }, prev: { 8: "right", 33: "down", 37: "right", 38: "down" }, close: [27], play: [32], toggle: [70] }, direction: { next: "left", prev: "right" }, scrollOutside: !0, index: 0, type: null, href: null, content: null, title: null, tpl: { wrap: '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>', image: '<img class="fancybox-image" src="{href}" alt="" />', iframe: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen' + (l ? ' allowtransparency="true"' : "") + "></iframe>", error: '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>', closeBtn: '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>', next: '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>', prev: '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>' }, openEffect: "fade", openSpeed: 250, openEasing: "swing", openOpacity: !0, openMethod: "zoomIn", closeEffect: "fade", closeSpeed: 250, closeEasing: "swing", closeOpacity: !0, closeMethod: "zoomOut", nextEffect: "elastic", nextSpeed: 250, nextEasing: "swing", nextMethod: "changeIn", prevEffect: "elastic", prevSpeed: 250, prevEasing: "swing", prevMethod: "changeOut", helpers: { overlay: !0, title: !0 }, onCancel: n.noop, beforeLoad: n.noop, afterLoad: n.noop, beforeShow: n.noop, afterShow: n.noop, beforeChange: n.noop, beforeClose: n.noop, afterClose: n.noop },
        group: {},
        opts: {},
        previous: null,
        coming: null,
        current: null,
        isActive: !1,
        isOpen: !1,
        isOpened: !1,
        wrap: null,
        skin: null,
        outer: null,
        inner: null,
        player: { timer: null, isActive: !1 },
        ajaxLoad: null,
        imgPreload: null,
        transitions: {},
        helpers: {},
        open: function(e, t) {
            return e && (n.isPlainObject(t) || (t = {}), !1 !== s.close(!0)) ? (n.isArray(e) || (e = f(e) ? n(e).get() : [e]), n.each(e, function(i, o) {
                var a, l, c, u, d, h = {}; "object" === n.type(o) && (o.nodeType && (o = n(o)), f(o) ? (h = { href: o.data("fancybox-href") || o.attr("href"), title: o.data("fancybox-title") || o.attr("title"), isDom: !0, element: o }, n.metadata && n.extend(!0, h, o.metadata())) : h = o), a = t.href || h.href || (p(o) ? o : null), l = t.title !== r ? t.title : h.title || "", u = (c = t.content || h.content) ? "html" : t.type || h.type, !u && h.isDom && (u = o.data("fancybox-type"), u || (u = (u = o.prop("class").match(/fancybox\.(\w+)/)) ? u[1] : null)), p(a) && (u || (s.isImage(a) ? u = "image" : s.isSWF(a) ? u = "swf" : "#" === a.charAt(0) ? u = "inline" : p(o) && (u = "html", c = o)), "ajax" === u && (d = a.split(/\s+/, 2), a = d.shift(), d = d.shift())), c || ("inline" === u ? a ? c = n(p(a) ? a.replace(/.*(?=#[^\s]+$)/, "") : a) : h.isDom && (c = o) : "html" === u ? c = a : !u && !a && h.isDom && (u = "inline", c = o)), n.extend(h, { href: a, type: u, content: c, title: l, selector: d }), e[i] = h }), s.opts = n.extend(!0, {}, s.defaults, t), t.keys !== r && (s.opts.keys = t.keys ? n.extend({}, s.defaults.keys, t.keys) : !1), s.group = e, s._start(s.opts.index)) : void 0 },
        cancel: function() {
            var e = s.coming;
            e && !1 !== s.trigger("onCancel") && (s.hideLoading(), s.ajaxLoad && s.ajaxLoad.abort(), s.ajaxLoad = null, s.imgPreload && (s.imgPreload.onload = s.imgPreload.onerror = null), e.wrap && e.wrap.stop(!0, !0).trigger("onReset").remove(), s.coming = null, s.current || s._afterZoomOut(e)) },
        close: function(e) { s.cancel(), !1 !== s.trigger("beforeClose") && (s.unbindEvents(), s.isActive && (s.isOpen && !0 !== e ? (s.isOpen = s.isOpened = !1, s.isClosing = !0, n(".fancybox-item, .fancybox-nav").remove(), s.wrap.stop(!0, !0).removeClass("fancybox-opened"), s.transitions[s.current.closeMethod]()) : (n(".fancybox-wrap").stop(!0).trigger("onReset").remove(), s._afterZoomOut()))) },
        play: function(e) {
            var t = function() { clearTimeout(s.player.timer) },
                n = function() { t(), s.current && s.player.isActive && (s.player.timer = setTimeout(s.next, s.current.playSpeed)) },
                r = function() { t(), a.unbind(".player"), s.player.isActive = !1, s.trigger("onPlayEnd") };!0 === e || !s.player.isActive && !1 !== e ? s.current && (s.current.loop || s.current.index < s.group.length - 1) && (s.player.isActive = !0, a.bind({ "onCancel.player beforeClose.player": r, "onUpdate.player": n, "beforeLoad.player": t }), n(), s.trigger("onPlayStart")) : r() },
        next: function(e) {
            var t = s.current;
            t && (p(e) || (e = t.direction.next), s.jumpto(t.index + 1, e, "next")) },
        prev: function(e) {
            var t = s.current;
            t && (p(e) || (e = t.direction.prev), s.jumpto(t.index - 1, e, "prev")) },
        jumpto: function(e, t, n) {
            var i = s.current;
            i && (e = h(e), s.direction = t || i.direction[e >= i.index ? "next" : "prev"], s.router = n || "jumpto", i.loop && (0 > e && (e = i.group.length + e % i.group.length), e %= i.group.length), i.group[e] !== r && (s.cancel(), s._start(e))) },
        reposition: function(e, t) {
            var r, i = s.current,
                o = i ? i.wrap : null;
            o && (r = s._getPosition(t), e && "scroll" === e.type ? (delete r.position, o.stop(!0, !0).animate(r, 200)) : (o.css(r), i.pos = n.extend({}, i.dim, r))) },
        update: function(e) {
            var t = e && e.type,
                n = !t || "orientationchange" === t;
            n && (clearTimeout(c), c = null), s.isOpen && !c && (c = setTimeout(function() {
                var r = s.current;
                r && !s.isClosing && (s.wrap.removeClass("fancybox-tmp"), (n || "load" === t || "resize" === t && r.autoResize) && s._setDimension(), "scroll" === t && r.canShrink || s.reposition(e), s.trigger("onUpdate"), c = null) }, n && !u ? 0 : 300)) },
        toggle: function(e) { s.isOpen && (s.current.fitToView = "boolean" === n.type(e) ? e : !s.current.fitToView, u && (s.wrap.removeAttr("style").addClass("fancybox-tmp"), s.trigger("onUpdate")), s.update()) },
        hideLoading: function() { a.unbind(".loading"), n("#fancybox-loading").remove() },
        showLoading: function() {
            var e, t;
            s.hideLoading(), e = n('<div id="fancybox-loading"><div></div></div>').click(s.cancel).appendTo("body"), a.bind("keydown.loading", function(e) { 27 === (e.which || e.keyCode) && (e.preventDefault(), s.cancel()) }), s.defaults.fixed || (t = s.getViewport(), e.css({ position: "absolute", top: .5 * t.h + t.y, left: .5 * t.w + t.x })) },
        getViewport: function() {
            var t = s.current && s.current.locked || !1,
                n = { x: o.scrollLeft(), y: o.scrollTop() };
            return t ? (n.w = t[0].clientWidth, n.h = t[0].clientHeight) : (n.w = u && e.innerWidth ? e.innerWidth : o.width(), n.h = u && e.innerHeight ? e.innerHeight : o.height()), n },
        unbindEvents: function() { s.wrap && f(s.wrap) && s.wrap.unbind(".fb"), a.unbind(".fb"), o.unbind(".fb") },
        bindEvents: function() {
            var e, t = s.current;
            t && (o.bind("orientationchange.fb" + (u ? "" : " resize.fb") + (t.autoCenter && !t.locked ? " scroll.fb" : ""), s.update), (e = t.keys) && a.bind("keydown.fb", function(i) {
                var o = i.which || i.keyCode,
                    a = i.target || i.srcElement;
                return 27 === o && s.coming ? !1 : void(!i.ctrlKey && !i.altKey && !i.shiftKey && !i.metaKey && (!a || !a.type && !n(a).is("[contenteditable]")) && n.each(e, function(e, a) {
                    return 1 < t.group.length && a[o] !== r ? (s[e](a[o]), i.preventDefault(), !1) : -1 < n.inArray(o, a) ? (s[e](), i.preventDefault(), !1) : void 0 })) }), n.fn.mousewheel && t.mouseWheel && s.wrap.bind("mousewheel.fb", function(e, r, i, o) {
                for (var a = n(e.target || null), l = !1; a.length && !l && !a.is(".fancybox-skin") && !a.is(".fancybox-wrap");) l = a[0] && !(a[0].style.overflow && "hidden" === a[0].style.overflow) && (a[0].clientWidth && a[0].scrollWidth > a[0].clientWidth || a[0].clientHeight && a[0].scrollHeight > a[0].clientHeight), a = n(a).parent();
                0 !== r && !l && 1 < s.group.length && !t.canShrink && (o > 0 || i > 0 ? s.prev(o > 0 ? "down" : "left") : (0 > o || 0 > i) && s.next(0 > o ? "up" : "right"), e.preventDefault()) })) },
        trigger: function(e, t) {
            var r, i = t || s.coming || s.current;
            if (i) {
                if (n.isFunction(i[e]) && (r = i[e].apply(i, Array.prototype.slice.call(arguments, 1))), !1 === r) return !1;
                i.helpers && n.each(i.helpers, function(t, r) { r && s.helpers[t] && n.isFunction(s.helpers[t][e]) && s.helpers[t][e](n.extend(!0, {}, s.helpers[t].defaults, r), i) }), a.trigger(e) } },
        isImage: function(e) {
            return p(e) && e.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i) },
        isSWF: function(e) {
            return p(e) && e.match(/\.(swf)((\?|#).*)?$/i) },
        _start: function(e) {
            var t, r, i = {};
            if (e = h(e), t = s.group[e] || null, !t) return !1;
            if (i = n.extend(!0, {}, s.opts, t), t = i.margin, r = i.padding, "number" === n.type(t) && (i.margin = [t, t, t, t]), "number" === n.type(r) && (i.padding = [r, r, r, r]), i.modal && n.extend(!0, i, { closeBtn: !1, closeClick: !1, nextClick: !1, arrows: !1, mouseWheel: !1, keys: null, helpers: { overlay: { closeClick: !1 } } }), i.autoSize && (i.autoWidth = i.autoHeight = !0), "auto" === i.width && (i.autoWidth = !0), "auto" === i.height && (i.autoHeight = !0), i.group = s.group, i.index = e, s.coming = i, !1 === s.trigger("beforeLoad")) s.coming = null;
            else {
                if (r = i.type, t = i.href, !r) return s.coming = null, s.current && s.router && "jumpto" !== s.router ? (s.current.index = e, s[s.router](s.direction)) : !1;
                if (s.isActive = !0, ("image" === r || "swf" === r) && (i.autoHeight = i.autoWidth = !1, i.scrolling = "visible"), "image" === r && (i.aspectRatio = !0), "iframe" === r && u && (i.scrolling = "scroll"), i.wrap = n(i.tpl.wrap).addClass("fancybox-" + (u ? "mobile" : "desktop") + " fancybox-type-" + r + " fancybox-tmp " + i.wrapCSS).appendTo(i.parent || "body"), n.extend(i, { skin: n(".fancybox-skin", i.wrap), outer: n(".fancybox-outer", i.wrap), inner: n(".fancybox-inner", i.wrap) }), n.each(["Top", "Right", "Bottom", "Left"], function(e, t) { i.skin.css("padding" + t, g(i.padding[e])) }), s.trigger("onReady"), "inline" === r || "html" === r) {
                    if (!i.content || !i.content.length) return s._error("content") } else if (!t) return s._error("href"); "image" === r ? s._loadImage() : "ajax" === r ? s._loadAjax() : "iframe" === r ? s._loadIframe() : s._afterLoad() } },
        _error: function(e) { n.extend(s.coming, { type: "html", autoWidth: !0, autoHeight: !0, minWidth: 0, minHeight: 0, scrolling: "no", hasError: e, content: s.coming.tpl.error }), s._afterLoad() },
        _loadImage: function() {
            var e = s.imgPreload = new Image;
            e.onload = function() { this.onload = this.onerror = null, s.coming.width = this.width / s.opts.pixelRatio, s.coming.height = this.height / s.opts.pixelRatio, s._afterLoad() }, e.onerror = function() { this.onload = this.onerror = null, s._error("image") }, e.src = s.coming.href, !0 !== e.complete && s.showLoading() },
        _loadAjax: function() {
            var e = s.coming;
            s.showLoading(), s.ajaxLoad = n.ajax(n.extend({}, e.ajax, { url: e.href, error: function(e, t) { s.coming && "abort" !== t ? s._error("ajax", e) : s.hideLoading() }, success: function(t, n) { "success" === n && (e.content = t, s._afterLoad()) } })) },
        _loadIframe: function() {
            var e = s.coming,
                t = n(e.tpl.iframe.replace(/\{rnd\}/g, (new Date).getTime())).attr("scrolling", u ? "auto" : e.iframe.scrolling).attr("src", e.href);
            n(e.wrap).bind("onReset", function() {
                try { n(this).find("iframe").hide().attr("src", "//about:blank").end().empty() } catch (e) {} }), e.iframe.preload && (s.showLoading(), t.one("load", function() { n(this).data("ready", 1), u || n(this).bind("load.fb", s.update), n(this).parents(".fancybox-wrap").width("100%").removeClass("fancybox-tmp").show(), s._afterLoad() })), e.content = t.appendTo(e.inner), e.iframe.preload || s._afterLoad() },
        _preloadImages: function() {
            var e, t, n = s.group,
                r = s.current,
                i = n.length,
                o = r.preload ? Math.min(r.preload, i - 1) : 0;
            for (t = 1; o >= t; t += 1) e = n[(r.index + t) % i], "image" === e.type && e.href && ((new Image).src = e.href) },
        _afterLoad: function() {
            var e, t, r, i, o, a = s.coming,
                l = s.current;
            if (s.hideLoading(), a && !1 !== s.isActive)
                if (!1 === s.trigger("afterLoad", a, l)) a.wrap.stop(!0).trigger("onReset").remove(), s.coming = null;
                else {
                    switch (l && (s.trigger("beforeChange", l), l.wrap.stop(!0).removeClass("fancybox-opened").find(".fancybox-item, .fancybox-nav").remove()),
                        s.unbindEvents(), e = a.content, t = a.type, r = a.scrolling, n.extend(s, { wrap: a.wrap, skin: a.skin, outer: a.outer, inner: a.inner, current: a, previous: l }), i = a.href, t) {
                        case "inline":
                        case "ajax":
                        case "html":
                            a.selector ? e = n("<div>").html(e).find(a.selector) : f(e) && (e.data("fancybox-placeholder") || e.data("fancybox-placeholder", n('<div class="fancybox-placeholder"></div>').insertAfter(e).hide()), e = e.show().detach(), a.wrap.bind("onReset", function() { n(this).find(e).length && e.hide().replaceAll(e.data("fancybox-placeholder")).data("fancybox-placeholder", !1) }));
                            break;
                        case "image":
                            e = a.tpl.image.replace("{href}", i);
                            break;
                        case "swf":
                            e = '<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="' + i + '"></param>', o = "", n.each(a.swf, function(t, n) { e += '<param name="' + t + '" value="' + n + '"></param>', o += " " + t + '="' + n + '"' }), e += '<embed src="' + i + '" type="application/x-shockwave-flash" width="100%" height="100%"' + o + "></embed></object>" }(!f(e) || !e.parent().is(a.inner)) && a.inner.append(e), s.trigger("beforeShow"), a.inner.css("overflow", "yes" === r ? "scroll" : "no" === r ? "hidden" : r), s._setDimension(), s.reposition(), s.isOpen = !1, s.coming = null, s.bindEvents(), s.isOpened ? l.prevMethod && s.transitions[l.prevMethod]() : n(".fancybox-wrap").not(a.wrap).stop(!0).trigger("onReset").remove(), s.transitions[s.isOpened ? a.nextMethod : a.openMethod](), s._preloadImages()
                }
        },
        _setDimension: function() {
            var e, t, r, i, o, a, l, c, u, f = s.getViewport(),
                p = 0,
                m = !1,
                y = !1,
                m = s.wrap,
                v = s.skin,
                x = s.inner,
                b = s.current,
                y = b.width,
                w = b.height,
                T = b.minWidth,
                C = b.minHeight,
                k = b.maxWidth,
                E = b.maxHeight,
                S = b.scrolling,
                N = b.scrollOutside ? b.scrollbarWidth : 0,
                j = b.margin,
                D = h(j[1] + j[3]),
                A = h(j[0] + j[2]);
            if (m.add(v).add(x).width("auto").height("auto").removeClass("fancybox-tmp"), j = h(v.outerWidth(!0) - v.width()), e = h(v.outerHeight(!0) - v.height()), t = D + j, r = A + e, i = d(y) ? (f.w - t) * h(y) / 100 : y, o = d(w) ? (f.h - r) * h(w) / 100 : w, "iframe" === b.type) {
                if (u = b.content, b.autoHeight && 1 === u.data("ready")) try { u[0].contentWindow.document.location && (x.width(i).height(9999), a = u.contents().find("body"), N && a.css("overflow-x", "hidden"), o = a.outerHeight(!0)) } catch (L) {} } else(b.autoWidth || b.autoHeight) && (x.addClass("fancybox-tmp"), b.autoWidth || x.width(i), b.autoHeight || x.height(o), b.autoWidth && (i = x.width()), b.autoHeight && (o = x.height()), x.removeClass("fancybox-tmp"));
            if (y = h(i), w = h(o), c = i / o, T = h(d(T) ? h(T, "w") - t : T), k = h(d(k) ? h(k, "w") - t : k), C = h(d(C) ? h(C, "h") - r : C), E = h(d(E) ? h(E, "h") - r : E), a = k, l = E, b.fitToView && (k = Math.min(f.w - t, k), E = Math.min(f.h - r, E)), t = f.w - D, A = f.h - A, b.aspectRatio ? (y > k && (y = k, w = h(y / c)), w > E && (w = E, y = h(w * c)), T > y && (y = T, w = h(y / c)), C > w && (w = C, y = h(w * c))) : (y = Math.max(T, Math.min(y, k)), b.autoHeight && "iframe" !== b.type && (x.width(y), w = x.height()), w = Math.max(C, Math.min(w, E))), b.fitToView)
                if (x.width(y).height(w), m.width(y + j), f = m.width(), D = m.height(), b.aspectRatio)
                    for (;
                        (f > t || D > A) && y > T && w > C && !(19 < p++);) w = Math.max(C, Math.min(E, w - 10)), y = h(w * c), T > y && (y = T, w = h(y / c)), y > k && (y = k, w = h(y / c)), x.width(y).height(w), m.width(y + j), f = m.width(), D = m.height();
                else y = Math.max(T, Math.min(y, y - (f - t))), w = Math.max(C, Math.min(w, w - (D - A)));
            N && "auto" === S && o > w && t > y + j + N && (y += N), x.width(y).height(w), m.width(y + j), f = m.width(), D = m.height(), m = (f > t || D > A) && y > T && w > C, y = b.aspectRatio ? a > y && l > w && i > y && o > w : (a > y || l > w) && (i > y || o > w), n.extend(b, { dim: { width: g(f), height: g(D) }, origWidth: i, origHeight: o, canShrink: m, canExpand: y, wPadding: j, hPadding: e, wrapSpace: D - v.outerHeight(!0), skinSpace: v.height() - w }), !u && b.autoHeight && w > C && E > w && !y && x.height("auto") },
        _getPosition: function(e) {
            var t = s.current,
                n = s.getViewport(),
                r = t.margin,
                i = s.wrap.width() + r[1] + r[3],
                o = s.wrap.height() + r[0] + r[2],
                r = { position: "absolute", top: r[0], left: r[3] };
            return t.autoCenter && t.fixed && !e && o <= n.h && i <= n.w ? r.position = "fixed" : t.locked || (r.top += n.y, r.left += n.x), r.top = g(Math.max(r.top, r.top + (n.h - o) * t.topRatio)), r.left = g(Math.max(r.left, r.left + (n.w - i) * t.leftRatio)), r },
        _afterZoomIn: function() {
            var e = s.current;
            e && (s.isOpen = s.isOpened = !0, s.wrap.css("overflow", "visible").addClass("fancybox-opened"), s.update(), (e.closeClick || e.nextClick && 1 < s.group.length) && s.inner.css("cursor", "pointer").bind("click.fb", function(t) {!n(t.target).is("a") && !n(t.target).parent().is("a") && (t.preventDefault(), s[e.closeClick ? "close" : "next"]()) }), e.closeBtn && n(e.tpl.closeBtn).appendTo(s.skin).bind("click.fb", function(e) { e.preventDefault(), s.close() }), e.arrows && 1 < s.group.length && ((e.loop || 0 < e.index) && n(e.tpl.prev).appendTo(s.outer).bind("click.fb", s.prev), (e.loop || e.index < s.group.length - 1) && n(e.tpl.next).appendTo(s.outer).bind("click.fb", s.next)), s.trigger("afterShow"), e.loop || e.index !== e.group.length - 1 ? s.opts.autoPlay && !s.player.isActive && (s.opts.autoPlay = !1, s.play()) : s.play(!1)) },
        _afterZoomOut: function(e) { e = e || s.current, n(".fancybox-wrap").trigger("onReset").remove(), n.extend(s, { group: {}, opts: {}, router: !1, current: null, isActive: !1, isOpened: !1, isOpen: !1, isClosing: !1, wrap: null, skin: null, outer: null, inner: null }), s.trigger("afterClose", e) }
    }), s.transitions = { getOrigPosition: function() {
            var e = s.current,
                t = e.element,
                n = e.orig,
                r = {},
                i = 50,
                o = 50,
                a = e.hPadding,
                l = e.wPadding,
                c = s.getViewport();
            return !n && e.isDom && t.is(":visible") && (n = t.find("img:first"), n.length || (n = t)), f(n) ? (r = n.offset(), n.is("img") && (i = n.outerWidth(), o = n.outerHeight())) : (r.top = c.y + (c.h - o) * e.topRatio, r.left = c.x + (c.w - i) * e.leftRatio), ("fixed" === s.wrap.css("position") || e.locked) && (r.top -= c.y, r.left -= c.x), r = { top: g(r.top - a * e.topRatio), left: g(r.left - l * e.leftRatio), width: g(i + l), height: g(o + a) } }, step: function(e, t) {
            var n, r, i = t.prop;
            r = s.current;
            var o = r.wrapSpace,
                a = r.skinSpace;
            ("width" === i || "height" === i) && (n = t.end === t.start ? 1 : (e - t.start) / (t.end - t.start), s.isClosing && (n = 1 - n), r = "width" === i ? r.wPadding : r.hPadding, r = e - r, s.skin[i](h("width" === i ? r : r - o * n)), s.inner[i](h("width" === i ? r : r - o * n - a * n))) }, zoomIn: function() {
            var e = s.current,
                t = e.pos,
                r = e.openEffect,
                i = "elastic" === r,
                o = n.extend({ opacity: 1 }, t);
            delete o.position, i ? (t = this.getOrigPosition(), e.openOpacity && (t.opacity = .1)) : "fade" === r && (t.opacity = .1), s.wrap.css(t).animate(o, { duration: "none" === r ? 0 : e.openSpeed, easing: e.openEasing, step: i ? this.step : null, complete: s._afterZoomIn }) }, zoomOut: function() {
            var e = s.current,
                t = e.closeEffect,
                n = "elastic" === t,
                r = { opacity: .1 };
            n && (r = this.getOrigPosition(), e.closeOpacity && (r.opacity = .1)), s.wrap.animate(r, { duration: "none" === t ? 0 : e.closeSpeed, easing: e.closeEasing, step: n ? this.step : null, complete: s._afterZoomOut }) }, changeIn: function() {
            var e, t = s.current,
                n = t.nextEffect,
                r = t.pos,
                i = { opacity: 1 },
                o = s.direction;
            r.opacity = .1, "elastic" === n && (e = "down" === o || "up" === o ? "top" : "left", "down" === o || "right" === o ? (r[e] = g(h(r[e]) - 200), i[e] = "+=200px") : (r[e] = g(h(r[e]) + 200), i[e] = "-=200px")), "none" === n ? s._afterZoomIn() : s.wrap.css(r).animate(i, { duration: t.nextSpeed, easing: t.nextEasing, complete: s._afterZoomIn }) }, changeOut: function() {
            var e = s.previous,
                t = e.prevEffect,
                r = { opacity: .1 },
                i = s.direction; "elastic" === t && (r["down" === i || "up" === i ? "top" : "left"] = ("up" === i || "left" === i ? "-" : "+") + "=200px"), e.wrap.animate(r, { duration: "none" === t ? 0 : e.prevSpeed, easing: e.prevEasing, complete: function() { n(this).trigger("onReset").remove() } }) } }, s.helpers.overlay = { defaults: { closeClick: !0, speedOut: 200, showEarly: !0, css: {}, locked: !u, fixed: !0 }, overlay: null, fixed: !1, el: n("html"), create: function(e) { e = n.extend({}, this.defaults, e), this.overlay && this.close(), this.overlay = n('<div class="fancybox-overlay"></div>').appendTo(s.coming ? s.coming.parent : e.parent), this.fixed = !1, e.fixed && s.defaults.fixed && (this.overlay.addClass("fancybox-overlay-fixed"), this.fixed = !0) }, open: function(e) {
            var t = this;
            e = n.extend({}, this.defaults, e), this.overlay ? this.overlay.unbind(".overlay").width("auto").height("auto") : this.create(e), this.fixed || (o.bind("resize.overlay", n.proxy(this.update, this)), this.update()), e.closeClick && this.overlay.bind("click.overlay", function(e) {
                return n(e.target).hasClass("fancybox-overlay") ? (s.isActive ? s.close() : t.close(), !1) : void 0 }), this.overlay.css(e.css).show() }, close: function() {
            var e, t;
            o.unbind("resize.overlay"), this.el.hasClass("fancybox-lock") && (n(".fancybox-margin").removeClass("fancybox-margin"), e = o.scrollTop(), t = o.scrollLeft(), this.el.removeClass("fancybox-lock"), o.scrollTop(e).scrollLeft(t)), n(".fancybox-overlay").remove().hide(), n.extend(this, { overlay: null, fixed: !1 }) }, update: function() {
            var e, n = "100%";
            this.overlay.width(n).height("100%"), l ? (e = Math.max(t.documentElement.offsetWidth, t.body.offsetWidth), a.width() > e && (n = a.width())) : a.width() > o.width() && (n = a.width()), this.overlay.width(n).height(a.height()) }, onReady: function(e, t) {
            var r = this.overlay;
            n(".fancybox-overlay").stop(!0, !0), r || this.create(e), e.locked && this.fixed && t.fixed && (r || (this.margin = a.height() > o.height() ? n("html").css("margin-right").replace("px", "") : !1), t.locked = this.overlay.append(t.wrap), t.fixed = !1), !0 === e.showEarly && this.beforeShow.apply(this, arguments) }, beforeShow: function(e, t) {
            var r, i;
            t.locked && (!1 !== this.margin && (n("*").filter(function() {
                return "fixed" === n(this).css("position") && !n(this).hasClass("fancybox-overlay") && !n(this).hasClass("fancybox-wrap") }).addClass("fancybox-margin"), this.el.addClass("fancybox-margin")), r = o.scrollTop(), i = o.scrollLeft(), this.el.addClass("fancybox-lock"), o.scrollTop(r).scrollLeft(i)), this.open(e) }, onUpdate: function() { this.fixed || this.update() }, afterClose: function(e) { this.overlay && !s.coming && this.overlay.fadeOut(e.speedOut, n.proxy(this.close, this)) } }, s.helpers.title = { defaults: { type: "float", position: "bottom" }, beforeShow: function(e) {
            var t = s.current,
                r = t.title,
                i = e.type;
            if (n.isFunction(r) && (r = r.call(t.element, t)), p(r) && "" !== n.trim(r)) {
                switch (t = n('<div class="fancybox-title fancybox-title-' + i + '-wrap">' + r + "</div>"), i) {
                    case "inside":
                        i = s.skin;
                        break;
                    case "outside":
                        i = s.wrap;
                        break;
                    case "over":
                        i = s.inner;
                        break;
                    default:
                        i = s.skin, t.appendTo("body"), l && t.width(t.width()), t.wrapInner('<span class="child"></span>'), s.current.margin[2] += Math.abs(h(t.css("margin-bottom"))) }
                t["top" === e.position ? "prependTo" : "appendTo"](i) } } }, n.fn.fancybox = function(e) {
        var t, r = n(this),
            i = this.selector || "",
            o = function(o) {
                var a, l, c = n(this).blur(),
                    u = t;!o.ctrlKey && !o.altKey && !o.shiftKey && !o.metaKey && !c.is(".fancybox-wrap") && (a = e.groupAttr || "data-fancybox-group", l = c.attr(a), l || (a = "rel", l = c.get(0)[a]), l && "" !== l && "nofollow" !== l && (c = i.length ? n(i) : r, c = c.filter("[" + a + '="' + l + '"]'), u = c.index(this)), e.index = u, !1 !== s.open(c, e) && o.preventDefault()) };
        return e = e || {}, t = e.index || 0, i && !1 !== e.live ? a.undelegate(i, "click.fb-start").delegate(i + ":not('.fancybox-item, .fancybox-nav')", "click.fb-start", o) : r.unbind("click.fb-start").bind("click.fb-start", o), this.filter("[data-fancybox-start=1]").trigger("click"), this }, a.ready(function() {
        var t, o;
        if (n.scrollbarWidth === r && (n.scrollbarWidth = function() {
                var e = n('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo("body"),
                    t = e.children(),
                    t = t.innerWidth() - t.height(99).innerWidth();
                return e.remove(), t }), n.support.fixedPosition === r) { t = n.support, o = n('<div style="position:fixed;top:20px;"></div>').appendTo("body");
            var a = 20 === o[0].offsetTop || 15 === o[0].offsetTop;
            o.remove(), t.fixedPosition = a }
        n.extend(s.defaults, { scrollbarWidth: n.scrollbarWidth(), fixed: n.support.fixedPosition, parent: n("body") }), t = n(e).width(), i.addClass("fancybox-lock-test"), o = n(e).width(), i.removeClass("fancybox-lock-test"), n("<style type='text/css'>.fancybox-margin{margin-right:" + (o - t) + "px;}</style>").appendTo("head") })
}(window, document, jQuery), $(function() {
    function e() {
        var e = { zoom: 16, scrollwheel: !1, center: new google.maps.LatLng(55.748228, 37.540254), styles: [{ featureType: "all", elementType: "labels.text.fill", stylers: [{ saturation: 36 }, { color: "#333333" }, { lightness: 40 }] }, { featureType: "all", elementType: "labels.text.stroke", stylers: [{ visibility: "on" }, { color: "#ffffff" }, { lightness: 16 }] }, { featureType: "all", elementType: "labels.icon", stylers: [{ visibility: "off" }] }, { featureType: "administrative", elementType: "geometry.fill", stylers: [{ color: "#fefefe" }, { lightness: 20 }] }, { featureType: "administrative", elementType: "geometry.stroke", stylers: [{ color: "#fefefe" }, { lightness: 17 }, { weight: 1.2 }] }, { featureType: "landscape", elementType: "geometry", stylers: [{ color: "#f5f5f5" }, { lightness: 20 }] }, { featureType: "poi", elementType: "geometry", stylers: [{ color: "#f5f5f5" }, { lightness: 21 }] }, { featureType: "poi.park", elementType: "geometry", stylers: [{ color: "#dedede" }, { lightness: 21 }] }, { featureType: "road.highway", elementType: "geometry.fill", stylers: [{ color: "#ffffff" }, { lightness: 17 }] }, { featureType: "road.highway", elementType: "geometry.stroke", stylers: [{ color: "#ffffff" }, { lightness: 29 }, { weight: .2 }] }, { featureType: "road.arterial", elementType: "geometry", stylers: [{ color: "#ffffff" }, { lightness: 18 }] }, { featureType: "road.local", elementType: "geometry", stylers: [{ color: "#ffffff" }, { lightness: 16 }] }, { featureType: "transit", elementType: "geometry", stylers: [{ color: "#f2f2f2" }, { lightness: 19 }] }, { featureType: "water", elementType: "geometry", stylers: [{ color: "#e9e9e9" }, { lightness: 17 }] }] },
            t = document.getElementById("map"),
            n = new google.maps.Map(t, e),
            r = "img/marker.png";
        new google.maps.Marker({ position: new google.maps.LatLng(55.748228, 37.540254), map: n, title: "Snazzy!", icon: r }) }
    google.maps.event.addDomListener(window, "load", e), $("[data-bg]").each(function() {
        var e = $(this).attr("data-bg");
        $(this).css({ "background-image": "url(" + e + ")" }), $(this).css({ "background-size": "cover" }) }), $(".steps-block-hidden__link").fancybox() });


//ilya
$(function() {
    console.log('map1');
    if ($('body').width() < 600) {
        console.log('map');
        $('#map_wrapper').append($('<div class="map_wrapper"></div>'));
        $('.steps-block').click(function() {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $('.steps-block').removeClass('active');
                $(this).addClass('active');
            }
        });

    } else {


    }
    $('.map_wrapper').click(function() {
        $('.map_wrapper').hide();
    });


    $('.steps-block-hidden__link').click(function() {
        $('#site').val($(this).data('site'));
        $('#modal .modal__title').text('Презентация ' + $(this).data('site'));
        $('#crm_type').val($(this).data('crm'));
        $('#pdf').val($(this).data('pdf'));
        text = '';
        texts = $(this).closest('.steps-block-hidden__content').find('.steps-block-text_for_modals');
        for (var i = 0; i < texts.length; i++) {
            text += texts.eq(i).text() + '<br>';
        }
        src = 'img/' + $(this).data('img');

        $('#modal .modal-form__title').html(text);
        $('#modal img').attr('src', src);

    });

    $('form').submit(function(e) {
        e.preventDefault();
        $inps = $(this).find('input[type="text"]');
        error = false;
        $inps.removeClass('error');
        for (var i = 0; i < $inps.length; i++) {
            $i = $inps.eq(i);
            val = $i.val();
            if (val.length < 2) {
                error = true;
                $i.addClass('error');
            }
        }
        if (error) {
            return false;
        }
        pdf = $('#pdf').val();
        $.ajax({
            method: 'post',
            data: $(this).serialize(),
            url: '/send.php',
            success: function() {
                $('.fancybox-overlay').click();
                $('.thanks').fancybox().click();
                name = '/presentation/' + pdf;
                window.location.pathname = name;
                // $("#thx").modal();
            }
        });
        return false;
    });


});
/*! modernizr 3.3.1 (Custom Build) | MIT *
 * https://modernizr.com/download/?-touchevents-setclasses !*/
! function(e, n, t) {
    function o(e, n) {
        return typeof e === n }

    function s() {
        var e, n, t, s, a, i, r;
        for (var l in c)
            if (c.hasOwnProperty(l)) {
                if (e = [], n = c[l], n.name && (e.push(n.name.toLowerCase()), n.options && n.options.aliases && n.options.aliases.length))
                    for (t = 0; t < n.options.aliases.length; t++) e.push(n.options.aliases[t].toLowerCase());
                for (s = o(n.fn, "function") ? n.fn() : n.fn, a = 0; a < e.length; a++) i = e[a], r = i.split("."), 1 === r.length ? Modernizr[r[0]] = s : (!Modernizr[r[0]] || Modernizr[r[0]] instanceof Boolean || (Modernizr[r[0]] = new Boolean(Modernizr[r[0]])), Modernizr[r[0]][r[1]] = s), f.push((s ? "" : "no-") + r.join("-")) } }

    function a(e) {
        var n = u.className,
            t = Modernizr._config.classPrefix || "";
        if (p && (n = n.baseVal), Modernizr._config.enableJSClass) {
            var o = new RegExp("(^|\\s)" + t + "no-js(\\s|$)");
            n = n.replace(o, "$1" + t + "js$2") }
        Modernizr._config.enableClasses && (n += " " + t + e.join(" " + t), p ? u.className.baseVal = n : u.className = n) }

    function i() {
        return "function" != typeof n.createElement ? n.createElement(arguments[0]) : p ? n.createElementNS.call(n, "http://www.w3.org/2000/svg", arguments[0]) : n.createElement.apply(n, arguments) }

    function r() {
        var e = n.body;
        return e || (e = i(p ? "svg" : "body"), e.fake = !0), e }

    function l(e, t, o, s) {
        var a, l, f, c, d = "modernizr",
            p = i("div"),
            h = r();
        if (parseInt(o, 10))
            for (; o--;) f = i("div"), f.id = s ? s[o] : d + (o + 1), p.appendChild(f);
        return a = i("style"), a.type = "text/css", a.id = "s" + d, (h.fake ? h : p).appendChild(a), h.appendChild(p), a.styleSheet ? a.styleSheet.cssText = e : a.appendChild(n.createTextNode(e)), p.id = d, h.fake && (h.style.background = "", h.style.overflow = "hidden", c = u.style.overflow, u.style.overflow = "hidden", u.appendChild(h)), l = t(p, e), h.fake ? (h.parentNode.removeChild(h), u.style.overflow = c, u.offsetHeight) : p.parentNode.removeChild(p), !!l }
    var f = [],
        c = [],
        d = { _version: "3.3.1", _config: { classPrefix: "", enableClasses: !0, enableJSClass: !0, usePrefixes: !0 }, _q: [], on: function(e, n) {
                var t = this;
                setTimeout(function() { n(t[e]) }, 0) }, addTest: function(e, n, t) { c.push({ name: e, fn: n, options: t }) }, addAsyncTest: function(e) { c.push({ name: null, fn: e }) } },
        Modernizr = function() {};
    Modernizr.prototype = d, Modernizr = new Modernizr;
    var u = n.documentElement,
        p = "svg" === u.nodeName.toLowerCase(),
        h = d._config.usePrefixes ? " -webkit- -moz- -o- -ms- ".split(" ") : ["", ""];
    d._prefixes = h;
    var m = d.testStyles = l;
    Modernizr.addTest("touchevents", function() {
        var t;
        if ("ontouchstart" in e || e.DocumentTouch && n instanceof DocumentTouch) t = !0;
        else {
            var o = ["@media (", h.join("touch-enabled),("), "heartz", ")", "{#modernizr{top:9px;position:absolute}}"].join("");
            m(o, function(e) { t = 9 === e.offsetTop }) }
        return t }), s(), a(f), delete d.addTest, delete d.addAsyncTest;
    for (var v = 0; v < Modernizr._q.length; v++) Modernizr._q[v]();
    e.Modernizr = Modernizr }(window, document);

if (Modernizr.touchevents) {
    $(".steps-block").on('touch', function() { //use a class, since your ID gets mangled
        $(this).toggleClass('active').siblings().removeClass('active');
    });
} else {
    $(".steps-block").on('click', function() { //use a class, since your ID gets mangled
        $(this).toggleClass('active').siblings().removeClass('active');
    });
}
