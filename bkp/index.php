<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Купите квартиру своей мечты!</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css">
	<meta name="viewport" content="width=1100">
	<meta name="description" content="Купите квартиру своей мечты!<">
</head>
<body>
<!-- <img src="whitewill.jpg" alt="" class="psd"> -->
<div class="container clearfix">
<div class="left">
	<img src="/img/kupitekvartiru-final-logo.svg" alt="">
	<span>лучшие жилые комплексы Москвы</span>
</div>
<div class="right">
	<h1>Купите квартиру своей мечты!</h1>
	<div class="images">
		<a href="#" class="bw">
			<img src="/img/logos/afi-0.png" alt="" class="black">
			<img src="/img/logos/afi-1.png" alt="" class="color">
		</a>

		<a href="#" class="bw">
			<img src="/img/logos/neva-0.png" alt="" class="black">
			<img src="/img/logos/neva-1.png" alt="" class="color">
		</a>

		<a href="#" class="bw">
			<img src="/img/logos/mone-0.png" alt="" class="black">
			<img src="/img/logos/mone-1.png" alt="" class="color">
		</a>

		<a href="#" class="bw">
			<img src="/img/logos/vtb-0.png" alt="" class="black">
			<img src="/img/logos/vtb-1.png" alt="" class="color">
		</a>

		<a href="#" class="bw">
			<img src="/img/logos/zilart-0.png" alt="" class="black">
			<img src="/img/logos/zilart-1.png" alt="" class="color">
		</a>

	<!-- 	<a href="http://red-side.ru/" class="bw">
			<img src="/img/logos/redside-0.png" alt="" class="black">
			<img src="/img/logos/redside-1.png" alt="" class="color">
		</a> -->

		<a href="#" class="bw">
			<img src="/img/logos/pol-0.png" alt="" class="black">
			<img src="/img/logos/pol-1.png" alt="" class="color">
		</a>

		<a href="#" class="bw">
			<img src="/img/logos/art-0.png" alt="" class="black">
			<img src="/img/logos/art-1.png" alt="" class="color">
		</a>


		<a href="#" class="bw">
			<img src="/img/logos/bark-0.png" alt="" class="black">
			<img src="/img/logos/bark-1.png" alt="" class="color">
		</a>

		<a href="#" class="bw">
			<img src="/img/logos/dyh-0.png" alt="" class="black">
			<img src="/img/logos/dyh-1.png" alt="" class="color">
		</a>

		<a href="#" class="bw">
			<img src="/img/logos/ak-0.png" alt="" class="black">
			<img src="/img/logos/ak-1.png" alt="" class="color">
		</a>


		<a href="#" class="bw">
			<img src="/img/logos/pekin-0.png" alt="" class="black">
			<img src="/img/logos/pekin-1.png" alt="" class="color">
		</a>


	<!-- 	<a href="#" class="bw">
			<img src="/img/logos/sk-0.png" alt="" class="black">
			<img src="/img/logos/sk-1.png" alt="" class="color">
		</a> -->

		<a href="#" class="bw">
			<img src="/img/logos/wh-0.png" alt="" class="black">
			<img src="/img/logos/wh-1.png" alt="" class="color">
		</a>

		<a href="#" class="bw">
			<img src="/img/logos/olymp-0.png" alt="" class="black">
			<img src="/img/logos/olymp-1.png" alt="" class="color">
		</a>







	</div>
	<div class="info">
		<ul>
			<li><span class="phone-logo"></span> <a href="tel:84950055324">8&nbsp;(495)&nbsp;005-53-24</a></li>
			<li>

			</li>
		</ul>
	</div>
</div>
</div>
<!-- <div class="logo"></div> -->
<!-- <h1> -->
	<!-- Эксперты по недвижимости -->

<!-- </h1> -->
</body>
</html>