<div style="width: 400px;margin:auto;margin-top: -10px;">
	<div style="font-size: 24px;">
		Заказать подбор
	</div>	
	<form action="#">
		<div style="margin-top: 15px;">Укажите ваши контактные данные и наш эксперт по недвижимости свяжется с вами.</div>
		<div class="row" style="margin-top: 15px;">
			<div class="col-md-4">Имя</div>
			<div class="col-md-6">
				<input class="form-control" type="text" required name="name" style="width: 130%;"></div>
		</div>
		<div class="row" style="margin-top: 15px;">
			<div class="col-md-4">Телефон</div>
			<div class="col-md-6">
				<input class="form-control mask" type="text" required name="phone" style="width: 130%;"></div>
		</div>
		<div class="row" style="margin-top: 15px;">
			<div class="col-md-4">Комментарий <br><small>(не обязательно)</small></div>
			<div class="col-md-6">
				<textarea class="form-control" required name="comment" style="width: 130%;resize: vertical;" rows="3"></textarea></div>
		</div>
		<input class="submit" style="display:none;" type="submit">
		<div class="gold-but" style="width:186px;height:40px;margin-left:35%;margin-top: 20px;margin-bottom: 50px;" data-where="заказал подбор">Отправить заявку</div>
	</form>
</div>
