<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "developer".
 *
 * @property integer $id
 * @property string $image
 * @property string $name
 * @property string $text
 */
class Developer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'developer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'name', 'text'], 'required'],
            [['text'], 'string'],
            [['image', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'name' => 'Name',
            'text' => 'Text',
        ];
    }
}
