<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "left_block".
 *
 * @property integer $id
 * @property string $image
 * @property string $func
 */
class LeftBlock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'left_block';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'func', 'type', 'h2', 'text'], 'required'],
            [['image', 'func', 'h2', 'text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'type' => 'Количество в линию',
            'func' => 'Func',
        ];
    }
}
