<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "under_type_obj".
 *
 * @property integer $id
 * @property string $name
 * @property integer $id_type
 */
class UnderTypeObj extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'under_type_obj';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['id_type'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'id_type' => 'Id Type',
        ];
    }
}
