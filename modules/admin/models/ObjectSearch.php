<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Object;
use app\modules\admin\models\RaionObj;

/**
 * ObjectSearch represents the model behind the search form about `app\modules\admin\models\Object`.
 */
class ObjectSearch extends Object
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'raiting', 'otdelka', 'price', 'metr'], 'integer'],
            [['raion', 'time_off', 'image'], 'safe'],
            [['name','raion'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Object::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'raiting' => $this->raiting,
            'otdelka' => $this->otdelka,
            'price' => $this->price,
            'metr' => $this->metr,
            'time_off' => $this->time_off,
            'name' => $this->name,
        ]);

        $query->andFilterWhere(['like', 'raion', function($data){
                    $th=RaionObj::find($data->raion)->one();
                    return $th->name;
                },]);

        return $dataProvider;
    }
}
