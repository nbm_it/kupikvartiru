<?php
namespace app\modules\admin\models;

use yii\base\Model;
use Yii;
use yii\web\UploadedFile;

class UpcoForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, svg'],
        ];
    }
    
    public function upload($dir)
    {
        if ($this->validate()) {
            $this->imageFile->saveAs(Yii::getAlias('@webroot').'/uploads/'.$dir.'/'. $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return $this->imageFile->baseName . '.' . $this->imageFile->extension;
        } else {
            return false;
        }
    }
       public function attributeLabels()
    {
        return [
            'imageFile' => 'Миниатюра для координат',
        ];
    }
}