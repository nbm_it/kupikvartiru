<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "sdacha".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_cal
 */
class Sdacha extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sdacha';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'name_cal'], 'required'],
            [['name', 'name_cal'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_cal' => 'Name Cal',
        ];
    }
}
