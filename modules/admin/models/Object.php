<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "object".
 *
 * @property integer $id
 * @property integer $raiting
 * @property string $raion
 * @property integer $otdelka
 * @property string $price
 * @property string $metr
 * @property integer $time_off
 * @property string $image
 * @property string $type_is
 * @property string $name
 * @property string $text
 */
class Object extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'object';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['raiting', 'raion', 'otdelka', 'price','price_m', 'metr', 'time_off', 'type_is','image','name','text','coords','url'], 'required','message'=>'Поле "{attribute}" обязательно для заполнения.'],
            [['raiting', 'otdelka', 'loft'], 'integer'],
            ['url', 'url', 'defaultScheme' => 'http','message'=>'Неправильный формат ссылки . Должно быть {http(s)://ваша_ссылка/} .'],
            // [['time_off'], 'safe'],
            [['price', 'metr','raion','type_is','un_type_is','image','coords','url','un_type_is'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'raiting' => 'Raiting',
            'raion' => 'Район',
            'otdelka' => 'Есть отделка',
            'price' => 'Цена за объект',
            'price_m' => 'Цена за метр',
            'metr' => 'Metr',
            'type_is' => 'Типы квартир',
            'un_type_is' => 'Подтипы квартир',
            'time_off' => 'Время сдачи',
            'image' => 'Изображение',
            'name' => 'Название',
            'text' => 'Текст',
            'coords' => 'Координаты',
            'url' => 'Ссылка',
            'loft' => 'Лофт',
        ];
    }
    public function beforeSave($insert)
    {
            //echo 13241;//exit;
        if (parent::beforeSave($insert)) {
     
            // if(isset())
            // $this->type_is = implode(",", $_POST['Object[type_is]']);
            return true;
        }
        return false;
    }
}
