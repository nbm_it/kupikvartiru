<?php

namespace app\modules\admin\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "carousel".
 *
 * @property integer $id
 * @property string $image
 * @property string $h2
 * @property string $text
 * @property string $func
 */
class Carousel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carousel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'h2', 'text', 'func'], 'required'],
            [['h2', 'text', 'func'], 'string'],            
            [['image'], 'image', 'extensions' => 'png, jpg, jpeg, gif', 'maxFiles' => 1,],
        ];
    }

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'image',
            'h2' => 'H2',
            'text' => 'Text',
            'func' => 'Func',
        ];
    }
}
