<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "bot_url".
 *
 * @property integer $id
 * @property integer $col
 * @property string $name
 * @property string $func
 */
class BotUrl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bot_url';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['col', 'name', 'func'], 'required'],
            [['col'], 'integer'],
            [['name', 'func'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'col' => 'Col',
            'name' => 'Name',
            'func' => 'Func',
        ];
    }
}
