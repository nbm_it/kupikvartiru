<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "video".
 *
 * @property integer $id
 * @property string $url
 */
class Video extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url','site','phone'], 'required'],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'ID ютуб',
            'site' => 'Ссылка на сайт',
            'phone' => 'Телефон',
        ];
    }
}
