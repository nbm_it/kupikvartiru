<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Object;
use app\modules\admin\models\ObjectSearch;
use app\modules\admin\models\TypeObj;
use app\modules\admin\models\UploadForm;
use app\modules\admin\models\UpcoForm;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;

/**
 * ObjectController implements the CRUD actions for Object model.
 */
class ObjectController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create','index', 'view', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','create', 'update', 'view'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }
    /**
     * Lists all Object models.
     * @return mixed
     */
    public function actionIndex()
    {
         $dataProvider = new ActiveDataProvider([ 
            'query' => Object::find(), 
        ]); 

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Object model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Object model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Object();
        $uploadModel = new UploadForm();
        $uploadModelC = new UpcoForm();

        if ($model->load(Yii::$app->request->post())) {
            
            $uploadModel->imageFile = UploadedFile::getInstance($uploadModel, 'imageFile');
            $uploadModelC->imageFile = UploadedFile::getInstance($uploadModelC, 'imageFile');

             if($uploadModel->imageFile!=''){
                $model->image = $uploadModel->upload("object");
            }else{
                $model->image = "nophoto.jpg";
            }

            if($uploadModelC->imageFile!=''){
                $model->coimage = $uploadModelC->upload("copic");
            }else{
                $model->coimage = "nophoto.jpg";
            }

            $model->type_is = implode(",", $model->type_is);
            if($model->un_type_is)
            $model->un_type_is = implode(",", $model->un_type_is);

            if( $model->save() )
                    return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'uploadModel'=>$uploadModel,
                'uploadModelC'=>$uploadModelC,
            ]);
        }
    }

    /**
     * Updates an existing Object model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $uploadModel = new UploadForm();
        $uploadModelC = new UpcoForm();

        $model->type_is = explode(",", $model->type_is);
        if($model->un_type_is)
        $model->un_type_is = explode(",", $model->un_type_is);

        if ($model->load(Yii::$app->request->post())) {
            $uploadModel->imageFile = UploadedFile::getInstance($uploadModel, 'imageFile');

            $uploadModelC->imageFile = UploadedFile::getInstance($uploadModelC, 'imageFile');

            if($uploadModel->imageFile!=''){
                $model->image = $uploadModel->upload("object");
            }

            if($uploadModelC->imageFile!=''){
                $model->coimage = $uploadModelC->upload("copic");
            }
            
            $model->type_is = implode(",", $model->type_is);
            if($model->un_type_is)
                $model->un_type_is = implode(",", $model->un_type_is);

            // echo "<pre>";print_r($model);exit;
             if( $model->save() )//echo 1; exit;
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'uploadModel'=>$uploadModel,
                'uploadModelC'=>$uploadModelC,
            ]);
        }
    }

    /**
     * Deletes an existing Object model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Object model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Object the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Object::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
