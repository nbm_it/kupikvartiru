<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Carousel;
use app\modules\admin\models\UploadForm;
use app\modules\admin\models\CarouselSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * CarouselController implements the CRUD actions for Carousel model.
 */
class CarouselController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create','index', 'view', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','create', 'update', 'view'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Carousel models.
     * @return mixed 
     */
    public function actionIndex()
    {
        $searchModel = new CarouselSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Carousel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Carousel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Carousel();
        $uploadModel = new UploadForm();

        if ($model->load(Yii::$app->request->post())) {
            
            $uploadModel->imageFile = UploadedFile::getInstance($uploadModel, 'imageFile');
            $model->image = $uploadModel->upload("carousel");
            
            if($model->save()){
                return $this->redirect(['view', 'id'=>$model->id]);
            } else {
            }
        }
        return $this->render('create', [
            'model'=>$model,
            'uploadModel'=>$uploadModel
        ]);
    }

    /**
     * Updates an existing Carousel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $uploadModel = new UploadForm();

        if ($model->load(Yii::$app->request->post())) {
            $uploadModel->imageFile = UploadedFile::getInstance($uploadModel, 'imageFile');
            if($uploadModel->imageFile!=''){
                $model->image = $uploadModel->upload("carousel");
            }
            if($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $uploadModel['imageFile']=$model['image'];
            return $this->render('update', [
                'model' => $model,
                'uploadModel'=>$uploadModel
            ]);
        }
    }

    /**
     * Deletes an existing Carousel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Carousel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Carousel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Carousel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
