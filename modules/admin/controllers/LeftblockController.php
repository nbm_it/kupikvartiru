<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\LeftBlock;
use app\modules\admin\models\LeftBlockSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\models\UploadForm;
use yii\web\UploadedFile;

/**
 * LeftblockController implements the CRUD actions for LeftBlock model.
 */
class LeftblockController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LeftBlock models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LeftBlockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LeftBlock model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LeftBlock model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LeftBlock();
        $uploadModel = new UploadForm();
        
        if ($model->load(Yii::$app->request->post())) {
            
            $uploadModel->imageFile = UploadedFile::getInstance($uploadModel, 'imageFile');

            if($uploadModel->imageFile!=''){
                $model->image = $uploadModel->upload("banner");
            }else{
                $model->image = "nophoto.jpg";
            }

            if ($model->save()) 
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'uploadModel'=>$uploadModel,
            ]);
        }
    }

    /**
     * Updates an existing LeftBlock model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $uploadModel = new UploadForm();

        if ($model->load(Yii::$app->request->post())) {
            
            $uploadModel->imageFile = UploadedFile::getInstance($uploadModel, 'imageFile');

             if($uploadModel->imageFile!=''){
                $model->image = $uploadModel->upload("banner");
            }

            if ($model->save()) 
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'uploadModel'=>$uploadModel,
            ]);
        }
    }

    /**
     * Deletes an existing LeftBlock model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LeftBlock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LeftBlock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LeftBlock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
