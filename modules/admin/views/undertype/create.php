<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\UnderTypeObj */

$this->title = 'Create Under Type Obj';
$this->params['breadcrumbs'][] = ['label' => 'Under Type Objs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="under-type-obj-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
