<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UnderTypeObjSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Under Type Objs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="under-type-obj-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Under Type Obj', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'name',
            'id_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
