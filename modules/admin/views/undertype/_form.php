<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\modules\admin\models\TypeObj;
// $type=new TypeObj;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\UnderTypeObj */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="under-type-obj-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php /*$form->field($model, 'id_type')
    ->dropDownList(ArrayHelper::map(TypeObj::find()->all(), 'id', 'name'), ['unselect' => null]);*/?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
