<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\slider\Slider; 
use app\assets\AppAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\Modal;

use app\modules\admin\models\TypeObj;
use app\modules\admin\models\RaionObj;
use app\modules\admin\models\Sdacha;
use app\modules\admin\models\UnderTypeObj;
$type=new TypeObj;
$raion=new RaionObj;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Object */
/* @var $form yii\widgets\ActiveForm */


?>

<style>
    .tooltip{display: none;}
    .form-control{display: block !important;}
    .sl .slider.slider-horizontal {padding: 20px;margin-bottom: 10px; background: #eee;border: 1px #e2e2e2 solid;border-radius: 2px;}
</style>

<div class="object-form">

    <!-- <?php /*Modal::begin([
         'header' => '<b>' . Yii::t('app', 'Create new project') . '</b>',
         'toggleButton' => ['label' => '+'],
    ]);
    echo $this->render('/typeobj/create', ['model' => $type ]);
    Modal::end();*/?> -->


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'url')->textInput() ?>

    <?= $form->field($model, 'coords')->textInput()->hint('[55.817522, 37.573178]') ?>

    <?php if(!$model->coimage){
        echo $form->field($uploadModelC, 'imageFile')->fileInput() ;
    }else{
        echo Html::img(Yii::getAlias('@web').'/uploads/copic/'.$model->coimage,['width'=>'40px']);
        echo $form->field($uploadModelC, 'imageFile')->fileInput() ;
    }
    ?>

    <?= $form->field($model, 'text')->textarea() ?>

    <?php if(!$model->image){
        echo $form->field($uploadModel, 'imageFile')->fileInput() ;
    }else{
        echo Html::img(Yii::getAlias('@web').'/uploads/object/'.$model->image,['width'=>'20%']);
        echo $form->field($uploadModel, 'imageFile')->fileInput() ;
    }
    ?>

       
    
    <?= $form->field($model, 'raiting')->dropDownList([
        '5' => '5',
        '4' => '4',
        '3'=>'3',
        '2'=>'2',
        '1'=>'1',
    ])->hint('Число от 1 до 5') ;?>


	<?= $form->field($model, 'type_is')
    ->checkboxList(ArrayHelper::map(TypeObj::find()->all(), 'id', 'name'), ['unselect' => null]);?>
    <div class="form-group">
        <label class="control-label" for="object-typeobj"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/typeobj/create">Добавить новый тип квартир</a></label>
    </div>

    <?= $form->field($model, 'un_type_is')->checkboxList(ArrayHelper::map(UnderTypeObj::find()->all(), 'id', 'name') ); ?>

    <div class="form-group">
        <label class="control-label" for="object-undertype"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/undertype/create">Добавить новый подтип квартир</a></label>
    </div>

    <?= $form->field($model, 'raion')->dropDownList(ArrayHelper::map(RaionObj::find()->all(), 'id', 'name') ); ?>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/raion/create">Добавить новый район</a></label>
    </div>
    <?= $form->field($model, 'otdelka')->checkbox() ?>
    <?= $form->field($model, 'loft')->checkbox() ?>

    
<!--     <div class="form-group field-object-raion required">
        <label class="control-label" for="object-raion">Цена</label>
        <div class="sl">
            <?=Slider::widget([
                'name'=>'Object[price]',
                'value'=>'100000,100000000',
                'sliderColor'=>Slider::TYPE_PRIMARY,
                'handleColor'=>Slider::TYPE_PRIMARY,
                'pluginOptions'=>[
                    'min'=>100000,
                    'max'=>100000000,
                    'step'=>5,
                    'range'=>true,
                    'tooltip'=>'always',
                    'tooltip_split'=>'true',
                ],
            ]) ?>
        </div>
        <div class="help-block"></div>
    </div> -->
    <?= $form->field($model, 'price')->textInput() ?>
    <?= $form->field($model, 'price_m')->textInput() ?>
<!--     <div class="form-group field-object-raion required">
        <label class="control-label" for="object-raion">Метры</label>
        <div class="sl">
            <?=Slider::widget([
                'name'=>'Object[metr]',
                'value'=>'100000,100000000',
                'sliderColor'=>Slider::TYPE_PRIMARY,
                'handleColor'=>Slider::TYPE_PRIMARY,
                'pluginOptions'=>[
                    'min'=>100000,
                    'max'=>100000000,
                    'step'=>5,
                    'range'=>true,
                    'tooltip'=>'always',
                    'tooltip_split'=>'true',
                ],
            ]) ?>
        </div>
        <div class="help-block"></div>
    </div> -->
    <?= $form->field($model, 'metr')->textInput() ?>

    <?= $form->field($model, 'time_off')->dropDownList(ArrayHelper::map(Sdacha::find()->all(), 'id', 'name') ); ?>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/sdacha/create">Добавить новое время сдачи</a></label>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
