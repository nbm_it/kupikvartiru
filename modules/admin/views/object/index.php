<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\admin\models\RaionObj;
    use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ObjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Objects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="object-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Object', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/typeobj/create">Добавить новый тип квартир</a></label>
    </div>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" style="color: #8faac1;" href="<?=Yii::getAlias('@web')?>/admin/undertype/create">Добавить новый подтип</a></label>
    </div>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/raion/create">Добавить новый район</a></label>
    </div>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/sdacha/create">Добавить новое время сдачи</a></label>
    </div>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
	    // 'rowOptions' => function ($model, $key, $index, $grid)
	    // {
	    //   // if($model->raiting == 5) {
	    //   //     return ['style' => 'background-color:#ffe1ad;'];
	    //   // }elseif($model->raiting == 3) {
	    //   //     return ['style' => 'background-color:#e2e1e1;'];
	    //   // }
	    // },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'un_type_is',
            [
                'label' => 'Район',
                'value' => function($data){
                	$th=RaionObj::findOne($data->raion);
                    return $th->name;
                },
            ],
            'otdelka:boolean',
            'price',
            // 'metr',
            // 'time_off',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
    Pjax::end();
    ?>
</div>
