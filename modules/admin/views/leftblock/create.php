<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\LeftBlock */

$this->title = 'Create Left Block';
$this->params['breadcrumbs'][] = ['label' => 'Left Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="left-block-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'uploadModel'=>$uploadModel,
    ]) ?>

</div>
