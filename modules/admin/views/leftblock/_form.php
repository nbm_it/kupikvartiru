<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\LeftBlock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="left-block-form">

    <?php $form = ActiveForm::begin(); ?>


    <?php if(!$model->image){
        echo $form->field($uploadModel, 'imageFile')->fileInput()->hint('670x375 для на 1 ячейку,<br> 335x375 для на 2 ячейку ') ;
    }else{
        echo Html::img(Yii::getAlias('@web').'/uploads/banner/'.$model->image,['width'=>'20%']);
        echo $form->field($uploadModel, 'imageFile')->fileInput()->hint('670x375 для на 1 ячейку,<br> 335x375 для на 2 ячейку ') ;
    }
    ?>
    <?= $form->field($model, 'h2')->textInput(['maxlength' => true])?>
    <?= $form->field($model, 'text')->textarea(['rows' => 6])?>

    <?= $form->field($model, 'func')->textInput(['maxlength' => true])->hint('{"otdelka":"yes","type":[1,2,3,4],"price":[200,10000000],"area":[200,1000],"time":2017}<br>{"Есть ли отделка":"yes","Тип квартир":[здесь перечисляются типы квартир по очереди],"Ценовой диапазон":[200,1000]},"Площадь":[200,1000],"Время сдачи":год}') ?>
<!-- [год,квартал] -->
	<?= $form->field($model, 'type')->dropDownList([
	        '2'=>'2 Ячейки',
	        '1'=>'1 Ячейка',
	    ])->hint(' ') ;?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
   <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/typeobj/index">тип квартир</a></label>
    </div>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" style="color: #8faac1;" href="<?=Yii::getAlias('@web')?>/admin/undertype/index">подтип</a></label>
    </div>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/raion/index">район</a></label>
    </div>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/sdacha/index">время сдачи</a></label>
    </div>