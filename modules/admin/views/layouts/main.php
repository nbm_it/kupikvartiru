<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" style="height: 100%;">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body style="height: 100%;">
<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Admin panel',
        'brandUrl' => Yii::$app->homeUrl."admin/",
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Слайдер', 'url' => ['/admin/carousel/index']],
            ['label' => 'Объект', 'url' => ['/admin/object/index']],
            ['label' => 'Эксперты', 'url' => ['/admin/expert/index']],
            ['label' => 'Девелоперы', 'url' => ['/admin/developer/index']],
            ['label' => 'Видео', 'url' => ['/admin/video/index']],
            ['label' => 'Банеры(л)', 'url' => ['/admin/leftblock/index']],
            ['label' => 'Ссылки футер', 'url' => ['/admin/boturl/index']],
            /*['label' => 'Типы квартир', 'url' => ['/admin/typeobj/index']],
            ['label' => 'Районы', 'url' => ['/admin/raion/index']],*/
            Yii::$app->user->isGuest ? (
                ['label' => 'ADMIN PANEL', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container" style="padding-top: 100px;">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>


        <?= $content ?>
    </div>
</div>

<footer class="footer" style="">
    <div class="container">
    <a href="<?=Yii::getAlias('@web')?>">На сайт</a>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
