<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Developer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="developer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(!$model->image){
     echo $form->field($uploadModel, 'imageFile')->fileInput() ;
    }else{
        echo Html::img(Yii::getAlias('@web').'/uploads/developers/'.$model->image,['width'=>'20%']);
        echo $form->field($uploadModel, 'imageFile')->fileInput() ;
    }
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
