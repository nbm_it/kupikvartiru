<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\BotUrl */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bot-url-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'col')->dropDownList([
	        '4'=>'4 Колонка',
	        '3'=>'3 Колонка',
	        '2'=>'2 Колонка',
	        '1'=>'1 Колонка',
	    ])->hint(' ') ;?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'func')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
   <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/typeobj/index">тип квартир</a></label>
    </div>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" style="color: #8faac1;" href="<?=Yii::getAlias('@web')?>/admin/undertype/index">подтип</a></label>
    </div>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/raion/index">район</a></label>
    </div>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/sdacha/index">время сдачи</a></label>
    </div>