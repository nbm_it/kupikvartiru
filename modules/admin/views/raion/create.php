<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\RaionObj */

$this->title = 'Create Raion Obj';
$this->params['breadcrumbs'][] = ['label' => 'Raion Objs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="raion-obj-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
