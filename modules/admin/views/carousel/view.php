<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Carousel */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Carousels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carousel-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <img style="width: 20%;" src="<?=Yii::getAlias('@web').'/uploads/'.$model->image;?>"
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // [
            //     'label' => 'Картинка',
            //     'format' => 'raw',
            //     'value' => function($data){
            //         return Html::img( '../../uploads/'.$data->image,[
            //             'alt'=>'yii2 - картинка в gridview',
            //             'style' => 'width:60px;'
            //         ]);
            //     },
            // ],
            'h2:ntext',
            'text:ntext',
        ],
    ]) ?>

</div>
