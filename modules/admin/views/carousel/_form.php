<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
// use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Carousel */
/*<?= $form->field($uploadModel, 'imageFile')->fileInput() ?> @var $form yii\widgets\ActiveForm */
?>

<div class="carousel-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(!$model->image){
     echo $form->field($uploadModel, 'imageFile')->fileInput() ;
    }else{
        echo Html::img(Yii::getAlias('@web').'/uploads/carousel/'.$model->image,['width'=>'20%']);
        echo $form->field($uploadModel, 'imageFile')->fileInput() ;
    }
    ?>
    <?= $form->field($model, 'func')->textInput(['rows' => 6]) ?>
    <?= $form->field($model, 'h2')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

   <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/typeobj/index">тип квартир</a></label>
    </div>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" style="color: #8faac1;" href="<?=Yii::getAlias('@web')?>/admin/undertype/index">подтип</a></label>
    </div>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/raion/index">район</a></label>
    </div>
    <div class="form-group">
        <label class="control-label" for="object-raion"><a target="_blank" href="<?=Yii::getAlias('@web')?>/admin/sdacha/index">время сдачи</a></label>
    </div>