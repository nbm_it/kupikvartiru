<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\SdachaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sdachas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sdacha-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sdacha', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            
            'id',
            'name',
            'name_cal',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
