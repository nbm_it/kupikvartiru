<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;
// use app\modules\admin\models\Pages;

AppAsset::register($this);
$this->registerJsFile(Yii::getAlias('@web').'/js/main/swiper.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::getAlias('@web').'/js/form/send.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::getAlias('@web').'/js/plugin/mask.phone.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::getAlias('@web').'/js/main/index.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta name="Description" content="">
    <meta name="Keywords" content="">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.min.js"></script>

    <?= Html::csrfMetaTags() ?>
    <title>Купи квартиру.com</title>
    <?php $this->head() ?>
</head>
<body>

<input type="text" style="display: none;
    position: relative;
    z-index: 9999999999;" class="mask">
<!-- <?= Html::encode($this->title) ?> -->
<?php $this->beginBody() ?>
<?php 
    // $pages=Pages::find()->all();
    // $menu='';
    // foreach ($pages as $key) {
    //     $menu.="<li><a href='".$key->url."'>".$key->name."</a></li>";
    // }
?>
<header class="header" style="background: url(<?=Yii::getAlias('@web').'/img/hat.png'?>);background-position: 47% 0px;/*margin-left:15%;max-width: 1510px;background-size: cover;*/">
    <div class="container">
        <img class="logo" style="float: left;" src="<?=Yii::getAlias('@web').'/img/logo.png'?>">

        <div style="float: right;">
            <a class="phone" href="tel:84953630911">8(495)005-53-24</a>
            <a class="call_me cp phone" data-target="form.php">8(495)005-53-24</a>
            <span class="call_me" data-target="form.php">Заказать звонок</span>
        </div>
    </div>
</header>

<div class="wrap">    
	<div class="container">
		<div class="site-index">
        	<?= $content ?>

<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
    <script type="text/javascript">
 

        ymaps.ready(init);
        var myMap, 
            myPlacemark;

        function init(){ 
            myMap = new ymaps.Map ("map", {
                center: [55.76, 37.64],
                zoom: 12
            }); 
            
            // myPlacemark = new ymaps.Placemark([55.76, 37.64], {
            //     hintContent: 'Москва!',
            //     balloonContent: 'Столица России',

            //                 iconImageHref: "uploads/copic/"+coim,
            // });

            myCollection = new ymaps.GeoObjectCollection();

            myMap.controls.remove('fullscreenControl');
            myMap.controls.remove('typeSelector');
            myMap.controls.remove('trafficControl');
            myMap.controls.remove('geolocationControl');
            myMap.controls.remove('searchControl');
            myMap.controls.add('zoomControl');
            
            myMap.geoObjects.add(myPlacemark);
            // run()
        }

        
    </script>
<?php $this->endBody() ?>
<script>
    

    // window.page=1;
    // $(".l"+(window.page)).addClass("active")

    // $(".page-link").click(function(event) {
    //     $(".l"+(window.page)).removeClass("active")
    //     if( $(this).data("page")==1 ){
    //         window.page=1;
    //         run()
    //         $( "div.list-group-item.oi:visible" ).slice(4, 200).hide();
            
    //     }else if( $(this).data("page")=="pr"){
    //         $(".l"+(window.page-1)+":visible").click()
    //     }else if( $(this).data("page")=="ne"){
    //         $(".l"+(window.page+1)+":visible").click()
    //     }else{
    //         window.page=$(this).data("page");
    //         run()
    //         $( "div.list-group-item.oi:visible" ).slice(0, 4).hide();
    //         $( "div.list-group-item.oi:visible" ).slice( (window.page*4) , 200).hide();
    //     }
    //     var destination = $(".main").offset().top;

    //     $(".l"+(window.page)).addClass("active")
    //     $('body').animate({ scrollTop: destination }, 600);
    //     $( ".nh").show();
    // })
</script>



</body>
</html>
<?php $this->endPage() ?>
