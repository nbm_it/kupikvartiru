<?php
use kartik\slider\Slider; 
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Carousel;
use yii\bootstrap\Modal;

$plural = function($number, $one, $two, $five) {
    if (($number - $number % 10) % 100 != 10) {
        if ($number % 10 == 1) {
            $result = $one;
        } elseif ($number % 10 >= 2 && $number % 10 <= 4) {
            $result = $two;
        } else {
            $result = $five;
        }
    } else {
        $result = $five;
    }
    return $result;
};

$this->title = 'My Yii Application';
foreach ($model as $key => $value) {
    $items[$key]=[
                'content' => '<div style="width:100%;height:571px; background: url(uploads/carousel/'.$value['image'].') no-repeat; background-position: top center;    background-size: 100% auto; " src=""></div>',
                'caption' => '<h2>'.$value['h2'].'</h2><p>'.$value['text'].'</p><div class="gold-but func nomob" style="font-size:18px;" data-target=\''.$value['func'].'\'>Посмотреть предложения</div>',
                'options' => []
            ];
}
?>
<div class="head-carousel">
    <?=Carousel::widget ( [
        'items' => $items,
        'options' => ['style' => 'width:100%;', 'class' => 'carousel slide', 'data-interval' => '12000'],
        'controls' => [
         '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
         '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'
        ]
    ]);?>
</div>

    

    <div style="clear:both;position: relative;" class="body-content">
<?php 

// main carousel

// video
$videos='';
$i=0;$r=0;
foreach ($video as $key => $value) {
        $videos.='<div class="swiper-slide magick"  data-site="'.$value['site'].'"  data-phone="'.str_replace('+','&nbsp;', str_replace(' ','&nbsp;',$value['phone']) ).'"  data-target="'.$value['url'].'?autoplay=1"><img  class="resp-preview"  data-video="'.$value['url'].'"  data-site="'.$value['site'].'"  data-phone="'.$value['phone'].'" src="//img.youtube.com/vi/'.$value['url'].'/mqdefault.jpg" data-toggle="modal" data-target="'.$i.'" /></div>';
        $i++;
}
$vi_con=$i;
?>
        

        <div class="row main">
            <h1>Выберите квартиру или апартаменты<br> в одном из лучших жилых комплексов Москвы</h1>
            <h2 class="h2"></h2>
<!-- Calculator -->
            <div class="col-md-4">

                <div class="list-group calc-it">
                    <div class="list-group-item">
                        <b>Отображать </b>
                        <div class="btn-group" data-toggle="buttons">
                          <label class="btn btn-default active" data-toggle="tab" data-target="#list" >
                            <input type="radio" name="options" checked id="option1" autocomplete="off" checked>
                            списком
                          </label>
                          <label class="btn btn-default run" data-toggle="tab" data-target="#map">
                            <input type="radio" name="options" id="option2" autocomplete="off">
                            на карте
                          </label>
                        </div> 
                    </div>

                    <div class="list-group-item type" style="height: 90px;">
                        <?php foreach ($type as $key => $value) { if($value['id']!=4){ ?>
                        <div class="checkbox">
                          <label><input type="checkbox" checked class="type<?=$value['id']?>"  value=""><?=$value['name']?> <span>(<?=$config['calcType'][$value['id']]?>)</span></label>
                        </div>
                        

                         <?php }}?>
                        <br>
                    </div>
                    <div class="list-group-item otdelka">
                        <div class="checkbox" style=" ">
                          <label><input type="checkbox" checked class="no" value="">без отделки <span>(<?=$config['calcOtd'][0]?>)</span></label>
                        </div>
                        <div class="checkbox" style="">
                          <label><input type="checkbox" checked class="yes" value="">с отделкой <span>(<?=$config['calcOtd'][1]?>)</span></label>
                        </div> 
                        <div class="checkbox" >
                          <label><input type="checkbox" checked  class="loft" value="">лофт <span>(<?=$config['calcLoft']?>)</span></label>
                        </div>
                    </div>

                    <div class="list-group-item untype" style="height: 90px;">
                        <?php foreach ($untype as $key => $value) { ?>
                        <div class="checkbox">
                          <label><input type="checkbox" class="untype<?=$value['id']?>"  value=""><?=$value['name']?> <span>(<?=$config['calcUnType'][$value['id']]?>)</span></label>
                        </div>
                        

                         <?php }?>
                        <br>
                    </div>
                    <div class="list-group-item tab-content">
                        <div>
                            <b style=" position: relative;top: 2px; ">Цена</b> 
                            <div class="btn-group priceC" data-toggle="buttons">
                              <label class="btn btn-default active" data-toggle="tab" data-target="#obj_price" >
                                <input type="radio" name="options" id="option1" class="obj" autocomplete="off" checked>
                                за объект
                              </label>
                              <label class="btn btn-default cost1" data-toggle="tab" data-target="#m2" >
                                <input type="radio" name="options" id="option2" class="m2" autocomplete="off">
                                за м<sup>2</sup>
                              </label>
                            </div> <!-- -->
                        </div>
                        <div class="slider-c pricel tab-pane active" id="obj_price">
                            <?=Slider::widget([
                                'name'=>'rating_3',
                                'value'=>''.$config['min'].','.$config['max'].'',
                                'sliderColor'=>Slider::TYPE_GREY,
                                'handleColor'=>Slider::TYPE_GOLD,
                                'pluginOptions'=>[
                                    'min'=>$config['min']*1,
                                    'max'=>$config['max']*1,
                                    'step'=>10000,
                                    'range'=>true,
                                    'tooltip'=>'always',
                                    'tooltip_split'=>'true',
                                ],
                            ]) ?>
                        </div>
                        <div class="slider-c pricelm tab-pane" id="m2">
                            <?=Slider::widget([
                                'name'=>'rating_9',
                                'value'=>''.$config['price_min'].','.$config['price_max'].'',
                                'sliderColor'=>Slider::TYPE_GREY,
                                'handleColor'=>Slider::TYPE_GOLD,
                                'pluginOptions'=>[
                                    'min'=>$config['price_min']*1,
                                    'max'=>$config['price_max']*1,
                                    'step'=>1000,
                                    'range'=>true,
                                    'tooltip'=>'always',
                                    'tooltip_split'=>'true',
                                ],
                            ]) ?>
                        </div>
                    </div>

                    <div class="list-group-item">
                        <b>Площадь</b>
                        <div class="slider-c metrl">
                            <!-- <b class="badge">10 м<sup>2</sup></b> -->
                            <?=Slider::widget([
                                'name'=>'rating_4',
                                'value'=>''.$config['metr_min'].','.$config['metr_max'].'',
                                'sliderColor'=>Slider::TYPE_GREY,
                                'handleColor'=>Slider::TYPE_GOLD,
                                'pluginOptions'=>[
                                    'min'=>$config['metr_min']*1,
                                    'max'=>$config['metr_max']*1,
                                    'step'=>1,
                                    'range'=>true,
                                    'tooltip'=>'always',
                                    'tooltip_split'=>'true',
                                ],
                            ]) ?>
                        </div>
                    </div>

                    <div class="list-group-item sdacha-cal" onclick="">
                        <b class="slup">Сдача объекта<i></i></b>
                        <div class="sdsl">
                            <div class="checkbox">
                              <label><input type="checkbox" checked class="all" value="">Все <!-- <span>()</span> --><div class="vrotebal"></div></label>
                            </div>
                            <?php foreach ($sdacha as $key => $value) {?>
                            <div class="checkbox sdcl"> 
                              <label><input type="checkbox" class="sdacha<?=$value['name_cal']?>" value="<?=$value['name_cal']?>" checked><?=$value['name_cal']?>
                              <div class="vrotebal"></div> <span>(<?=$config['calcSda'][$value['name_cal']]?>)</span></label>
                            </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="list-group-item raion-cal">
                        <b class="slup">Район Москвы<i></i></b>
                        <div class="sdsl">
                            <div class="checkbox">
                              <label><input type="checkbox" checked class="all" value="">Все <!-- <span>()</span> --><div class="vrotebal"></div></label>
                            </div>
                            <?php foreach ($raion as $key => $value) {?>
                                <div class="checkbox sdcl">
                                  <label><input type="checkbox" checked class="raion<?=$value['id']?>" value="<?=$value['name'];?>"><div class="vrotebal"></div><?=$value['name'];?> <span>(<?=$config['calcRai'][$value['id']]?>)</span></label>
                                </div>
                            <?php }?>
                        </div>
                    </div>

                </div>
                <div style="width: 254px; margin: auto;" class="hidden-sm hidden-xs">
                    <b style="font-size: 21px;">Не хотите искать сами?</b>
                    <div class="gold-but zakazaka" style="margin: 0;     width: 250px;height: 47px;font-size: 20px;    margin-bottom: 110px;" data-target="form2.php">Заказать подбор</div>
                </div> 
            </div>

<!-- Objects -->
            <div class="col-md-8">
                <span class="search_num">Найдено <span class="num_all2"><?=($config['pages']+4)?></span> <span class="word_all"><?=$plural(($config['pages']+4), "вариант", "варианта", "вариантов")?></span> </span>
                <em class="change" style="display: none; margin: auto; width: inherit; font-size: large;">Измените, пожалуйста, параметры поиска</em>
                <div class="tab-content list_map">
                    <div class="list-group tab-pane active" id="list">

                    <?php foreach ($object as $key => $value) {?>
       
                        <?php 
                            $price=explode(",",$value['price']);
                            $priceM=explode(",",$value['price_m']);
                            $metr=explode(",",$value['metr']);
                            $loft=$value['loft'];
                            foreach ($sdacha as $val) {
                                if($val['id']==$value['time_off']){
                                    $sdTi= $val['name'];
                                    $sdata= $val['name_cal'];
                                }
                            } 
                            ?>
                        <div class="list-group-item oi" data-coimage="<?=$value['coimage']?>" data-timeoff="<?=$sdata?>" data-pricef="<?=$price[0]?>" data-pricet="<?=$price[1]?>" data-pricemf="<?=$priceM[0]?>" data-pricemt="<?=$priceM[1]?>" data-raiting="<?=$value['raiting']?>" data-otdelka="<?=$value['otdelka']?>" data-coords="<?=$value['coords']?>" data-type="<?=$value['type_is']?>" data-untype="<?=$value['un_type_is']?>" data-raion="<?=$value['raion']?>" data-metrf="<?=$metr[0]?>" data-metrt="<?=$metr[1]?>" data-name="<?=$value['name']?>"  data-loft="<?=$value['loft']?>" >
                            <div class="row obj_block">
                                <div class="col-md-5 col-sm-5">
                                    <a href="<?=$value['url']?>" target="_blank" >
                                        <img src="uploads/object/<?=urlencode($value['image'])?>" class="obj_img">
                                    </a>
                                </div>
<!-- Звезды рейтинг (b - актив, i - нет) -->
                                <div class="col-md-7 col-sm-7 this_to_map">
                                        <a href="<?=$value['url']?>" target="_blank" class="obj_name"><?=$value['name']?></a>
                                        <div class="stary">
                                            <div class="stars" data-toggle="tooltip" title="Внутренний рейтинг портала">

                                                <?php for($i=0;$i<5-$value['raiting'];$i++){ ?> 
                                                <i></i>
                                                <?php } ?>
                                                <?php for($i=0;$i<$value['raiting'];$i++){ ?> 
                                                <b></b>
                                                <?php } ?>

                                                <!-- <b></b><i></i><i></i><i></i> -->
                                            </div>
                                            <div class="obj_sdacha">
                                             <?=$sdTi?>
                                            </div>
                                        </div>
                                        <div class="obj_i_block">
                                            <div class="obj_opis"><?=$value['text']?>
                                            </div>
                                                <div class="obj_calci">
                                                    <div class="obj_price">
                                                    <span class="from"><?=number_format($priceM[0], 0, ',', ' ');?></span> - 
                                                    <span class="to"><?=number_format($priceM[1], 0, ',', ' ');?></span> за м<sup>2</sup>
                                                    </div>
                                                    <div class="row ob">
                                                        <?= $loft?"<div class='col-md-3 col-sm-3 col-xs-6'>Лофт</div>":"" ?>
<?php //-----------------------------------type:objectView
                                                        $type_is=explode(",",$value['type_is']);
                                                        foreach ($type_is as $v) {
                                                            foreach ($type as $val) {
                                                                if($val['id']==$v){
                                                                    echo "<div class='col-md-3 col-sm-3 col-xs-6'>".$val['name']."</div>";
                                                                }
                                                            }
                                                            }
                                                        ?>
                                                    </div>
                                                <!-- •  -->
                                                    <!--<?=$value['otdelka']?'с отделкой':'Без отделки'?>-->
                                                    <div>
                                                        <div class="row obj_bot" style="margin-top: 15px;">
                                                            <div class="col-md-5 col-xs-5 obj_raion">
<?php //-----------------------------------raion:objectView
                                                                foreach ($raion as $val) {
                                                                    if($val['id']==$value['raion'])
                                                                    echo $val['name'];
                                                                } 
                                                                ?>
                                                            </div>
                                                            <div class="col-md-7 col-xs-7">
                                                                <div style="display: flex;"><a href="<?=$value['url']?>" target="_blank" style="margin-top: 0;" class="gold-but">
                                                                Узнать подробнее
                                                                </a></div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>

                                        
                                    </div>

                            </div>
                        </div>
                    <?php }?>
                    </div>
                    <div  class="tab-pane" id="map">
                    </div>
                </div>
                <div style="font-size: 22px; height: 50px;">
                    <div class="href showObj" style="">Показать еще <span class="num_all"><?=$config['pages']?></span></div>
                    <div class="href hideObj" style="display: none;    height: 60px;">Скрыть</div>
                </div>    
            </div>

                <div style="width: 254px; margin: auto;" class="hidden-md hidden-lg apple-fix">
                    <b style="font-size: 21px;">Не хотите искать сами?</b>
                    <div class="gold-but zakazaka" style="margin: 0; width: 250px;height: 47px;font-size: 20px; margin-bottom: 110px;" data-target="form2.php">Заказать подбор</div>
                </div> 
        </div>
        <div class="row">
<!-- Advert block -->
            <div class="col-md-8">
                <div class="row"  style="margin-bottom: 5px;position: relative;">   
                    <div class="col-md-12">
                        <div class="mtor">
                            <div class="torbosov" style="cursor: pointer; height: 400px;width: 93.8%;  background: url('/img/img_click.jpg');background-size: cover; position: absolute;" >
                                <div class="upblock1">
                                    <div style="font-size: 55px;" class="h2">Дайджест</div> 
                                    <div style="font-size: 20px" class="text">Вся информация о новостройках Москвы в удобном формате </div> 
                                </div>
                            </div>
                        </div>
                        <div class="notorbosov" style="    padding-top: 60px; height: 400px;width: 97.8%;   background: url('/img/daijest.jpg');">
                            <div class="torbosovagain" style="width: 485px;height: 52%; color: #fff;text-align: center;margin: auto;">
                                <span style="font-size: 20px;">Хотите получать наш квартальный дайджест о лучших новостройках Москвы?<br>
                                <div style="line-height: 50px;" >
                                Подпишитесь на рассылку:
                                </div>
                                </span>
                                <div style="margin: auto; width: 170px;">
                                <form action="#">
                                    <input class="form-control" style="width:170px;margin-top: 10px;float: left;" type="text" name="name" placeholder="Имя">
                                    <input class="form-control mask" style="width:170px;margin-top: 10px;float: left;" type="text" name="phone" placeholder="Телефон">
                                    <input class="form-control" style="width:170px;margin-top: 10px;float: left;" type="text" name="email" placeholder="Email">
                                    <input class="submit" style="display:none;" type="submit">
                                    <input name="comment" value="" style="display:none;" type="hidden">
                                    <div style="width:170px;float: left;margin-top: 10px;height:35px;" type="submit" class="gold-but" data-where="заказал дайджест">Подписаться
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>              
                <div class="row" style="margin-bottom: 15px;    margin-right: -40px;">
                    <div class="col-md-12" style="    margin-left: -15px;">
                    <?php $i=0; foreach ($leftblock2 as $values) {$i++;?>
                        <div style="color: #000;" class="func col-md-6 col-xs-6 " style="float: left;  width: 49.8%;" data-target='<?=$values['func'];?>'>
                            <img style="width: 102%;<?=$i==2?'margin-left: -18px;':'';?>" src="uploads/banner/<?=$values['image'];?>" alt=""/>
                            <div style="font-size: 20px;margin-top: 7px;margin-bottom: 3px; <?=$i==2?'margin-left: -15px;':''?>" class="h2"><?=$values['h2'];?></div> 
                            <div style="<?=$i==2?'margin-left: -15px;':''?>" class="text"><?=$values['text'];?></div> 
                        </div>
                    <?php }?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 15px;   margin-right: -40px;">
                    <div class="col-md-12" style="    margin-left: -15px;">
                        <?php foreach ($leftblock as $values) {?>
                            <div style="width: 98.2%;position: relative;  " class="func col-md-6 col-xs-6" data-target='<?=$values['func'];?>'>
                                <img style="width: 100%;" src="uploads/banner/<?=$values['image'];?>" alt=""/> 
                                <div class="upblock">
                                    <div style="font-size: 40px;" class="h2"><?=$values['h2'];?></div> 
                                    <div style="font-size: 20px;" class="text"><?=$values['text'];?></div> 
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div> 
            </div>
            <div class="col-md-4 hidden-xs hidden-sm" style="padding-left: 30px;">
<!-- Expert -->
                <h3 style="font-weight: bold;margin-top: 0;">Эксперты портала</h3>
                <div style="position: relative; margin-top: 15px;">
                    <?php foreach ($expert as $key => $value) {?>
                    <div class="row" style="position: relative;margin: 20px 0;">
                        <div class="col-md-4" style="border-radius: 40px; width: 80px;height: 80px;background: url(uploads/experts/<?=urlencode($value['image'])?>);background-size: cover;"></div>
                        <div class="col-md-8"  style="padding-right: 0;width: 73%;">
                            <div><b><?=$value['name'];?></b></div>
                            <div class="chtext"><?=$value['text'];?></div>
                        </div>
                    </div>
                    <?php }?>
                    <div class="leftB" style="<?=($config['ec']==2?'width: 109%;': ($config['ec']==3?'width: 220%;':'') )?>">
                    <?php $e=0;foreach ($expert2 as $key => $value) {$e++;?>
                    <div class="row" style="position: relative;margin-bottom: 20px;">
                        <div class="col-md-4" style="border-radius: 40px; width: 80px;height: 80px;background: url(uploads/experts/<?=urlencode($value['image'])?>);background-size: cover;"></div>
                        <div class="col-md-8"  style="padding-right: 0;width: 73%;">
                            <div><b><?=$value['name'];?></b></div>
                            <div class="chtext"><?=$value['text'];?></div>
                        </div>
                    </div>
                    <?php }?>
                    </div>
                        <div class="href" style="<?=($e?'':'display: none;')?>">Показать все</div>
                        <div class="href" style="display: none;" >Скрыть</div>
                </div>
<!-- Developer -->
                <h3 style="font-weight: bold; margin-top: 45px;">Застройщики и девелоперы</h3>
                <div style="position: relative;">
                    <?php foreach ($developer as $key => $value) {?>
                        <div class="row" style="margin:20px 0 ;">
                            <div class="col-md-4" style="width: 80px;height: 80px;background: url(uploads/developers/<?=urlencode($value['image'])?>) no-repeat;background-size: contain;background-position: top 50%;"></div>
                            <div class="col-md-8"  style="padding-right: 0;width: 73%;">
                                <div><b><?=$value['name'];?></b></div>
                                <div class="chtext"><?=$value['text'];?></div>
                            </div>
                        </div>
                    <?php }?>
                    <div class="leftB" style="<?=($config['dc']==2?'width: 109%;': ($config['ec']==3?'width: 220%;':'') )?>">
                    <?php $d=0;foreach ($developer2 as $key => $value) {$d++;?>
                        <div class="row" style="position: relative;margin-bottom: 20px;">
                            <div class="col-md-4" style="width: 80px;height: 80px;background: url(uploads/developers/<?=$urlencode(value['image'])?>) no-repeat ;background-size: contain;background-position: top 50%;"></div>
                            <div class="col-md-8"  style="padding-right: 0;width: 73%;">
                                <div><b><?=$value['name'];?></b></div>
                                <div class="chtext"><?=$value['text'];?></div>
                            </div>
                        </div>
                    <?php }?>
                    </div>
                        <div class="href" style="<?=($d?'':'display: none;')?>">Показать все</div>
                        <div class="href" style="display: none;">Скрыть</div>
                </div>
        </div>
    </div>
    </div>
</div>


<?php
    yii\bootstrap\Modal::begin([
        'header' => '',
        'id' => 'modal',
        'size' => 'modal-md',       
    ]);
?>
<div id='modal-content'>Загружаю...</div>
<?php yii\bootstrap\Modal::end(); ?>

</div><!-- this div close container -->
<?php
 /*Carousel::widget ( [
                    'items' => $videos,
                    'options' => ['style' => 'width:100%;margin:auto;', 'class' => 'carousel slide', 'data-interval' => '12000'],
                    'controls' => [
                     '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
                     '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'
                    ]
                ]);*/?>
<div class="row "  style=" /*margin-top: 33px;*/ margin-right: 0px; margin-left: 0;">
    <div style="    padding-right: 0px; padding-left: 0;" class="col-md-12"> 
        <div class="vid-block ">
        <div style="color:#ffffff;font-size: 34px;/*font-weight: bold;*/padding-top: 35px;padding-bottom: 15px; text-align: center;">Видео про лучшие жилые комплексы</div>
            <div class="vid-car ">
                <div class="vid-mocentr swiper-container" style="/*position: absolute;left: -370px;*/">
                    <a class="left swiper-button-prev carousel-control" href="#w7" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>
                    <a class="right swiper-button-next carousel-control" href="#w7" data-slide="next"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    <div class="vid-cont swiper-wrapper" style="<?=$vi_con*370?>">
                    
                            <?=$videos?>    
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- some bydlocod -->
    </div>
</div>
<!-- some bydlocod -->
<?php foreach ($boturl as $key => $value) {
    if($value['col']==1){
        $u[1][$key]['name'] =  $value['name'];
        $u[1][$key]['func'] =  $value['func'];}
    if($value['col']==2){
        $u[2][$key]['name'] =  $value['name'];
        $u[2][$key]['func'] =  $value['func'];}
    if($value['col']==3){
        $u[3][$key]['name'] =  $value['name'];
        $u[3][$key]['func'] =  $value['func'];}
    if($value['col']==4){
        $u[4][$key]['name'] =  $value['name'];
        $u[4][$key]['func'] =  $value['func'];}
}?>


<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="//whitewill.ru/" target="_blank"><img class="logo-booty" src="img/ww.png" style="margin: auto;width:120px;display: block;margin-bottom: 35px;"></a>
            </div>
        </div>
        <div class="row" style="line-height: 21px; text-align: left;">
            <?php  foreach ($u as $ssilki) {?>
                <div class="col-md-3">
                    <?php foreach ($ssilki as $po_kolonkam) {?>
                        <a class="href func" style="color: #fff;display: block; text-align: left;font-size: 10px;" href='/<?=$po_kolonkam["func"];?>'><?=$po_kolonkam['name'];?> </a>
                    <?php }?>
                </div>
            <?php }?>




        </div>
    </div>
</footer>
